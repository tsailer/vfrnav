/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/*
 *  Copyright © 2014 Igalia S.L.
 *  Copyright © 2016 autorouter.eu
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include "cfmu-web-extension.h"

#include "cfmu-web-extension-names.h"

#include <gio/gio.h>
#include <gtk/gtk.h>
#include <libsoup/soup.h>
#include <string.h>
#include <webkit2/webkit-web-extension.h>
#define WEBKIT_DOM_USE_UNSTABLE_API
#include <webkitdom/WebKitDOMDOMWindowUnstable.h>
#include <JavaScriptCore/JavaScript.h>

static const gboolean debug = FALSE;

struct _CFMUWebExtension {
	GObject parent_instance;

	WebKitWebExtension *extension;
	gboolean initialized;

	GDBusConnection *dbus_connection;
	GCancellable *cancellable;
	GArray *page_created_signals_pending;
	GDBusMethodInvocation *validate_invocation;
};

static const char introspection_xml[] = "<node>"
                                        " <interface name='eu.autorouter.cfmu.WebExtension'>"
                                        "  <signal name='PageCreated'>"
                                        "   <arg type='t' name='page_id' direction='out'/>"
                                        "  </signal>"
                                        "  <signal name='DomChanged'>"
                                        "   <arg type='t' name='page_id' direction='out'/>"
                                        "   <arg type='b' name='portal_dom' direction='out'/>"
                                        "   <arg type='b' name='validate_dom' direction='out'/>"
                                        "  </signal>"
                                        "  <method name='LaunchValidate'>"
                                        "   <arg type='t' name='page_id' direction='in'/>"
                                        "  </method>"
                                        "  <method name='Validate'>"
                                        "   <arg type='t' name='page_id' direction='in'/>"
                                        "   <arg type='s' name='fpl' direction='in'/>"
                                        "   <arg type='as' name='errors' direction='out'/>"
                                        "  </method>"
                                        " </interface>"
                                        "</node>";

G_DEFINE_TYPE(CFMUWebExtension, cfmu_web_extension, G_TYPE_OBJECT)

static gboolean web_page_send_request(WebKitWebPage *web_page, WebKitURIRequest *request,
                                      WebKitURIResponse *redirected_response, CFMUWebExtension *extension)
{
	const char *request_uri = webkit_uri_request_get_uri(request);
	if (debug)
		g_message("send_request: %s\n", request_uri);

	/* do not modify requests for now */
	return FALSE;
}

static void cfmu_web_extension_emit_dom_changed(CFMUWebExtension *extension, guint64 page_id, gboolean portal_dom,
                                                gboolean validate_dom)
{
	GError *error = NULL;

	g_dbus_connection_emit_signal(extension->dbus_connection, NULL, CFMU_WEB_EXTENSION_OBJECT_PATH,
	                              CFMU_WEB_EXTENSION_INTERFACE, "DomChanged",
	                              g_variant_new("(tbb)", page_id, portal_dom, validate_dom), &error);
	if (error) {
		g_warning("Error emitting signal DomChanged: %s\n", error->message);
		g_error_free(error);
	}
}

static void web_page_check_dom(CFMUWebExtension *extension, WebKitWebPage *web_page)
{
	const gchar *uri = webkit_web_page_get_uri(web_page);
	guint64 page_id = webkit_web_page_get_id(web_page);

	if (debug)
		g_message("DOM changed: %s\n", uri);
	if (uri && !g_strcmp0(uri, "https://www.public.nm.eurocontrol.int/PUBPORTAL/gateway/spec/index.html")) {
		static const char script[] =
		        "(function(){"
		        "var btn = document.getElementById(\"IFPUV_LAUNCH_AREA.FREE_TEXT_EDIT_LINK_LABEL\");"
		        "if (btn !== null && btn !== undefined) { return true; }"
		        "return false;"
		        "})();";
		JSCContext *jsContext = NULL;
		JSCValue *retval = NULL;
		gboolean success = FALSE;

		jsContext = webkit_frame_get_js_context_for_script_world(webkit_web_page_get_main_frame(web_page),
		                                                         webkit_script_world_get_default());
		retval = jsc_context_evaluate(jsContext, script, -1);
		if (retval) {
			success = jsc_value_to_boolean(retval);
			g_object_unref(retval);
			retval = NULL;
		} else {
			g_message("cannot execute script \"%s\"\n", script);
		}
		g_object_unref(jsContext);
		jsContext = NULL;
		if (extension->dbus_connection)
			cfmu_web_extension_emit_dom_changed(extension, page_id, success, FALSE);
		return;
	}
	if (uri && g_strrstr(uri, "_view_id=IFPUV_DETACHED_LIST")) {
		static const char scriptchk[] =
		        "(function(){"
		        "var fpl = document.getElementById(\"FREE_TEXT_EDITOR.FLIGHT_DATA_AREA.GENERAL_DATA_ENTRY.INTRODUCE_FLIGHT_PLAN_FIELD\");"
		        "if (fpl === null || fpl === null) { return false; }"
		        "var resarea = document.getElementById(\"FREE_TEXT_EDITOR.VALIDATION_RESULTS_AREA\");"
		        "if (resarea === null || resarea === null) { return false; }"
		        "var lbl = document.getElementById(\"FREE_TEXT_EDITOR.FLIGHT_DATA_AREA.VALIDATE_ACTION_LABEL\");"
		        "if (lbl === null || lbl === null) { return false; }"
		        "return true;"
		        "})();";
		static const char scriptextr[] =
		        "(function(){"
		        "var resarea = document.getElementById(\"FREE_TEXT_EDITOR.VALIDATION_RESULTS_AREA\");"
		        "if (resarea === null || resarea === null) { return \"\"; }"
		        "var msg = '';"
		        "resarea.querySelectorAll(\"div.portal_dataValue\").forEach("
		        "  function (currentValue, currentIndex, listObj) {"
		        "    if (currentValue.offsetParent !== null && currentValue.innerText !== null && currentValue.innerText !== undefined) {"
		        "      msg += currentValue.innerText + '\\n';"
		        "    }"
		        "  });"
		        "resarea.querySelectorAll(\"tr.portal_rowOdd,tr.portal_rowEven\").forEach("
		        "  function (currentValue, currentIndex, listObj) {"
		        "    if (currentValue.offsetParent !== null && currentValue.innerText !== null && currentValue.innerText !== undefined) {"
		        "      msg += currentValue.innerText + '\\n';"
		        "    }"
		        "  });"
		        "return msg;"
		        "})();";
		JSCContext *jsContext = NULL;
		JSCValue *retval = NULL;
		gboolean success = FALSE;
		char *result = NULL;

		jsContext = webkit_frame_get_js_context_for_script_world(webkit_web_page_get_main_frame(web_page),
		                                                         webkit_script_world_get_default());
		retval = jsc_context_evaluate(jsContext, scriptchk, -1);
		if (retval) {
			success = jsc_value_to_boolean(retval);
			g_object_unref(retval);
			retval = NULL;
		} else {
			g_message("cannot execute script \"%s\"\n", scriptchk);
		}
		retval = jsc_context_evaluate(jsContext, scriptextr, -1);
		if (retval) {
			result = jsc_value_to_string(retval);
			g_object_unref(retval);
			retval = NULL;
		} else {
			g_message("cannot execute script \"%s\"\n", scriptextr);
		}
		g_object_unref(jsContext);
		jsContext = NULL;
		if (result) {
			if (debug)
				g_message("result: \"%s\"\n", result);
			if (extension->validate_invocation) {
				guint result_count = 0;
				char *msg = result;
				GVariantBuilder builder;
				g_variant_builder_init(&builder, G_VARIANT_TYPE("as"));
				while (msg) {
					char *next = strchr(msg, '\n');
					if (next) {
						*next++ = 0;
					}
					if (*msg) {
						g_variant_builder_add(&builder, "s", msg);
						++result_count;
					}
					msg = next;
				}
				if (result_count) {
					g_dbus_method_invocation_return_value(extension->validate_invocation,
					                                      g_variant_new("(as)", &builder));
					extension->validate_invocation = 0;
				} else {
					g_variant_builder_clear(&builder);
				}
			}
			g_free(result);
		}
		if (extension->dbus_connection)
			cfmu_web_extension_emit_dom_changed(extension, page_id, FALSE, success);
		return;
	}
	if (extension->dbus_connection)
		cfmu_web_extension_emit_dom_changed(extension, page_id, FALSE, FALSE);
}

static void cfmu_web_extension_vfrnav_notify_cb(guint64 page_id, CFMUWebExtension *extension)
{
	WebKitWebPage *web_page = NULL;

	if (debug)
		g_message("vfrnav_notify: %" G_GUINT64_FORMAT "\n", page_id);

	web_page = webkit_web_extension_get_page(extension->extension, page_id);
	if (!web_page) {
		g_error("vfrnav_notify: page_id %" G_GUINT64_FORMAT " not found\n", page_id);
		return;
	}
	web_page_check_dom(extension, web_page);
}

static void cfmu_web_extension_vfrnav_notify_destroy_cb(CFMUWebExtension *extension)
{
	if (debug)
		g_message("vfrnav_notify destroyed\n");
}

static void web_page_document_loaded(WebKitWebPage *web_page, CFMUWebExtension *extension)
{
	static const char scripttempl[] =
	        "(function(){"
	        "  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;"
	        "  var observer = new MutationObserver(function(mutations, observer) {"
	        "    vfrnav_notify(%" G_GUINT64_FORMAT ");"
	        "  });"
	        "  observer.observe(document, {"
	        "    subtree: true,"
	        "    attributes: true,"
	        "    childList: true,"
	        "    characterData: true"
	        "  });"
	        "})();";
	char *script = NULL;
	JSCContext *jsContext = NULL;
	JSCValue *retval = NULL;
	guint64 page_id = webkit_web_page_get_id(web_page);

	if (debug)
		g_message("document loaded: %s\n", webkit_web_page_get_uri(web_page));
	jsContext = webkit_frame_get_js_context_for_script_world(webkit_web_page_get_main_frame(web_page),
	                                                         webkit_script_world_get_default());
	retval = jsc_value_new_function(jsContext, NULL, (GCallback)cfmu_web_extension_vfrnav_notify_cb, extension,
	                                (GDestroyNotify)cfmu_web_extension_vfrnav_notify_destroy_cb, G_TYPE_NONE, 1,
	                                G_TYPE_UINT64);
	jsc_context_set_value(jsContext, "vfrnav_notify", retval);
	g_object_unref(retval);
	script = g_strdup_printf(scripttempl, page_id);
	retval = jsc_context_evaluate(jsContext, script, -1);
	if (retval) {
		g_object_unref(retval);
		retval = NULL;
	} else {
		g_message("cannot execute script \"%s\"\n", script);
	}
	g_free(script);
	script = NULL;
}

static void web_page_uri_changed(WebKitWebPage *web_page, GParamSpec *param_spec, CFMUWebExtension *extension)
{
	if (debug)
		g_message("uri_changed: %s\n", webkit_web_page_get_uri(web_page));
}

static void cfmu_web_extension_emit_page_created(CFMUWebExtension *extension, guint64 page_id)
{
	GError *error = NULL;

	g_dbus_connection_emit_signal(extension->dbus_connection, NULL, CFMU_WEB_EXTENSION_OBJECT_PATH,
	                              CFMU_WEB_EXTENSION_INTERFACE, "PageCreated", g_variant_new("(t)", page_id),
	                              &error);
	if (error) {
		g_warning("Error emitting signal PageCreated: %s\n", error->message);
		g_error_free(error);
	}
}

static void cfmu_web_extension_emit_page_created_signals_pending(CFMUWebExtension *extension)
{
	guint i;

	if (!extension->page_created_signals_pending)
		return;

	for (i = 0; i < extension->page_created_signals_pending->len; i++) {
		guint64 page_id;
		WebKitWebPage *web_page;

		page_id = g_array_index(extension->page_created_signals_pending, guint64, i);
		cfmu_web_extension_emit_page_created(extension, page_id);

		web_page = webkit_web_extension_get_page(extension->extension, page_id);
		if (web_page)
			web_page_check_dom(extension, web_page);
	}

	g_array_free(extension->page_created_signals_pending, TRUE);
	extension->page_created_signals_pending = NULL;
}

static void cfmu_web_extension_queue_page_created_signal_emission(CFMUWebExtension *extension, guint64 page_id)
{
	if (!extension->page_created_signals_pending)
		extension->page_created_signals_pending = g_array_new(FALSE, FALSE, sizeof(guint64));
	extension->page_created_signals_pending = g_array_append_val(extension->page_created_signals_pending, page_id);
}

static void cfmu_web_extension_page_created_cb(CFMUWebExtension *extension, WebKitWebPage *web_page)
{
	guint64 page_id = webkit_web_page_get_id(web_page);
	if (extension->dbus_connection)
		cfmu_web_extension_emit_page_created(extension, page_id);
	else
		cfmu_web_extension_queue_page_created_signal_emission(extension, page_id);

	g_signal_connect(web_page, "send-request", G_CALLBACK(web_page_send_request), extension);
	g_signal_connect(web_page, "document-loaded", G_CALLBACK(web_page_document_loaded), extension);
	g_signal_connect(web_page, "notify::uri", G_CALLBACK(web_page_uri_changed), extension);
}

static WebKitWebPage *get_webkit_web_page_or_return_dbus_error(GDBusMethodInvocation *invocation,
                                                               WebKitWebExtension *web_extension, guint64 page_id)
{
	WebKitWebPage *web_page = webkit_web_extension_get_page(web_extension, page_id);
	if (!web_page) {
		g_dbus_method_invocation_return_error(invocation, G_DBUS_ERROR, G_DBUS_ERROR_INVALID_ARGS,
		                                      "Invalid page ID: %" G_GUINT64_FORMAT, page_id);
	}
	return web_page;
}

static void handle_method_call(GDBusConnection *connection, const char *sender, const char *object_path,
                               const char *interface_name, const char *method_name, GVariant *parameters,
                               GDBusMethodInvocation *invocation, gpointer user_data)
{
	CFMUWebExtension *extension = CFMU_WEB_EXTENSION(user_data);

	if (g_strcmp0(interface_name, CFMU_WEB_EXTENSION_INTERFACE) != 0)
		return;

	if (g_strcmp0(method_name, "LaunchValidate") == 0) {
		static const char script[] =
		        "(function(){"
		        "var btn = document.getElementById(\"IFPUV_LAUNCH_AREA.FREE_TEXT_EDIT_LINK_LABEL\");"
		        "if (btn !== null && btn !== undefined) { btn.click(); return true; }"
		        "return false;"
		        "})();";
		WebKitWebPage *web_page;
		guint64 page_id;
		JSCContext *jsContext = NULL;
		JSCValue *retval = NULL;
		gboolean success = FALSE;

		g_variant_get(parameters, "(t)", &page_id);
		web_page = get_webkit_web_page_or_return_dbus_error(invocation, extension->extension, page_id);
		if (!web_page) {
			g_dbus_method_invocation_return_error(invocation, G_IO_ERROR, G_IO_ERROR_NOT_FOUND,
			                                      "page ID %" G_GUINT64_FORMAT " not found", page_id);
			return;
		}
		jsContext = webkit_frame_get_js_context_for_script_world(webkit_web_page_get_main_frame(web_page),
		                                                         webkit_script_world_get_default());
		retval = jsc_context_evaluate(jsContext, script, -1);
		if (retval) {
			success = jsc_value_to_boolean(retval);
			g_object_unref(retval);
			retval = NULL;
		} else {
			g_message("cannot execute script \"%s\"\n", script);
		}
		g_object_unref(jsContext);
		jsContext = NULL;
		if (!success) {
			g_dbus_method_invocation_return_error_literal(
			        invocation, G_IO_ERROR, G_IO_ERROR_NOT_FOUND,
			        "IFPUV_LAUNCH_AREA.FREE_TEXT_EDIT_LINK_LABEL not found");
			return;
		}
		g_dbus_method_invocation_return_value(invocation, 0);
		return;
	}
	if (g_strcmp0(method_name, "Validate") == 0) {
		static const char scripttempl[] =
		        "(function(){"
		        "var resarea = document.getElementById(\"FREE_TEXT_EDITOR.VALIDATION_RESULTS_AREA\");"
		        "if (resarea === null || resarea === undefined) {"
		        "  return \"FREE_TEXT_EDITOR.VALIDATION_RESULTS_AREA field not found\";"
		        "}"
		        "resarea.querySelectorAll(\"tr.portal_rowOdd,tr.portal_rowEven\").forEach("
		        "  function (currentValue, currentIndex, listObj) {"
		        "    currentValue.replaceChildren();"
		        "  });"
		        "var fplfield = document.getElementById(\"FREE_TEXT_EDITOR.FLIGHT_DATA_AREA.GENERAL_DATA_ENTRY.INTRODUCE_FLIGHT_PLAN_FIELD\");"
		        "if (fplfield === null || fplfield === undefined) {"
		        "  return \"FREE_TEXT_EDITOR.FLIGHT_DATA_AREA.GENERAL_DATA_ENTRY.INTRODUCE_FLIGHT_PLAN_FIELD field not found\";"
		        "}"
		        "fplfield.value = \'%s\';"
		        "var lbl = document.getElementById(\"FREE_TEXT_EDITOR.FLIGHT_DATA_AREA.VALIDATE_ACTION_LABEL\");"
		        "if (lbl === null || lbl === undefined) {"
		        "  return \"FREE_TEXT_EDITOR.FLIGHT_DATA_AREA.VALIDATE_ACTION_LABEL field not found\";"
		        "}"
		        "lbl.click();"
		        "return \"\";"
		        "})();";
		WebKitWebPage *web_page;
		guint64 page_id = 0;
		const char *fpl = NULL;
		JSCContext *jsContext = NULL;
		JSCValue *retval = NULL;
		char *script = NULL;
		char *cp = NULL;

		g_variant_get(parameters, "(ts)", &page_id, &fpl);
		web_page = get_webkit_web_page_or_return_dbus_error(invocation, extension->extension, page_id);
		if (!web_page) {
			g_dbus_method_invocation_return_error(invocation, G_IO_ERROR, G_IO_ERROR_NOT_FOUND,
			                                      "page ID %" G_GUINT64_FORMAT " not found", page_id);
			return;
		}
		if (extension->validate_invocation) {
			g_dbus_method_invocation_return_error(invocation, G_IO_ERROR, G_IO_ERROR_BUSY,
			                                      "validation in progress");
			return;
		}
		if (!fpl || !*fpl) {
			g_dbus_method_invocation_return_error(invocation, G_IO_ERROR, G_IO_ERROR_INVALID_DATA,
			                                      "flight plan empty");
			return;
		}

		if (*fpl == '-') {
			++fpl;
		}

		if (strchr(fpl, '\'') != NULL) {
			g_dbus_method_invocation_return_error(invocation, G_IO_ERROR, G_IO_ERROR_FAILED,
			                                      "invalid flight plan");
			return;
		}

		jsContext = webkit_frame_get_js_context_for_script_world(webkit_web_page_get_main_frame(web_page),
		                                                         webkit_script_world_get_default());

		script = g_strdup_printf(scripttempl, fpl);
		retval = jsc_context_evaluate(jsContext, script, -1);
		g_object_unref(jsContext);
		jsContext = NULL;
		if (!jsc_value_is_string(retval)) {
			g_dbus_method_invocation_return_error(invocation, G_IO_ERROR, G_IO_ERROR_FAILED,
			                                      "cannot execute javascript script");
			g_message("Cannot execute js \"%s\"\n", script);
			g_free(script);
			script = NULL;
			return;
		}
		g_free(script);
		script = NULL;
		cp = jsc_value_to_string(retval);
		g_object_unref(retval);
		retval = NULL;
		if (cp && *cp) {
			g_dbus_method_invocation_return_error(invocation, G_IO_ERROR, G_IO_ERROR_FAILED, "%s", cp);
			g_free(cp);
			return;
		}
		extension->validate_invocation = invocation;
		return;
	}
}

static const GDBusInterfaceVTable interface_vtable = {handle_method_call, NULL, NULL};

static void cfmu_web_extension_dispose(GObject *object)
{
	CFMUWebExtension *extension = CFMU_WEB_EXTENSION(object);

	if (extension->validate_invocation) {
		GVariantBuilder *builder = g_variant_builder_new(G_VARIANT_TYPE("(as)"));
		g_variant_builder_add(builder, "s", "WEBPAGE_DISPOSE");
		g_dbus_method_invocation_return_value(extension->validate_invocation, g_variant_new("(as)", builder));
		g_variant_builder_unref(builder);
		extension->validate_invocation = 0;
	}

	if (extension->page_created_signals_pending) {
		g_array_free(extension->page_created_signals_pending, TRUE);
		extension->page_created_signals_pending = NULL;
	}

	g_clear_object(&extension->cancellable);
	g_clear_object(&extension->dbus_connection);

	g_clear_object(&extension->extension);

	G_OBJECT_CLASS(cfmu_web_extension_parent_class)->dispose(object);
}

static void cfmu_web_extension_class_init(CFMUWebExtensionClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);

	object_class->dispose = cfmu_web_extension_dispose;
}

static void cfmu_web_extension_init(CFMUWebExtension *extension)
{
}

static gpointer cfmu_web_extension_create_instance(gpointer data)
{
	return g_object_new(CFMU_TYPE_WEB_EXTENSION, NULL);
}

CFMUWebExtension *cfmu_web_extension_get()
{
	static GOnce once_init = G_ONCE_INIT;
	return CFMU_WEB_EXTENSION(g_once(&once_init, cfmu_web_extension_create_instance, NULL));
}

static void dbus_connection_created_cb(GObject *source_object, GAsyncResult *result, CFMUWebExtension *extension)
{
	static GDBusNodeInfo *introspection_data = NULL;
	GDBusConnection *connection;
	guint registration_id;
	GError *error = NULL;

	if (!introspection_data)
		introspection_data = g_dbus_node_info_new_for_xml(introspection_xml, NULL);

	connection = g_dbus_connection_new_for_address_finish(result, &error);
	if (error) {
		g_warning("Failed to connect to UI process: %s", error->message);
		g_error_free(error);
		return;
	}

	registration_id = g_dbus_connection_register_object(connection, CFMU_WEB_EXTENSION_OBJECT_PATH,
	                                                    introspection_data->interfaces[0], &interface_vtable,
	                                                    extension, NULL, &error);
	if (!registration_id) {
		g_warning("Failed to register web extension object: %s\n", error->message);
		g_error_free(error);
		g_object_unref(connection);
		return;
	}

	extension->dbus_connection = connection;
	cfmu_web_extension_emit_page_created_signals_pending(extension);
}

void cfmu_web_extension_initialize(CFMUWebExtension *extension, WebKitWebExtension *wk_extension,
                                   const char *server_address)
{
	GDBusAuthObserver *observer;

	g_return_if_fail(CFMU_IS_WEB_EXTENSION(extension));

	if (extension->initialized)
		return;

	extension->initialized = TRUE;

	extension->extension = g_object_ref(wk_extension);

	g_signal_connect_swapped(extension->extension, "page-created", G_CALLBACK(cfmu_web_extension_page_created_cb),
	                         extension);

	extension->cancellable = g_cancellable_new();

	observer = g_dbus_auth_observer_new();

	g_dbus_connection_new_for_address(server_address, G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT, observer,
	                                  extension->cancellable, (GAsyncReadyCallback)dbus_connection_created_cb,
	                                  extension);
	g_object_unref(observer);
}
