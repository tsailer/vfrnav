/***************************************************************************
 *   Copyright (C) 2018                                                    *
 *     by Thomas Sailer t.sailer@alumni.ethz.ch                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "sysdeps.h"
#include "geomgdal.h"

#ifdef HAVE_GDAL

namespace {

void addpoints(OGRLineString *x, const LineString& ls, Point& refpt, bool close)
{
	double rlon(refpt.get_lon_deg_dbl()), rlat(refpt.get_lat_deg_dbl());
	for (LineString::const_iterator i(ls.begin()), e(ls.end()); i != e; ++i) {
		if (i->is_invalid())
			continue;
		if (refpt.is_invalid()) {
			refpt = *i;
			rlon = refpt.get_lon_deg_dbl();
			rlat = refpt.get_lat_deg_dbl();
		}
		Point p(*i - refpt);
		x->addPoint(rlon + p.get_lon_deg_dbl(), rlat + p.get_lat_deg_dbl());
	}
	if (close) {
		for (LineString::const_iterator i(ls.begin()), e(ls.end()); i != e; ++i) {
			if (i->is_invalid())
				continue;
			Point p(*i - refpt);
			x->addPoint(rlon + p.get_lon_deg_dbl(), rlat + p.get_lat_deg_dbl());
			break;
		}
	}
}

};

OGRPoint *to_ogr_geometry(const Point& pt)
{
	if (pt.is_invalid())
		return new OGRPoint();
	return new OGRPoint(pt.get_lon_deg_dbl(), pt.get_lat_deg_dbl());
}

OGRLinearRing *to_ogr_geometry(const Rect& r)
{
	OGRLinearRing *x = new OGRLinearRing();
	x->addPoint(r.get_southwest().get_lon_deg_dbl(), r.get_southwest().get_lat_deg_dbl());
	x->addPoint(r.get_southeast().get_lon_deg_dbl(), r.get_southeast().get_lat_deg_dbl());
	x->addPoint(r.get_northeast().get_lon_deg_dbl(), r.get_northeast().get_lat_deg_dbl());
	x->addPoint(r.get_northwest().get_lon_deg_dbl(), r.get_northwest().get_lat_deg_dbl());
	return x;
}

OGRMultiPoint *to_ogr_geometry(const MultiPoint& mp)
{
	OGRMultiPoint *x = new OGRMultiPoint();
	for (MultiPoint::const_iterator i(mp.begin()), e(mp.end()); i != e; ++i)
		x->addGeometryDirectly(to_ogr_geometry(*i));
	return x;
}

OGRLineString *to_ogr_geometry(const LineString& ls, Point& refpt)
{
	OGRLineString *x = new OGRLineString();
	addpoints(x, ls, refpt, false);
	return x;
}

OGRLineString *to_ogr_geometry(const LineString& ls)
{
	Point refpt(Point::invalid);
	return to_ogr_geometry(ls, refpt);
}

OGRMultiLineString *to_ogr_geometry(const MultiLineString& mls, Point& refpt)
{
	OGRMultiLineString *x = new OGRMultiLineString();
	for (MultiLineString::const_iterator i(mls.begin()), e(mls.end()); i != e; ++i)
		x->addGeometryDirectly(to_ogr_geometry(*i, refpt));
	return x;
}

OGRMultiLineString *to_ogr_geometry(const MultiLineString& mls)
{
	Point refpt(Point::invalid);
	return to_ogr_geometry(mls, refpt);
}

OGRLinearRing *to_ogr_geometry(const PolygonSimple& ps, Point& refpt)
{
	OGRLinearRing *x = new OGRLinearRing();
	addpoints(x, ps, refpt, true);
	return x;
}

OGRLinearRing *to_ogr_geometry(const PolygonSimple& ps)
{
	Point refpt(Point::invalid);
	return to_ogr_geometry(ps, refpt);
}

OGRPolygon *to_ogr_geometry(const PolygonHole& ph, Point& refpt)
{
	OGRPolygon *x = new OGRPolygon();
 	x->addRingDirectly(to_ogr_geometry(ph.get_exterior(), refpt));
	for (unsigned int i(0), n(ph.get_nrinterior()); i < n; ++i)
		x->addRingDirectly(to_ogr_geometry(ph[i], refpt));
	return x;
}

OGRPolygon *to_ogr_geometry(const PolygonHole& ph)
{
	Point refpt(Point::invalid);
	return to_ogr_geometry(ph, refpt);
}

OGRMultiPolygon *to_ogr_geometry(const MultiPolygonHole& mph, Point& refpt)
{
	OGRMultiPolygon *x = new OGRMultiPolygon();
	for (MultiPolygonHole::const_iterator i(mph.begin()), e(mph.end()); i != e; ++i)
		x->addGeometryDirectly(to_ogr_geometry(*i, refpt));
	return x;
}

OGRMultiPolygon *to_ogr_geometry(const MultiPolygonHole& mph)
{
	OGRMultiPolygon *x = new OGRMultiPolygon();
	for (MultiPolygonHole::const_iterator i(mph.begin()), e(mph.end()); i != e; ++i)
		x->addGeometryDirectly(to_ogr_geometry(*i));
	return x;
}

#endif
