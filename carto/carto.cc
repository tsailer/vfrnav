//
// C++ Implementation: carto
//
// Description: Carto project file parser
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glibmm.h>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cmath>

#ifdef HAVE_YAMLCPP
#include <yaml-cpp/yaml.h>
#endif

#include "geom.h"
#include "carto.h"

namespace {
	const std::string& to_str(YAML::NodeType::value v) {
		switch (v) {
		case YAML::NodeType::Undefined:
		{
			static const std::string r("undefined");
			return r;
		}

		case YAML::NodeType::Null:
		{
			static const std::string r("null");
			return r;
		}

		case YAML::NodeType::Scalar:
		{
			static const std::string r("scalar");
			return r;
		}

		case YAML::NodeType::Sequence:
		{
			static const std::string r("sequence");
			return r;
		}

		case YAML::NodeType::Map:
		{
			static const std::string r("map");
			return r;
		}

		default:
		{
			static const std::string r("?");
			return r;
		}
		}
	}

	inline std::ostream& operator<<(std::ostream& os, YAML::NodeType::value v) { return os << to_str(v); }
};

const std::string& to_str(Carto::LayerBase::layertype_t x)
{
	switch (x) {
	case Carto::LayerBase::layertype_t::null:
	{
		static const std::string r("null");
		return r;
	}

	case Carto::LayerBase::layertype_t::osm:
	{
		static const std::string r("osm");
		return r;
	}

	case Carto::LayerBase::layertype_t::osmstatic:
	{
		static const std::string r("osmstatic");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

Carto::LayerBase::LayerBase(void)
	: m_layertype(layertype_t::null)
{
}

Carto::LayerBase::LayerBase(const sqlptr_t& expr)
	: m_layertype(layertype_t::null)
{
	if (expr) {
		::new(&m_sqlexpr) sqlptr_t(expr);
		m_layertype = layertype_t::osm;
	}
}

Carto::LayerBase::LayerBase(staticlayer_t sl)
	: m_layertype(layertype_t::osmstatic)
{
	m_staticlayer = sl;
}

Carto::LayerBase::LayerBase(samelayer_t samelayer)
	: m_layertype(layertype_t::samelayer)
{
	m_samelayer = samelayer;
}

Carto::LayerBase::LayerBase(const LayerBase& x)
	: m_layertype(layertype_t::null)
{
	operator=(x);
}

Carto::LayerBase::LayerBase(LayerBase&& x)
	: m_layertype(layertype_t::null)
{
	operator=(x);
}

Carto::LayerBase::~LayerBase()
{
	dealloc();
}

void Carto::LayerBase::dealloc(void)
{
	switch (m_layertype) {
	case layertype_t::osm:
		m_sqlexpr.~sqlptr_t();
		break;

	default:
		break;
	}
	m_layertype = layertype_t::null;
}

Carto::LayerBase& Carto::LayerBase::operator=(const sqlptr_t& expr)
{
	dealloc();
	if (expr) {
		m_layertype = layertype_t::osm;
		::new(&m_sqlexpr) sqlptr_t(expr);
	}
	return *this;
}

Carto::LayerBase& Carto::LayerBase::operator=(staticlayer_t sl)
{
	dealloc();
	m_layertype = layertype_t::osmstatic;
	m_staticlayer = sl;
	return *this;
}

Carto::LayerBase& Carto::LayerBase::operator=(samelayer_t samelayer)
{
	dealloc();
	m_layertype = layertype_t::samelayer;
        m_samelayer = samelayer;
	return *this;
}

Carto::LayerBase& Carto::LayerBase::operator=(const LayerBase& x)
{
	dealloc();
	m_layertype = x.m_layertype;
	switch (m_layertype) {
	case layertype_t::null:
		break;

	case layertype_t::osm:
		::new(&m_sqlexpr) sqlptr_t(x.m_sqlexpr);
		break;

	case layertype_t::osmstatic:
		m_staticlayer = x.m_staticlayer;
		break;

	case layertype_t::samelayer:
		m_samelayer = x.m_samelayer;
		break;
	}
	return *this;
}

Carto::LayerBase& Carto::LayerBase::operator=(LayerBase&& x)
{
	dealloc();
	m_layertype = x.m_layertype;
	switch (m_layertype) {
	case layertype_t::null:
		break;

	case layertype_t::osm:
		::new(&m_sqlexpr) sqlptr_t();
		m_sqlexpr.swap(x.m_sqlexpr);
		break;

	case layertype_t::osmstatic:
		m_staticlayer = x.m_staticlayer;
		break;

	case layertype_t::samelayer:
		m_samelayer = x.m_samelayer;
		break;
	}
	return *this;
}

Carto::LayerBase::staticlayer_t Carto::LayerBase::get_staticlayer(void) const
{
	if (m_layertype == layertype_t::osmstatic)
		return m_staticlayer;
	return static_cast<staticlayer_t>(static_cast<std::underlying_type<staticlayer_t>::type>(-1));
}

const Carto::LayerBase::sqlptr_t& Carto::LayerBase::get_sqlexpr(void) const
{
	if (m_layertype == layertype_t::osm)
		return m_sqlexpr;
	static const sqlptr_t nullexpr;
	return nullexpr;
}

Carto::LayerBase::samelayer_t Carto::LayerBase::get_samelayer(void) const
{
	if (m_layertype == layertype_t::samelayer)
		return m_samelayer;
	return static_cast<samelayer_t>(-1);
}

Carto::Layer::Layer(void)
	: m_minzoom(0), m_maxzoom(22), m_geometry(geometry_t::invalid), m_save(false)
{
}

Carto::Error::Error(const std::string& x)
	: std::runtime_error(x)
{
}

Carto::SQLError::SQLError(const CartoSQL::SQLError& x, const std::string& layerid, const std::string& expr)
	: CartoSQL::SQLError(x), m_layerid(layerid), m_sqlexpr(expr)
{
}

Carto::Carto(void)
	: m_osmstaticdb(nullptr), m_minzoom(0), m_maxzoom(22)
{
}

Carto::Carto(const std::string& fn)
	: Carto()
{
	parse_project(fn);
}

void Carto::parse_project(std::istream& is, const std::string& dirname, std::ostream *msg)
{
	static constexpr bool debug = false;
	if (!is)
                throw std::runtime_error("Cannot read project file");
	YAML::Node proj(YAML::Load(is));
	if (!proj) {
		std::cerr << "Project file error" << std::endl;
		return;
	}
	m_minzoom = proj["minzoom"].as<zoom_t>(0);
	m_maxzoom = proj["maxzoom"].as<zoom_t>(22);
	m_srs = proj["srs"].as<std::string>(std::string());
	m_layers.clear();
	if (proj["Stylesheet"] && proj["Stylesheet"].IsSequence()) {
		std::vector<std::string> fns;
		for (const auto& st : proj["Stylesheet"]) {
			fns.push_back(Glib::build_filename(dirname, st.as<std::string>()));
			if (debug && msg)
				*msg << "Stylesheet: " << fns.back() << std::endl;
		}
		m_css.parse_stylefiles(fns, dirname, msg);
	}
	if (proj["Layer"] && proj["Layer"].IsSequence()) {
		typedef std::map<std::string, Layer::samelayer_t> sqlexpr_t;
		sqlexpr_t sqlexpr;
		typedef std::map<Layer::samelayer_t, Layer::samelayer_t> lastuse_t;
		lastuse_t lastuse;
		for (const auto& layer : proj["Layer"]) {
			Layer lay;
			lay.set_id(layer["id"].as<std::string>());
			lay.set_geometry(Layer::geometry_t::invalid);
			{
				std::string geom(layer["geometry"].as<std::string>(""));
				if (!geom.empty()) {
					Layer::geometry_t g(Geometry::parse_type(geom));
					if (g == Layer::geometry_t::invalid)
						throw Error(lay.get_id() + ": unrecognized geomertry " + geom);
					if (g != Layer::geometry_t::point && g != Layer::geometry_t::linestring && g != Layer::geometry_t::polygon)
						throw Error(lay.get_id() + ": unsupported geomertry " + to_str(g));
					lay.set_geometry(g);
				}
			}
			const YAML::Node& prop(layer["properties"]);
			lay.set_minzoom(prop["minzoom"].as<zoom_t>(get_minzoom()));
			lay.set_maxzoom(prop["maxzoom"].as<zoom_t>(get_maxzoom()));
			const YAML::Node& ds(layer["Datasource"]);
			std::string sql(ds["table"].as<std::string>(""));
			std::string file(ds["file"].as<std::string>(""));
			if (!(file.empty() || sql.empty()) || (file.empty() && sql.empty()))
				throw Error(lay.get_id() + ": either table or file must be specified, but not both");
			if (!file.empty()) {
				std::string base(Glib::path_get_basename(file));
				if (base == "ne_110m_admin_0_boundary_lines_land.shp")
					lay.set_staticlayer(Layer::staticlayer_t::boundarylinesland);
				else if (base == "icesheet_polygons.shp")
					lay.set_staticlayer(Layer::staticlayer_t::icesheetpolygons);
				else if (base == "icesheet_outlines.shp")
					lay.set_staticlayer(Layer::staticlayer_t::icesheetoutlines);
				else if (base == "water_polygons.shp")
					lay.set_staticlayer(Layer::staticlayer_t::waterpolygons);
				else if (base == "simplified_water_polygons.shp")
					lay.set_staticlayer(Layer::staticlayer_t::simplifiedwaterpolygons);
				else
					throw Error(lay.get_id() + ": unknown static layer " + file + " (basename " + base + ")");
			}
  			if (!sql.empty()) {
				std::pair<sqlexpr_t::iterator,bool> ins(sqlexpr.insert(sqlexpr_t::value_type(sql, m_layers.size())));
				if (!ins.second) {
					if (debug && msg)
						*msg << "Layer " << lay.get_id() << ": same expression as layer " << m_layers[ins.first->second].get_id() << std::endl;
					lay.set_samelayer(ins.first->second);
					std::pair<lastuse_t::iterator,bool> ins2(lastuse.insert(lastuse_t::value_type(ins.first->second, m_layers.size())));
					if (ins2.second)
						m_layers[ins.first->second].set_save(true);
					else
						m_layers[ins2.first->second].set_save(true);
				} else {
					try {
						std::istringstream iss(sql);
						CartoSQL::ExprTable::const_ptr_t expr(m_sql.parse_sql(iss, msg));
						if (!expr)
							throw SQLError(CartoSQL::SQLError("no select expression"), lay.get_id(), sql);
						lay.set_sqlexpr(expr);
						CartoSQL::ExprTable::const_ptr_t exprs(expr->simplify(&m_sql));
						if (exprs)
							lay.set_sqlexpr(exprs);
					} catch (const CartoSQL::SQLError& e) {
						throw SQLError(e, lay.get_id(), sql);
					}
				}
			}
			m_layers.push_back(lay);
			if (debug && msg) {
				*msg << "Layer: " << lay.get_id() << " geom: " << lay.get_geometry()
				     << " zoom: " << lay.get_minzoom() << "..." << lay.get_maxzoom() << std::endl;
				if (!sql.empty())
					*msg << "  sql: " << sql << std::endl;
				if (!file.empty())
					*msg << "  file: " << file << std::endl;
				for(const auto& x : ds)
					*msg << "  - " << x.first.as<std::string>() << ": " << x.second.Type() << std::endl;
			}
		}
	}
}

void Carto::parse_project(const std::string& fn, std::ostream *msg)
{
	std::ifstream is(fn.c_str());
	if (!is.is_open())
		throw std::runtime_error("Cannot open file " + fn);
	parse_project(is, Glib::path_get_dirname(fn), msg);
}

void Carto::load_project(const std::string& fn, std::ostream *msg)
{
	static constexpr bool optgeneral = true;
	static constexpr bool optcolumn = true;
	static constexpr bool optfilter = true;
	parse_style(Glib::build_filename(Glib::path_get_dirname(fn), "openstreetmap-carto.style"));
	parse_project(fn, &std::cerr);
	// optimize SQL queries
	for (Layer& layer : m_layers) {
		if (layer.get_layertype() != Layer::layertype_t::osm)
			continue;
		if (optgeneral) {
			CartoSQL::ExprTable::const_ptr_t e(layer.get_sqlexpr()->simplify(&m_sql));
			if (e)
				layer.set_sqlexpr(e);
		}
		if (optcolumn) {
			CartoSQL::ExprTable::const_ptr_t e(layer.get_sqlexpr()->remove_unused_columns(&m_sql));
			if (e)
				layer.set_sqlexpr(e);
		}
		if (optfilter) {
			CartoSQL::ExprTable::const_ptr_t e(layer.get_sqlexpr()->push_filter_up(&m_sql, CartoSQL::Expr::const_ptr_t()));
			if (e)
				layer.set_sqlexpr(e);
		}
	}
}
