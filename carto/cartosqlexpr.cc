//
// C++ Implementation: cartosqlexpr
//
// Description: CartoSQL Expression implementation
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cmath>

#include "geom.h"
#include "cartosql.h"

#include <boost/algorithm/string/predicate.hpp>

CartoSQL::Error::Error(const std::string& x)
	: std::runtime_error(x)
{
}

const std::string& to_str(CartoSQL::Expr::type_t t)
{
	switch (t) {
	case CartoSQL::Expr::type_t::value:
	{
		static const std::string r("value");
		return r;
	}

	case CartoSQL::Expr::type_t::variable:
	{
		static const std::string r("variable");
		return r;
	}

	case CartoSQL::Expr::type_t::function:
	{
		static const std::string r("function");
		return r;
	}

	case CartoSQL::Expr::type_t::funcaggregate:
	{
		static const std::string r("funcaggregate");
		return r;
	}

	case CartoSQL::Expr::type_t::functable:
	{
		static const std::string r("functable");
		return r;
	}

	case CartoSQL::Expr::type_t::case_:
	{
		static const std::string r("case");
		return r;
	}

	case CartoSQL::Expr::type_t::cast:
	{
		static const std::string r("cast");
		return r;
	}

	case CartoSQL::Expr::type_t::tagmember:
	{
		static const std::string r("tagmember");
		return r;
	}

	case CartoSQL::Expr::type_t::column:
	{
		static const std::string r("column");
		return r;
	}

	case CartoSQL::Expr::type_t::tablecolumn:
	{
		static const std::string r("tablecolumn");
		return r;
	}

	case CartoSQL::Expr::type_t::osmspecial:
	{
		static const std::string r("osmspecial");
		return r;
	}

	case CartoSQL::Expr::type_t::osmtag:
	{
		static const std::string r("osmtag");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

CartoSQL::Expr::Expr(void)
	: m_refcount(0)
{
}

CartoSQL::Expr::~Expr()
{
}

bool CartoSQL::Expr::calculate(Calc& c, Value& v) const
{
	v.set_null();
	return false;
}

CartoSQL::Expr::const_ptr_t CartoSQL::Expr::simplify(const CartoSQL *csql) const
{
	Calculate c(csql);
	Value v;
	if (!calculate(c, v))
		return const_ptr_t();
	return const_ptr_t(new ExprValue(v));
}

CartoSQL::Expr::const_ptr_t CartoSQL::Expr::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	Calculate c(csql);
	Value val;
	if (!calculate(c, val)) {
		const_ptr_t p1(visitor_call_modify(csql, v));
		v.visitend(*this);
		return p1;
	}
	const_ptr_t p1(new ExprValue(val));
	const_ptr_t p2(v.modify(csql, *p1));
	v.visitend(*this);
	return p2 ? p2 : p1;
}

bool CartoSQL::Expr::is_false(void) const
{
	Value v;
	if (!is_type(v))
		return false;
	if (v.is_null())
		return true;
	if (!v.is_boolean())
		return false;
	return !v.get_boolean();
}

bool CartoSQL::Expr::is_true(void) const
{
	Value v;
	if (!is_type(v))
		return false;
	if (!v.is_boolean())
		return false;
	return v.get_boolean();
}

CartoSQL::Value CartoSQL::Expr::as_value(void) const
{
	Value v;
	if (is_type(v))
		return v;
	std::ostringstream oss;
	print(oss, 0);
	throw Error("expression " + oss.str() + " is not a Value");
}

int CartoSQL::Expr::compare(const Expr& x) const
{
	type_t t0(get_type()), t1(x.get_type());
	if (t0 < t1)
		return -1;
	if (t1 < t0)
		return 1;
        return 0;
}

int CartoSQL::Expr::compare(const const_ptr_t& p0, const const_ptr_t& p1)
{
	if (p0) {
		if (p1) {
			int c(p0->compare(*p1));
			if (c)
				return c;
		} else {
			return 1;
		}
	} else {
		if (p1)
			return -1;
	}
	return 0;
}

int CartoSQL::Expr::compare(const exprlist_t& e0, const exprlist_t& e1)
{
	exprlist_t::size_type i(e1.size()), n(e0.size());
	if (n < i)
		return -1;
	if (i < n)
		return 1;
	for (i = 0; i < n; ++i) {
		int c(compare(e0[i], e1[i]));
		if (c)
			return c;
	}
	return 0;
}

class CartoSQL::Expr::AggregateVisitor : public Visitor {
public:
	AggregateVisitor(void) : m_aggregate(false) {}
	bool is_aggregate(void) const { return m_aggregate; }
	virtual void visit(const ExprFuncAggregate& e) { m_aggregate = true; }
	virtual bool descend(const ExprTable& e) { return false; }

protected:
	bool m_aggregate;
};

bool CartoSQL::Expr::is_aggregate(void) const
{
	AggregateVisitor vis;
	visit(vis);
	return vis.is_aggregate();
}

CartoSQL::Expr::const_ptr_t CartoSQL::Expr::replace_variable(Calc& c) const
{
	ReplaceVariable vis(c);
	return simplify(c.get_csql(), vis);
}

void CartoSQL::Expr::split_and_terms(exprlist_t& el) const
{
	if (!is_true())
		el.push_back(get_ptr());
}

CartoSQL::Expr::const_ptr_t CartoSQL::Expr::combine_and_terms(const exprlist_t& el)
{
	const_ptr_t p;
	for (const const_ptr_t& p1 : el) {
		if (!p1)
			continue;
		if (!p) {
			p = p1;
			continue;
		}
		exprlist_t el1;
		el1.push_back(p);
		el1.push_back(p1);
		p = Expr::ptr_t(new ExprFunc(ExprFunc::func_t::logicaland, el1));
	}
	return p;
}

std::ostream& CartoSQL::Expr::print_expr(std::ostream& os, const const_ptr_t& e, unsigned int indent, const PrintContext& ctx, bool paren)
{
	if (!e)
		return os << "?unsupported?";
	paren = paren && e->is_paren();
	if (paren)
		os << '(';
	e->print(os, indent, ctx);
	if (paren)
		os << ')';
	return os;
}

std::ostream& CartoSQL::Expr::print_expr(std::ostream& os, const exprlist_t& e, unsigned int indent, const PrintContext& ctx)
{
	bool subseq(false);
	for (const const_ptr_t& expr : e) {
		if (subseq)
			os << ", ";
		subseq = true;
		print_expr(os, expr, indent, ctx, false);
	}
	return os;
}

CartoSQL::Expr::const_ptr_t CartoSQL::ExprValue::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p(visitor_call_modify(csql, v));
	v.visitend(*this);
	return p;
}

std::ostream& CartoSQL::ExprValue::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	return os << m_value.get_sqlstring();
}

int CartoSQL::ExprValue::compare(const Expr& x) const
{
	int c(Expr::compare(x));
	if (c)
		return c;
	const ExprValue& xx(static_cast<const ExprValue&>(x));
	return m_value.compare(xx.m_value);
}

CartoSQL::Expr::const_ptr_t CartoSQL::ExprVariable::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p(visitor_call_modify(csql, v));
	v.visitend(*this);
	return p;
}

bool CartoSQL::ExprVariable::calculate(Calc& c, Value& v) const
{
	if (c.get_variable(v, get_variable()))
		return true;
	c.error(*this, "variable not found");
	return false;
}

std::ostream& CartoSQL::ExprVariable::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	return os << '!' << m_variable << '!';
}

int CartoSQL::ExprVariable::compare(const Expr& x) const
{
	int c(Expr::compare(x));
	if (c)
		return c;
	const ExprVariable& xx(static_cast<const ExprVariable&>(x));
	return m_variable.compare(xx.m_variable);
}

int CartoSQL::ExprCase::When::compare(const When& x) const
{
	int c(Expr::compare(get_expr(), x.get_expr()));
	if (c)
		return c;
	return Expr::compare(get_value(), x.get_value());
}

CartoSQL::Value::type_t CartoSQL::ExprCase::When::calculate_type(const subtables_t& subtables) const
{
	if (!get_value())
		return Value::type_t::null;
	return get_value()->calculate_type(subtables);
}

bool CartoSQL::ExprCase::calculate(Calc& c, Value& v) const
{
	if (m_expr) {
		ExprFunc::valuelist_t val;
		val.resize(2);
		if (!m_expr->calculate(c, val[0])) {
			c.error(*this, "case argument error");
			return false;
		}
		for (const When& w : m_when) {
			if (!w.get_expr() || !w.get_value() || !w.get_expr()->calculate(c, val[1])) {
				c.error(*this, "condition argument error");
				return false;
			}
			Value vres;
			if (!ExprFunc::calculate(vres, ExprFunc::func_t::equal, val)) {
				c.error(*this, "condition comparison error");
				return false;
			}
			if (!vres.is_boolean() || !vres.get_boolean())
				continue;
			if (w.get_value()->calculate(c, v))
				return true;
			c.error(*this, "condition value error");
			return false;
		}
	} else {
		for (const When& w : m_when) {
			Value vres;
			if (!w.get_expr() || !w.get_value() || !w.get_expr()->calculate(c, vres)) {
				c.error(*this, "condition argument error");
				return false;
			}
			if (vres.is_null())
				continue;
			if (!vres.is_boolean()) {
				c.error(*this, "condition is not boolean");
				return false;
			}
			if (!vres.get_boolean())
				continue;
			if (w.get_value()->calculate(c, v))
				return true;
			c.error(*this, "condition value error");
			return false;
		}
	}
	if (!get_else())
		return true;
	if (get_else()->calculate(c, v))
		return true;
	c.error(*this, "else value error");
	return false;
}

CartoSQL::Value::type_t CartoSQL::ExprCase::calculate_type(const subtables_t& subtables) const
{
	Value::type_t t(Value::type_t::null);
	if (get_else()) {
		t = get_else()->calculate_type(subtables);
		if (t == Value::type_t::null)
			return Value::type_t::null;
	}
	for (const When& w : m_when) {
		Value::type_t tx(w.calculate_type(subtables));
		if (tx == Value::type_t::null)
			return tx;
		if (t == Value::type_t::null || (tx == Value::type_t::floatingpoint && t == Value::type_t::integer)) {
			t = tx;
			continue;
		}
		if (tx == Value::type_t::integer && t == Value::type_t::floatingpoint)
			continue;
		if (tx != t)
			return Value::type_t::null;
	}
	return t;
}

template <typename... Args>
CartoSQL::Expr::const_ptr_t CartoSQL::ExprCase::simplify_int(const CartoSQL *csql, Args&&... args) const
{
	bool work(false);
	boost::intrusive_ptr<ExprCase> case_(new ExprCase());
	if (get_expr()) {
		case_->set_expr(get_expr());
		const_ptr_t p(get_expr()->simplify(csql, std::forward<Args>(args)...));
		if (p) {
			case_->set_expr(p);
			work = true;
		}
	}
	if (get_else()) {
		case_->set_else(get_else());
		const_ptr_t p(case_->get_else()->simplify(csql, std::forward<Args>(args)...));
		if (p) {
			case_->set_else(p);
			work = true;
		}
	}
	for (const When& w : get_when()) {
		When ww;
		if (w.get_expr()) {
			ww.set_expr(w.get_expr());
			const_ptr_t p(w.get_expr()->simplify(csql, std::forward<Args>(args)...));
			if (p) {
				ww.set_expr(p);
				work = true;
			}
		}
		if (w.get_value()) {
			ww.set_value(w.get_value());
			const_ptr_t p(w.get_value()->simplify(csql, std::forward<Args>(args)...));
			if (p) {
				ww.set_value(p);
				work = true;
			}
		}
		case_->append_when(ww);
	}
	{
		Calculate c(csql);
		Value v;
		if (case_->calculate(c, v))
			return ptr_t(new ExprValue(v));
	}
	if (work)
		return case_;
	return const_ptr_t();
}

CartoSQL::Expr::const_ptr_t CartoSQL::ExprCase::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p;
	if (v.descend(*this))
		p = simplify_int(csql, v);
	const_ptr_t p1(p ? p->visitor_call_modify(csql, v) : visitor_call_modify(csql, v));
	v.visitend(*this);
	return p1 ? p1 : p;
}

void CartoSQL::ExprCase::visit(Visitor& v) const
{
	v.visit(*this);
	if (v.descend(*this)) {
		if (get_expr())
			get_expr()->visit(v);
		if (get_else())
			get_else()->visit(v);
		for (const When& w : get_when()) {
			if (w.get_expr())
				w.get_expr()->visit(v);
			if (w.get_value())
				w.get_value()->visit(v);
		}
	}
	v.visitend(*this);
}

std::ostream& CartoSQL::ExprCase::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	os << "case";
	if (get_expr())
		get_expr()->print(os << ' ', indent + 2, ctx);
	for (const When& w : get_when()) {
		w.get_expr()->print(os << std::endl << std::string(indent, ' ') << "when ", indent + 2, ctx);
		w.get_value()->print(os << " then ", indent + 2, ctx);
	}
	if (get_else())
		get_else()->print(os << std::endl << std::string(indent, ' ') << "else ", indent + 2, ctx);
	return os << " end";
}

int CartoSQL::ExprCase::compare(const Expr& x) const
{
	int c(Expr::compare(x));
	if (c)
		return c;
	const ExprCase& xx(static_cast<const ExprCase&>(x));
	c = Expr::compare(get_expr(), xx.get_expr());
	if (c)
		return c;
	{
		when_t::size_type i(xx.get_when().size()), n(get_when().size());
		if (n < i)
			return -1;
		if (i < n)
			return 1;
		for (i = 0; i < n; ++i) {
			int c(get_when()[i].compare(xx.get_when()[i]));
			if (c)
				return c;
		}
	}
	return Expr::compare(get_else(), xx.get_else());
}

void CartoSQL::ExprOneChild::visit(Visitor& v) const
{
	if (get_expr())
		get_expr()->visit(v);
}

std::ostream& CartoSQL::ExprOneChild::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	if (get_expr())
		get_expr()->print(os, indent, ctx);
	else
		os << "?unsupported?";
	return os;
}

int CartoSQL::ExprOneChild::compare(const Expr& x) const
{
	int c(Expr::compare(x));
	if (c)
		return c;
	const ExprOneChild& xx(static_cast<const ExprOneChild&>(x));
	return Expr::compare(get_expr(), xx.get_expr());
}

CartoSQL::Expr::const_ptr_t CartoSQL::ExprCast::simplify_tail(const CartoSQL *csql, const const_ptr_t& expr, Value::type_t typ, bool modif)
{
	{
		Calculate c(csql);
		Value v;
		if (calculate(c, v, expr, typ, nullptr))
			return ptr_t(new ExprValue(v));
	}
	if (modif)
		return ptr_t(new ExprCast(expr, typ));
	return ptr_t();
}

template <typename... Args>
CartoSQL::Expr::const_ptr_t CartoSQL::ExprCast::simplify_int(const CartoSQL *csql, Args&&... args) const
{
	bool work(false);
	const_ptr_t expr;
	if (m_expr) {
		expr = m_expr->simplify(csql, std::forward<Args>(args)...);
		if (expr)
			work = true;
		else
			expr = m_expr;
	}
	return simplify_tail(csql, expr, m_type, work);
}

CartoSQL::Expr::const_ptr_t CartoSQL::ExprCast::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p;
	if (v.descend(*this))
		p = simplify_int(csql, v);
	const_ptr_t p1(p ? p->visitor_call_modify(csql, v) : visitor_call_modify(csql, v));
	v.visitend(*this);
	return p1 ? p1 : p;
}

void CartoSQL::ExprCast::visit(Visitor& v) const
{
	v.visit(*this);
	if (v.descend(*this))
		ExprOneChild::visit(v);
	v.visitend(*this);
}

bool CartoSQL::ExprCast::calculate(Calc& c, Value& v, const const_ptr_t& expr, Value::type_t typ, const Expr *expr1)
{
	if (!expr) {
		v.set_null();
		return true;
	}
	if (!expr->calculate(c, v)) {
		if (expr1)
			c.error(*expr1, "argument error");
		return false;
	}
	if (v.is_null())
		return true;
	Value v1(v.as_type(typ));
	if (!v1.is_null()) {
		v = std::move(v1);
		return true;
	}
	if (expr1)
		c.error(*expr1, v, "cannot cast argument to type");
	return false;
}

std::ostream& CartoSQL::ExprCast::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	return ExprOneChild::print(os << "cast(", indent, ctx)
		<< " as " << Value::get_sqltypename(get_datatype()) << ')';
}

int CartoSQL::ExprCast::compare(const Expr& x) const
{
	int c(ExprOneChild::compare(x));
	if (c)
		return c;
	const ExprCast& xx(static_cast<const ExprCast&>(x));
	if (get_datatype() < xx.get_datatype())
		return -1;
	if (xx.get_datatype() < get_datatype())
		return 1;
	return 0;
}

CartoSQL::Expr::const_ptr_t CartoSQL::ExprTagMember::simplify_tail(const CartoSQL *csql, const const_ptr_t& expr, const std::string& key, const std::string& value,
								   OSMDB::Object::tagkey_t tagkey, OSMDB::Object::tagname_t tagname, tag_t tag, bool modif)
{
	{
		Calculate c(csql);
		Value v;
		if (calculate(c, v, expr, key, value, tagkey, tagname, tag, nullptr))
			return ptr_t(new ExprValue(v));
	}
	if (expr && expr->get_type() == type_t::osmspecial && csql) {
		const OSMDB *osmdb(csql->get_osmdb());
		if (osmdb) {
			const ExprOSMSpecial& e(static_cast<const ExprOSMSpecial&>(*expr));
			if (e.get_field() == ExprOSMSpecial::field_t::tags) {
				ExprOSMTag::tagkey_t tkey(osmdb->find_tagkey(key));
				ExprOSMTag::tagname_t tvalue(ExprOSMTag::tagname_t::invalid);
				switch (tag) {
				case tag_t::value:
					if (tkey == ExprOSMTag::tagkey_t::invalid)
						return Expr::const_ptr_t(new ExprValue(Value()));
					return Expr::const_ptr_t(new ExprOSMTag(tag, tkey, tvalue));

				case tag_t::exists:
					if (tkey == ExprOSMTag::tagkey_t::invalid)
						return Expr::const_ptr_t(new ExprValue(Value(false)));
					return Expr::const_ptr_t(new ExprOSMTag(tag, tkey, tvalue));

				case tag_t::isequal:
					tvalue = osmdb->find_tagname(value);
					if (tkey == ExprOSMTag::tagkey_t::invalid ||
					    tvalue == ExprOSMTag::tagname_t::invalid)
						return Expr::const_ptr_t(new ExprValue(Value(false)));
					return Expr::const_ptr_t(new ExprOSMTag(tag, tkey, tvalue));

				default:
					break;
				}
			}
		}
	}
	if (modif)
		return ptr_t(new ExprTagMember(expr, tag, key, value, tagkey, tagname));
	return ptr_t();
}

template <typename... Args>
CartoSQL::Expr::const_ptr_t CartoSQL::ExprTagMember::simplify_int(const CartoSQL *csql, Args&&... args) const
{
	bool work(false);
	const_ptr_t expr;
	if (m_expr) {
		expr = m_expr->simplify(csql, std::forward<Args>(args)...);
		if (expr)
			work = true;
		else
			expr = m_expr;
	}
	return simplify_tail(csql, expr, m_key, m_value, m_tagkey, m_tagname, m_tag, work);
}

CartoSQL::Expr::const_ptr_t CartoSQL::ExprTagMember::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p;
	if (v.descend(*this))
		p = simplify_int(csql, v);
	const_ptr_t p1(p ? p->visitor_call_modify(csql, v) : visitor_call_modify(csql, v));
	v.visitend(*this);
	return p1 ? p1 : p;
}

void CartoSQL::ExprTagMember::visit(Visitor& v) const
{
	v.visit(*this);
	if (v.descend(*this))
		ExprOneChild::visit(v);
	v.visitend(*this);
}

bool CartoSQL::ExprTagMember::calculate(Calc& c, Value& v, const const_ptr_t& expr, const std::string& key, const std::string& value,
					OSMDB::Object::tagkey_t tagkey, OSMDB::Object::tagname_t tagname, tag_t tag, const Expr *expr1)
{
	v.set_null();
	if (!expr)
		return true;
	Value val;
	if (!expr->calculate(c, val)) {
		if (expr1)
			c.error(*expr1, "argument error");
		return false;
	}
	switch (val.get_type()) {
	case Value::type_t::null:
		return true;

	case Value::type_t::tags:
	{
		const Value::tags_t& tags(val.get_tags());
		Value::tags_t::const_iterator i(tags.find(key));
		switch (tag) {
		case tag_t::value:
			if (i != tags.end())
				v = i->second;
			break;

		case tag_t::exists:
			v = i != tags.end();
			break;

		case tag_t::isequal:
			if (i == tags.end()) {
				v = false;
				break;
			}
			v =!i->second.compare_value(Value(value));
			break;

		default:
			if (expr1)
				c.error(*expr1, "invalid tag operation");
			return false;
		}
		if (false)
			std::cerr << "ExprTagMember::calculate: tags " << val.get_sqlstring(Value::sqlstringflags_t::all)
				  << " key \"" << key << "\" value \"" << value << "\" tag " << tag
				  << " found " << (i == tags.end() ? "no" : "yes")
				  << " return " << v.get_sqlstring(Value::sqlstringflags_t::all) << std::endl;
		return true;
	}

	case Value::type_t::osmdbobject:
	{
		OSMDB::Object::tagname_t tagnm(val.get_osmdbobject()->find_tag(tagkey));
		switch (tag) {
		case tag_t::value:
			if (tagkey == OSMDB::Object::tagkey_t::invalid || tagnm == OSMDB::Object::tagname_t::invalid) {
				v.set_null();
				return true;
			}
			{
				if (!c.get_csql())
					return false;
				const OSMDB *osmdb(c.get_csql()->get_osmdb());
				if (!osmdb)
					return false;
				const char *cp(osmdb->find_tagname(tagnm));
				if (cp)
					v = cp;
				else
					v.set_null();
			}
			return true;

		case tag_t::exists:
			v = tagkey != OSMDB::Object::tagkey_t::invalid && tagnm != OSMDB::Object::tagname_t::invalid;
			return true;

		case tag_t::isequal:
			if (tagkey == OSMDB::Object::tagkey_t::invalid || tagnm == OSMDB::Object::tagname_t::invalid) {
				v = false;
				return true;
			}
			v = tagnm == tagname;
			return true;

		default:
			if (expr1)
				c.error(*expr1, "invalid tag operation");
			return false;
		}
	}

	default:
		if (expr1)
			c.error(*expr1, v, "argument has wrong type");
		return false;
	}
}

CartoSQL::Value::type_t CartoSQL::ExprTagMember::calculate_type(const subtables_t& subtables) const
{
	switch (get_tag()) {
	case tag_t::value:
		return Value::type_t::null;

	case tag_t::exists:
	case tag_t::isequal:
		return Value::type_t::boolean;

	default:
		return Value::type_t::null;
	}
}

std::ostream& CartoSQL::ExprTagMember::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	ExprOneChild::print(os, indent, ctx) << get_tag() << '\'' << get_key();
	if (get_tag() == tag_t::isequal)
		os << "=>" << get_value();
	return os << '\'';
}

int CartoSQL::ExprTagMember::compare(const Expr& x) const
{
	int c(ExprOneChild::compare(x));
	if (c)
		return c;
	const ExprTagMember& xx(static_cast<const ExprTagMember&>(x));
	if (get_tag() < xx.get_tag())
		return -1;
	if (xx.get_tag() < get_tag())
		return 1;
	c = get_key().compare(xx.get_key());
	if (c)
		return c;
	c = get_value().compare(xx.get_value());
	if (c)
		return c;
	return 0;
}

const std::string& to_str(CartoSQL::ExprColumn::record_t x)
{
	switch (x) {
	case CartoSQL::ExprColumn::record_t::none:
	{
		static const std::string r("-");
		return r;
	}

	case CartoSQL::ExprColumn::record_t::member:
	{
		static const std::string r("->");
		return r;
	}

	case CartoSQL::ExprColumn::record_t::exists:
	{
		static const std::string r("?");
		return r;
	}

	case CartoSQL::ExprColumn::record_t::equalsvalue:
	{
		static const std::string r("@>");
		return r;
	}

	case CartoSQL::ExprColumn::record_t::invalid:
	default:
	{
		static const std::string r("??");
		return r;
	}
	}
}

CartoSQL::Expr::const_ptr_t CartoSQL::ExprColumn::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p(visitor_call_modify(csql, v));
	v.visitend(*this);
	return p;
}

bool CartoSQL::ExprColumn::calculate(Calc& c, Value& v) const
{
	v.set_null();
	c.error(*this, "not implemented");
	return false;
}

std::ostream& CartoSQL::ExprColumn::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	static constexpr bool markcol = !false;
	if (markcol)
		os << "?column:";
	if (!get_schema().empty())
		os << get_schema() << '.';
	if (!get_table().empty())
		os << get_table() << '.';
	os << get_column();
	if (get_record() != record_t::none) {
		os << get_record();
		std::string x(get_member());
		if (get_record() == record_t::equalsvalue) {
			x.append("=>");
			x.append(get_value());
		}
		os << Value(x).get_sqlstring();
	}
	if (markcol)
		os << '?';
	return os;
}

int CartoSQL::ExprColumn::compare(const Expr& x) const
{
	int c(Expr::compare(x));
	if (c)
		return c;
	const ExprColumn& xx(static_cast<const ExprColumn&>(x));
	c = get_schema().compare(xx.get_schema());
	if (c)
		return c;
	c = get_table().compare(xx.get_table());
	if (c)
		return c;
	c = get_column().compare(xx.get_column());
	if (c)
		return c;
	if (get_record() < xx.get_record())
		return -1;
	if (xx.get_record() < get_record())
		return 1;
	c = get_member().compare(xx.get_member());
	if (c)
		return c;
	return get_value().compare(xx.get_value());
}

CartoSQL::Expr::const_ptr_t CartoSQL::ExprTableColumn::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p(visitor_call_modify(csql, v));
	v.visitend(*this);
	return p;
}

bool CartoSQL::ExprTableColumn::calculate(Calc& c, Value& v) const
{
	const TableRow *tr(c.get_table_row(get_tableindex()));
	if (!tr) {
		c.error(*this, "cannot get table row");
		v.set_null();
		return false;
	}
	if (get_columnindex() >= tr->size()) {
		c.error(*this, "column index out of range");
		v.set_null();
		return false;
	}
	v = (*tr)[get_columnindex()];
	return true;
}

CartoSQL::Value::type_t CartoSQL::ExprTableColumn::calculate_type(const subtables_t& subtables) const
{
	if (get_tableindex() >= subtables.size() ||
	    !subtables[get_tableindex()] ||
	    get_columnindex() >= subtables[get_tableindex()]->get_columns().size())
		return Value::type_t::null;
	return subtables[get_tableindex()]->get_columns()[get_columnindex()].get_type();
}

std::ostream& CartoSQL::ExprTableColumn::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	os << ctx.get_tablecolumn(get_tableindex(), get_columnindex());
	if (false)
		os << '?' << get_tableindex() << '.' << get_columnindex() << '?';
	return os;
}

int CartoSQL::ExprTableColumn::compare(const Expr& x) const
{
	int c(Expr::compare(x));
	if (c)
		return c;
	const ExprTableColumn& xx(static_cast<const ExprTableColumn&>(x));
	if (get_tableindex() < xx.get_tableindex())
		return -1;
	if (xx.get_tableindex() < get_tableindex())
		return 1;
	if (get_columnindex() < xx.get_columnindex())
		return -1;
	if (xx.get_columnindex() < get_columnindex())
		return 1;
	return 0;
}

const std::string& to_str(CartoSQL::ExprOSMSpecial::field_t x)
{
	switch (x) {
	case CartoSQL::ExprOSMSpecial::field_t::osmid:
	{
		static const std::string r("osm_id");
		return r;
	}

	case CartoSQL::ExprOSMSpecial::field_t::zorder:
	{
		static const std::string r("z_order");
		return r;
	}

	case CartoSQL::ExprOSMSpecial::field_t::tags:
	{
		static const std::string r("tags");
		return r;
	}

	case CartoSQL::ExprOSMSpecial::field_t::wayarea:
	{
		static const std::string r("way_area");
		return r;
	}

	case CartoSQL::ExprOSMSpecial::field_t::waytruearea:
	{
		static const std::string r("way_true_area");
		return r;
	}

	case CartoSQL::ExprOSMSpecial::field_t::wayangulararea:
	{
		static const std::string r("way_angular_area");
		return r;
	}

	case CartoSQL::ExprOSMSpecial::field_t::way:
	{
		static const std::string r("way");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

CartoSQL::Expr::const_ptr_t CartoSQL::ExprOSMSpecial::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p(visitor_call_modify(csql, v));
	v.visitend(*this);
	return p;
}

bool CartoSQL::ExprOSMSpecial::calculate(Calc& c, Value& v) const
{
	static constexpr bool return_osmdbobject = false;
	const OSMDB::Object *osmobj(c.get_osmobject());
	if (!osmobj) {
		c.error(*this, "cannot get OSM object");
		v.set_null();
		return false;
	}
	switch (get_field()) {
	case field_t::osmid:
	{
		v = static_cast<int64_t>(osmobj->get_id());
		return true;
	}

	case field_t::zorder:
	{
		v = static_cast<int64_t>(osmobj->get_zorder());
		return true;
	}

	case field_t::tags:
	{
		if (return_osmdbobject) {
			v = osmobj->get_ptr();
			return true;
		}
		if (!c.get_csql()) {
			c.error(*this, "CartoSQL pointer null");
			v.set_null();
			return false;
		}
		const OSMDB *osmdb(c.get_csql()->get_osmdb());
		if (!osmdb) {
			c.error(*this, "no underlying OSM database");
			v.set_null();
			return false;
		}
		Value::tags_t tags;
		for (const auto& t : osmobj->get_tags()) {
			const char *key(osmdb->find_tagkey(std::get<OSMDB::Object::tagkey_t>(t)));
			if (!key)
				continue;
			const char *name(osmdb->find_tagname(std::get<OSMDB::Object::tagname_t>(t)));
			if (name) {
				tags[key] = Value(name);
				continue;
			}
			tags[key].set_null();
		}
		v = std::move(tags);
		return true;
	}

	case field_t::wayarea:
		switch (osmobj->get_type()) {
		case OSMDB::ObjPoint::object_type:
		case OSMDB::ObjLine::object_type:
		case OSMDB::ObjRoad::object_type:
			v = 0.0;
			return true;

		case OSMDB::ObjArea::object_type:
		{
			static constexpr double to_nmi(Point::to_deg_dbl * 60);
			static constexpr double to_m(to_nmi * 1e3 / Point::km_to_nmi_dbl);
			static constexpr double scale(0.5 * to_m * to_m);
			MultiPolygonHole mph(static_cast<const OSMDB::ObjArea *>(osmobj)->get_area());
			mph.transform_webmercator();
			v = scale * std::abs(mph.area2());
			return true;
		}

		default:
			c.error(*this, "unknown object type");
			v.set_null();
			return false;
		}

	case field_t::waytruearea:
		switch (osmobj->get_type()) {
		case OSMDB::ObjPoint::object_type:
		case OSMDB::ObjLine::object_type:
		case OSMDB::ObjRoad::object_type:
			v = 0.0;
			return true;

		case OSMDB::ObjArea::object_type:
			v = 1e6 * std::abs(static_cast<const OSMDB::ObjArea *>(osmobj)->get_area().spherical_area());
			return true;

		default:
			c.error(*this, "unknown object type");
			v.set_null();
			return false;
		}

	case field_t::wayangulararea:
		switch (osmobj->get_type()) {
		case OSMDB::ObjPoint::object_type:
		case OSMDB::ObjLine::object_type:
		case OSMDB::ObjRoad::object_type:
			v = 0.0;
			return true;

		case OSMDB::ObjArea::object_type:
		{
			static constexpr double scale(0.5 * Point::to_deg_dbl * Point::to_deg_dbl);
			v = scale * std::abs(static_cast<const OSMDB::ObjArea *>(osmobj)->get_area().area2());
			return true;
		}

		default:
			c.error(*this, "unknown object type");
			v.set_null();
			return false;
		}

	case field_t::way:
		if (return_osmdbobject) {
			v = osmobj->get_ptr();
			return true;
		}
		switch (osmobj->get_type()) {
		case OSMDB::ObjPoint::object_type:
		{
			MultiPoint mp;
			mp.push_back(static_cast<const OSMDB::ObjPoint *>(osmobj)->get_location());
			v = std::move(mp);
			return true;
		}

		case OSMDB::ObjLine::object_type:
		case OSMDB::ObjRoad::object_type:
		{
			MultiLineString mls;
			mls.push_back(static_cast<const OSMDB::ObjLine *>(osmobj)->get_line());
			v = std::move(mls);
			return true;
		}

		case OSMDB::ObjArea::object_type:
			v = static_cast<const OSMDB::ObjArea *>(osmobj)->get_area();
			return true;

		default:
			c.error(*this, "unknown object type");
			v.set_null();
			return false;
		}

	default:
		c.error(*this, "not implemented");
		v.set_null();
		return false;
	}
}

CartoSQL::Value::type_t CartoSQL::ExprOSMSpecial::get_valuetype(field_t fld)
{
	switch (fld) {
	case field_t::osmid:
	case field_t::zorder:
		return Value::type_t::integer;

	case field_t::tags:
		return Value::type_t::tags;

	case field_t::wayarea:
	case field_t::waytruearea:
	case field_t::wayangulararea:
		return Value::type_t::floatingpoint;

	case field_t::way:
		// FIXME: need to know type
		return Value::type_t::null;

	default:
		return Value::type_t::null;
	}
}

CartoSQL::Value::type_t CartoSQL::ExprOSMSpecial::get_valuetype(field_t fld, OSMDB::Object::type_t osmt)
{
	if (fld != field_t::way)
		return get_valuetype(fld);
	switch (osmt) {
	case OSMDB::Object::type_t::point:
		return Value::type_t::points;

	case OSMDB::Object::type_t::line:
	case OSMDB::Object::type_t::road:
		return Value::type_t::lines;

	case OSMDB::Object::type_t::area:
		return Value::type_t::polygons;

	default:
		return Value::type_t::null;
	}
}

std::ostream& CartoSQL::ExprOSMSpecial::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	return os << m_field;
}

int CartoSQL::ExprOSMSpecial::compare(const Expr& x) const
{
	int c(Expr::compare(x));
	if (c)
		return c;
	const ExprOSMSpecial& xx(static_cast<const ExprOSMSpecial&>(x));
	if (get_field() < xx.get_field())
		return -1;
	if (xx.get_field() < get_field())
		return 1;
	return 0;
}

bool CartoSQL::ExprOSMSpecial::parse_field(field_t& fld, const std::string& str)
{
	for (field_t f = field_t::first; f <= field_t::last;
	     f = static_cast<field_t>(static_cast<std::underlying_type<field_t>::type>(f) + 1)) {
		if (!boost::iequals(to_str(f), str))
			continue;
		fld = f;
		return true;
	}
	return false;
}

const std::string& to_str(CartoSQL::ExprOSMTag::tag_t x)
{
	switch (x) {
	case CartoSQL::ExprOSMTag::tag_t::value:
	{
		static const std::string r("->");
		return r;
	}

	case CartoSQL::ExprOSMTag::tag_t::exists:
	{
		static const std::string r("?");
		return r;
	}

	case CartoSQL::ExprOSMTag::tag_t::isequal:
	{
		static const std::string r("@>");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

CartoSQL::Expr::const_ptr_t CartoSQL::ExprOSMTag::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p(visitor_call_modify(csql, v));
	v.visitend(*this);
	return p;
}

bool CartoSQL::ExprOSMTag::calculate(Calc& c, Value& v) const
{
	const OSMDB::Object *osmobj(c.get_osmobject());
	if (!osmobj) {
		c.error(*this, "cannot get OSM object");
		v.set_null();
		return false;
	}
	tagname_t name(osmobj->find_tag(get_key()));
	switch (get_tag()) {
	case tag_t::value:
	{
		if (name == tagname_t::invalid) {
			v.set_null();
			return true;
		}
		if (!c.get_csql()) {
			c.error(*this, "CartoSQL pointer null");
			v.set_null();
			return false;
		}
		const OSMDB *osmdb(c.get_csql()->get_osmdb());
		if (!osmdb) {
			c.error(*this, "no underlying OSM database");
			v.set_null();
			return false;
		}
		const char *val(osmdb->find_tagname(name));
		if (val)
			v = Value(val);
		else
			v.set_null();
		return true;
	}

	case tag_t::exists:
		v = Value(name != tagname_t::invalid);
		return true;

	case tag_t::isequal:
		if (name == tagname_t::invalid) {
			v = false;
			return true;
		}
		v = Value(name == get_value());
		return true;

	default:
		c.error(*this, "not implemented");
		v.set_null();
		return false;
	}
}

CartoSQL::Value::type_t CartoSQL::ExprOSMTag::calculate_type(const subtables_t& subtables) const
{
	switch (get_tag()) {
	case tag_t::value:
		return Value::type_t::string;

	case tag_t::exists:
	case tag_t::isequal:
		return Value::type_t::boolean;

	default:
		return Value::type_t::null;
	}
}

std::ostream& CartoSQL::ExprOSMTag::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	os << "tags" << get_tag() << '\'';
	{
		const char *cp(ctx.get_osmkey(get_key()));
		if (cp)
			os << cp;
		else
			os << "?key:" << static_cast<uint32_t>(get_key()) << '?';
	}
	if (get_tag() == tag_t::isequal) {
		os << "=>";
		const char *cp(ctx.get_osmvalue(get_value()));
		if (cp)
			os << cp;
		else
			os << "?name:" << static_cast<uint32_t>(get_value()) << '?';
	}
	return os << '\'';
}

int CartoSQL::ExprOSMTag::compare(const Expr& x) const
{
	int c(Expr::compare(x));
	if (c)
		return c;
	const ExprOSMTag& xx(static_cast<const ExprOSMTag&>(x));
	if (get_tag() < xx.get_tag())
		return -1;
	if (xx.get_tag() < get_tag())
		return 1;
	if (get_key() < xx.get_key())
		return -1;
	if (xx.get_key() < get_key())
		return 1;
	if (get_value() < xx.get_value())
		return -1;
	if (xx.get_value() < get_value())
		return 1;
	return 0;
}
