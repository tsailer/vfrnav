//
// C++ Interface: cartorender
//
// Description: CartoCSS Renderer
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef CARTORENDER_HH
#define CARTORENDER_HH

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <boost/smart_ptr/intrusive_ptr.hpp>
#include <boost/variant.hpp>
#include <cairomm/cairomm.h>
#include <pangomm.h>

#include "carto.h"
#include "osmdb.h"
#include "osmsdb.h"
#include "dbobj.h"

class CartoRender {
public:
	enum class layerid_t : uint8_t {
		// keep order - same order as in project file, render order
		first,
		landcoverlowzoom = first,
		landcover,
		landcoverline,
		icesheetpoly,
		waterlinescasing,
		waterlineslowzoom,
		waterlines,
		waterareas,
		oceanlz,
		ocean,
		landcoverareasymbols,
		icesheetoutlines,
		marinasarea,
		waterbarriersline,
		waterbarrierspoly,
		pierspoly,
		piersline,
		waterbarrierspoint,
		bridge,
		buildings,
		tunnels,
		landuseoverlay,
		tourismboundary,
		barriers,
		cliffs,
		ferryroutes,
		turningcirclecasing,
		highwayareacasing,
		roadscasing,
		highwayareafill,
		roadsfill,
		turningcirclefill,
		aerialways,
		roadslowzoom,
		waterwaybridges,
		bridges,
		guideways,
		entrances,
		aeroways,
		necountries,
		adminlowzoom,
		adminmidzoom,
		adminhighzoom,
		powerminorline,
		powerline,
		protectedareas,
		trees,
		countrynames,
		capitalnames,
		statenames,
		placenamesmedium,
		placenamessmall,
		stations,
		junctions,
		bridgetext,
		countynames,
		amenitypoints,
		amenityline,
		powertowers,
		roadstextreflowzoom,
		roadstextref,
		roadsareatextname,
		roadstextname,
		pathstextname,
		railwaystextname,
		roadstextrefminor,
		textpolylowzoom,
		textline,
		textpoint,
		buildingtext,
		interpolation,
		addresses,
		waterlinestext,
		ferryroutestext,
		admintext,
		protectedareastext,
		amenitylowpriority,
		textlowpriority,
		last = textlowpriority,
		invalid = 0xff
	};

	typedef CartoCSS::zoom_t zoom_t;

	class Obstacle {
	public:
		Obstacle(const Point& pt = Point::invalid,
			 double agl = std::numeric_limits<double>::quiet_NaN(),
			 double amsl = std::numeric_limits<double>::quiet_NaN(),
			 bool marked = false, bool lighted = false,
			 const std::string& hyperlink = "");
		const Point& get_pt(void) const { return m_pt; }
		double get_agl(void) const { return m_agl; }
		double get_amsl(void) const { return m_amsl; }
		bool is_marked(void) const { return m_marked; }
		bool is_lighted(void) const { return m_lighted; }
		const std::string& get_hyperlink(void) const { return m_hyperlink; }

	protected:
		Point m_pt;
		double m_agl;
		double m_amsl;
		std::string m_hyperlink;
		bool m_marked;
		bool m_lighted;
	};

	typedef std::vector<Obstacle> obstacles_t;

	class CoordObstacle : public Obstacle {
	public:
		CoordObstacle(const Obstacle& ob = Obstacle(),
			      double x = std::numeric_limits<double>::quiet_NaN(),
			      double y = std::numeric_limits<double>::quiet_NaN());
		double get_x(void) const { return m_x; }
		double get_y(void) const { return m_y; }

	protected:
		double m_x;
		double m_y;
	};

	typedef std::vector<CoordObstacle> coordobstacles_t;

	CartoRender(void);
	CartoRender(const Carto& carto);

	const OSMDB *get_osmdb(void) const;
	const OSMStaticDB *get_osmsdb(void) const;
	const CartoCSS *get_style(void) const;
	const CartoSQL *get_sql(void) const;
	void set_carto(const Carto *carto);
	const Carto *get_carto(void) const { return m_carto; }

	void clear(void);
	void set_bbox(const Rect& bbox, double max_width_in_points = 841.89, double max_height_in_points = 595.28);
	const Rect& get_bbox(void) const { return m_bbox; }
	Rect get_extended_bbox(void) const;
	double get_width(void) const;
	double get_height(void) const;
	void set_arp(const Point& arp) { m_arp = arp; }
	const Point& get_arp(void) const { return m_arp; }
	void set_zoom(zoom_t z) { m_zoom = z; }
	zoom_t get_zoom(void) const { return m_zoom; }
	// zoom needs to be set before load because of static data
	void load(void);
	bool empty(void) const;
	static constexpr OSMDB::Object::id_t debugobject_off = std::numeric_limits<OSMDB::Object::id_t>::min();
	void set_debugobject(OSMDB::Object::id_t id = debugobject_off) { m_debugobject = id; }
	OSMDB::Object::id_t get_debugobject(void) const { return m_debugobject; }
	void set_cssdebuglevel(unsigned int l) { m_cssdebuglevel = l; }
	unsigned int get_cssdebuglevel(void) const { return m_cssdebuglevel; }

	static bool is_layer_enabled(layerid_t id, zoom_t zoom);
	bool is_layer_enabled(layerid_t id) const { return is_layer_enabled(id, get_zoom()); }

	const OSMDB::objects_t& get_points(void) const { return m_points; }
	const OSMDB::objects_t& get_lines(void) const { return m_lines; }
	const OSMDB::objects_t& get_roads(void) const { return m_roads; }
	const OSMDB::objects_t& get_areas(void) const { return m_areas; }
	const OSMStaticDB::objects_t& get_boundarylinesland(void) const { return m_boundarylinesland; }
	const OSMStaticDB::objects_t& get_icesheetpolygons(void) const { return m_icesheetpolygons; }
	const OSMStaticDB::objects_t& get_icesheetoutlines(void) const { return m_icesheetoutlines; }
	const OSMStaticDB::objects_t& get_waterpolygons(void) const { return m_waterpolygons; }
	const OSMStaticDB::objects_t& get_simplifiedwaterpolygons(void) const { return m_simplifiedwaterpolygons; }

	const obstacles_t& get_obstacles(void) const { return m_obstacles; }
	obstacles_t& get_obstacles(void) { return m_obstacles; }
	void set_obstacles(const obstacles_t& x) { m_obstacles = x; }
	coordobstacles_t draw(const Cairo::RefPtr<Cairo::Context>& cr,
			      const Cairo::RefPtr<Cairo::PdfSurface>& surface = Cairo::RefPtr<Cairo::PdfSurface>());
	coordobstacles_t drawsql(const Cairo::RefPtr<Cairo::Context>& cr,
				 const Cairo::RefPtr<Cairo::PdfSurface>& surface = Cairo::RefPtr<Cairo::PdfSurface>());

protected:
	class RenderObj {
	public:
		typedef boost::intrusive_ptr<RenderObj> ptr_t;
		typedef boost::intrusive_ptr<const RenderObj> const_ptr_t;
		typedef CartoCSS::Value fieldval_t;
		typedef CartoCSS::Fields fields_t;
		typedef OSMDB::Object::id_t id_t;
		typedef std::vector<id_t> ids_t;

		RenderObj(const fields_t& fields = fields_t(), id_t id = 0, int32_t zorder = 0);
		virtual ~RenderObj();

		const ids_t& get_ids(void) const { return m_ids; }
		void set_ids(const ids_t& ids) { m_ids = ids; }
		void set_ids(ids_t&& ids) { m_ids.swap(ids); }
		bool is_id(id_t id) const;
		bool is_id(id_t id0, id_t id1) const;
		bool is_id(const ids_t& ids) const { ids_t x(ids); return is_id_helper(x); }
		bool is_id(std::initializer_list<id_t> ids) const { ids_t x(ids); return is_id_helper(x); }
		void merge_ids(const ids_t& ids);
		std::string get_ids_str(void) const;
		id_t get_ids_hash(void) const;
		const fields_t& get_fields(void) const { return m_fields; }
		fields_t& get_fields(void) { return m_fields; }
		void set_fields(const fields_t& fields) { m_fields = fields; }
		void set_fields(fields_t&& fields) { m_fields.swap(fields); }
		void set_field(const std::string& fld, const char *val) { m_fields[fld] = val; }
		void set_field(const std::string& fld, const std::string& val) { m_fields[fld] = val; }
		void set_field(const std::string& fld, double val) { m_fields[fld] = val; }
		void set_field(const std::string& fld, int64_t val) { m_fields[fld] = val; }
		void set_field(const std::string& fld, bool val) { m_fields[fld] = val; }
		void set_field(const std::string& fld) { m_fields[fld] = fieldval_t(); }
		const fieldval_t& find_field(const std::string& key) const;
		void copy_field_string(const OSMDB& odb, const std::string& fld, OSMDB::Object::tagname_t val);
		void copy_field_string(const OSMDB& odb, const std::string& fld, OSMDB::Object::tagname_t val, const std::string& dflt);
		void copy_field_double(const OSMDB& odb, const std::string& fld, OSMDB::Object::tagname_t val);
		void copy_field_double(const OSMDB& odb, const std::string& fld, OSMDB::Object::tagname_t val, double dflt);
		void copy_field_int(const OSMDB& odb, const std::string& fld, OSMDB::Object::tagname_t val);
		void copy_field_int(const OSMDB& odb, const std::string& fld, OSMDB::Object::tagname_t val, int64_t dflt);
	        const CartoCSS::ClassID& get_classid(void) const { return m_classid; }
		void set_classid(const std::string& clsid) { m_classid = clsid; }
		int32_t get_zorder(void) const { return m_zorder; }
		void set_zorder(int32_t z) { m_zorder = z; }

		unsigned int breference(void) const { return ++m_refcount; }
		unsigned int bunreference(void) const { return --m_refcount; }
		unsigned int get_refcount(void) const { return m_refcount; }
		ptr_t get_ptr(void) { return ptr_t(this); }
		const_ptr_t get_ptr(void) const { return const_ptr_t(this); }
		friend inline void intrusive_ptr_add_ref(const RenderObj* expr) { expr->breference(); }
		friend inline void intrusive_ptr_release(const RenderObj* expr) { if (!expr->bunreference()) delete expr; }

        	std::string get_binary_attributes(void) const;
		virtual void coalesce_multilines(void) {}
		virtual void sortarea_multipolygons(void) {}
		virtual void draw(std::ostream *debugout, CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::ObjectParameters& par) const = 0;
		virtual std::ostream& print(std::ostream& os, unsigned int indent) const;

		template<class Archive> void hibernate_attr(Archive& ar) {
			m_fields.hibernate(ar);
			ar.ioleb(m_zorder);
			ar.io(static_cast<std::string&>(m_classid));
		}

		template<class Archive> void hibernate(Archive& ar) {
			{
				ids_t::size_type sz(m_ids.size());
				ar.ioleb(sz);
				if (ar.is_load())
					m_ids.resize(sz);
				for (ids_t::iterator i(m_ids.begin()), e(m_ids.end()); i != e; ++i)
					ar.ioleb(*i);
			}
			hibernate_attr(ar);
		}

	protected:
		fields_t m_fields;
		ids_t m_ids;
		CartoCSS::ClassID m_classid;
		mutable std::atomic<unsigned int> m_refcount;
		int32_t m_zorder;

		bool is_id_helper(ids_t& ids) const;
		static void set_line_context(const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::LineParams& par);
		bool draw_marker(double& width, double& height, CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr,
				 const CartoCSS::MarkerParams& par, double ptx, double pty, double rot = 0) const;
		void draw_marker(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::MarkerParams& par, const MultiPoint& coords) const;
		void draw_marker(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::MarkerParams& par, const LineString& coords) const;
		void draw_text(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::TextParams& par, const MultiPoint& coords) const;
		void draw_text(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::TextParams& par, const LineString& coords) const;
		void draw_shields(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::ShieldParams& par, const MultiPoint& coords) const;
		void draw_shields(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::ShieldParams& par, const MultiLineString& coords) const;
		void draw_linepattern(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::LinePatternParams& par, const LineString& coords) const;

	private:
		class TextLayout;
		class LineTextLayout;
	};

	class RenderObjPoints : public RenderObj {
	public:
		typedef boost::intrusive_ptr<RenderObjPoints> ptr_t;
		typedef boost::intrusive_ptr<const RenderObjPoints> const_ptr_t;

		RenderObjPoints(const MultiPoint& pts = MultiPoint(), const fields_t& fields = fields_t(), id_t id = 0, int32_t zorder = 0);
		virtual ~RenderObjPoints();

		const MultiPoint& get_points(void) const { return m_points; }
		MultiPoint& get_points(void) { return m_points; }
		void set_points(const MultiPoint& pts) { m_points = pts; }

		virtual void draw(std::ostream *debugout, CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::ObjectParameters& par) const;
		virtual std::ostream& print(std::ostream& os, unsigned int indent) const;

		template<class Archive> void hibernate(Archive& ar) {
			RenderObj::hibernate(ar);
			m_points.hibernate_binary(ar);
		}

	protected:
		MultiPoint m_points;
	};

	class RenderObjLines : public RenderObj {
	public:
		typedef boost::intrusive_ptr<RenderObjLines> ptr_t;
		typedef boost::intrusive_ptr<const RenderObjLines> const_ptr_t;

		RenderObjLines(const MultiLineString& ln = MultiLineString(), const fields_t& fields = fields_t(), id_t id = 0, int32_t zorder = 0);
		virtual ~RenderObjLines();

		const MultiLineString& get_lines(void) const { return m_lines; }
		MultiLineString& get_lines(void) { return m_lines; }
		void set_lines(const MultiLineString& ln) { m_lines = ln; }

		virtual void coalesce_multilines(void) { m_lines.join_adjoining(true); }
		virtual void draw(std::ostream *debugout, CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::ObjectParameters& par) const;
		virtual std::ostream& print(std::ostream& os, unsigned int indent) const;

		template<class Archive> void hibernate(Archive& ar) {
			RenderObj::hibernate(ar);
			m_lines.hibernate_binary(ar);
		}

	protected:
		MultiLineString m_lines;
	};

	class RenderObjPolygons : public RenderObj {
	public:
		typedef boost::intrusive_ptr<RenderObjPolygons> ptr_t;
		typedef boost::intrusive_ptr<const RenderObjPolygons> const_ptr_t;

		RenderObjPolygons(const MultiPolygonHole& p = MultiPolygonHole(), const fields_t& fields = fields_t(), id_t id = 0, int32_t zorder = 0);
		virtual ~RenderObjPolygons();

		const MultiPolygonHole& get_polygons(void) const { return m_polygons; }
		MultiPolygonHole& get_polygons(void) { return m_polygons; }
		void set_polygons(const MultiPolygonHole& p) { m_polygons = p; }

		virtual void sortarea_multipolygons(void) { m_polygons.sort_by_area_desc(); }
		virtual void draw(std::ostream *debugout, CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::ObjectParameters& par) const;
		virtual std::ostream& print(std::ostream& os, unsigned int indent) const;

		template<class Archive> void hibernate(Archive& ar) {
			RenderObj::hibernate(ar);
			m_polygons.hibernate_binary(ar);
		}

	protected:
		MultiPolygonHole m_polygons;
	};

	typedef std::vector<RenderObj::ptr_t> renderobjs_t;

	class RenderObjVisitor : public CartoCSS::Visitor {
	public:
		RenderObjVisitor(const RenderObj::const_ptr_t& ro) : m_obj(ro) {}
		virtual CartoCSS::Expr::const_ptr_t modify(const CartoCSS::Expr& e);

	protected:
		RenderObj::const_ptr_t m_obj;
	};

	class LayerAllocVisitor : public CartoCSS::Visitor {
	public:
		LayerAllocVisitor(CartoCSS::LayerAllocator& alloc) : m_alloc(alloc), m_curblock(nullptr) {}
		~LayerAllocVisitor();
		virtual void visit(const CartoCSS::Expr& e);
		virtual void visit(const CartoCSS::SubBlock& s);
		virtual void visitstatements(const CartoCSS::SubBlock& s);
		virtual void visitend(const CartoCSS::SubBlock& s);
		virtual void visit(const CartoCSS::VariableAssignments& v);

	protected:
		CartoCSS::LayerAllocator& m_alloc;
		typedef std::pair<std::string,const void *> stackel_t;
		typedef std::stack<stackel_t> stack_t;
		stack_t m_stack;
		const void *m_curblock;
	};

	class LayerSelectVisitor : public CartoCSS::Visitor {
	public:
		LayerSelectVisitor(const std::string& l = std::string()) : m_layer(l) {}
		virtual CartoCSS::Expr::const_ptr_t modify(const CartoCSS::Expr& e);

	protected:
		std::string m_layer;
	};

	class UnresolvedVisitor : public CartoCSS::Visitor {
	public:
		UnresolvedVisitor(void) : m_unresolved(false) {}
		virtual void visit(const CartoCSS::SubBlock& e);
		bool is_unresolved(void) const { return m_unresolved; }

	protected:
		bool m_unresolved;
	};

	class ApplyErrorVisitor : public CartoCSS::Statement::ApplyErrorVisitor {
	public:
		ApplyErrorVisitor(std::ostream *dbg) : m_dbg(dbg), m_error(false) {}
		virtual void error(const CartoCSS::VariableAssignments::Assignment& assignment);
		virtual void error(const CartoCSS::VariableAssignments::Assignment& assignment, const std::exception& e);
		virtual void error(const CartoCSS::SubBlock& s, const std::set<std::string>& layers);
		bool is_error(void) const { return m_error; }

	protected:
		std::ostream *m_dbg;
		bool m_error;
	};

	class Pattern {
	public:
		typedef boost::intrusive_ptr<Pattern> ptr_t;
		typedef boost::intrusive_ptr<const Pattern> const_ptr_t;

		Pattern(void);
		virtual ~Pattern();
		
		unsigned int breference(void) const { return ++m_refcount; }
		unsigned int bunreference(void) const { return --m_refcount; }
		unsigned int get_refcount(void) const { return m_refcount; }
		ptr_t get_ptr(void) { return ptr_t(this); }
		const_ptr_t get_ptr(void) const { return const_ptr_t(this); }
		friend inline void intrusive_ptr_add_ref(const Pattern* expr) { expr->breference(); }
		friend inline void intrusive_ptr_release(const Pattern* expr) { if (!expr->bunreference()) delete expr; }

		virtual bool set_context(double& width, double& height, const Cairo::RefPtr<Cairo::Context>& cr, bool repeat, bool center) { return false; }

	protected:
		mutable std::atomic<unsigned int> m_refcount;
	};

	class PatternSVG;
	class PatternPNG;

	class RPoint {
	public:
		RPoint(double x = 0, double y = 0) : m_x(x), m_y(y) {}
		double get_x(void) const { return m_x; }
		double get_y(void) const { return m_y; }
		void set_x(double x) { m_x = x; }
		void set_y(double y) { m_y = y; }
		double get_length2(void) const { return m_x * m_x + m_y * m_y; }
		double get_length(void) const { return sqrt(get_length2()); }
		double get_arg(void) const { return atan2(m_y, m_x); }
		RPoint get_unitvector(void) const { double invl(1.0 / get_length()); return RPoint(m_x * invl, m_y * invl); }
		bool operator==(const RPoint& z) const { return m_x == z.m_x && m_y == z.m_y; }
		bool operator!=(const RPoint& z) const { return !operator==(z); }
		RPoint& operator+=(const RPoint& z) { m_x += z.m_x; m_y += z.m_y; return *this; }
		RPoint& operator-=(const RPoint& z) { m_x -= z.m_x; m_y -= z.m_y; return *this; }
		RPoint& operator*=(double z) { m_x *= z; m_y *= z; return *this; }
		RPoint& operator/=(double z) { m_x /= z; m_y /= z; return *this; }
		RPoint operator+(const RPoint& z) const { return RPoint(m_x + z.m_x, m_y + z.m_y); }
		RPoint operator-(const RPoint& z) const { return RPoint(m_x - z.m_x, m_y - z.m_y); }
		RPoint operator-(void) const { return RPoint(-m_x, -m_y); }
		RPoint operator*(double z) const { return RPoint(m_x * z, m_y * z); }
		RPoint operator/(double z) const { return RPoint(m_x / z, m_y / z); }

		std::string to_str(void) const;

		static double area(const RPoint& p0, const RPoint& p1, const RPoint& p2);

	protected:
		double m_x;
		double m_y;
	};

	class RLine : public std::vector<RPoint> {
	public:
		class PolyParam {
		public:
			typedef Aircraft::Poly1D<double> Poly1D;
			PolyParam(const Poly1D& x, const Poly1D& y) : m_polyx(x), m_polyy(y) {}
			RPoint transform(const RPoint& p) const;
			void transform(double& rx, double& ry, double x, double y) const;
			void transform_path(const Cairo::RefPtr<Cairo::Context>& cr) const;
			bool is_valid(void) const { return m_polyx.size() >= 2 && m_polyy.size() >= 2; }

		protected:
			Poly1D m_polyx;
			Poly1D m_polyy;
		};

		double get_length(void) const;
		RPoint get_along(size_type& idx, double dist) const;
		RLine get_line_fraction(double distb, double diste) const;
		double get_max_segment_angle(void) const;
		void visvalingam_whyatt(double thr);
		void reverse(void) { std::reverse(begin(), end()); }
		PolyParam polyparameterize(unsigned int maxorder) const;
		PolyParam polyparameterize2(unsigned int maxorder, unsigned int extension) const;

		std::string to_str(void) const;
	};

	static constexpr bool log_missing_symbolizer = true;
	static constexpr double obstacle_cairo_radius = 3;
	static constexpr double obstacle_cairo_fontsize = 8;
	static constexpr bool obstacle_preferamsl = true;
	static constexpr double arp_cairo_radius = 3;
	static constexpr double points_per_inch = 72;
	static constexpr double pixels_per_inch = 90;

	const Carto *m_carto;
	Rect m_bbox;
	Point m_arp;
	zoom_t m_zoom;
	OSMDB::objects_t m_points;
	OSMDB::objects_t m_lines;
	OSMDB::objects_t m_roads;
	OSMDB::objects_t m_areas;
	OSMStaticDB::objects_t m_boundarylinesland;
	OSMStaticDB::objects_t m_icesheetpolygons;
	OSMStaticDB::objects_t m_icesheetoutlines;
	OSMStaticDB::objects_t m_waterpolygons;
	OSMStaticDB::objects_t m_simplifiedwaterpolygons;
	MultiPolygonHole m_collisions;
	typedef std::map<std::string,Pattern::ptr_t> patterncache_t;
	patterncache_t m_patterncache;
	typedef std::map<std::string,Pango::FontDescription> fontlist_t;
	fontlist_t m_fontlist;
	obstacles_t m_obstacles;
	double m_scale;
	double m_area2pixelscale;
	double m_yoffset;
	OSMDB::Object::id_t m_debugobject;
	unsigned int m_cssdebuglevel;

	class DbQueryNoSorter;
	class DbQueryWayPixelSorter;
	class DbQueryWayPixelFeatureSorter;
	class DbQueryAerowaysSorter;
	class DbQueryLayerSorter;
	class DbQueryRoadsBridgesSorter;
	class DbQueryLayerWayPixelSorter;
	class DbQueryIntegerSorter;
	class DbQueryZLayerNameIdSorter;
	class DbQueryZSorter;
	class DbQueryAdminLevelSorter;
	class DbQueryAdminLevelWayPixelSorter;
	class DbQueryScoreWayPixelSorter;
	class DbQueryRoadsTextSorter;
	class DbQueryTurningCircleSorter;
	class DbQueryScoreNameSorter;
	class SQLCalcError;

	renderobjs_t dbquery(layerid_t id) const;
	renderobjs_t dbquery_addresses(void) const;
	renderobjs_t dbquery_adminhighzoom(void) const;
	renderobjs_t dbquery_adminlowzoom(void) const;
	renderobjs_t dbquery_adminmidzoom(void) const;
	renderobjs_t dbquery_admintext(void) const;
	renderobjs_t dbquery_aerialways(void) const;
	renderobjs_t dbquery_aeroways(void) const;
	renderobjs_t dbquery_amenityline(void) const;
	renderobjs_t dbquery_amenitylowpriority(void) const;
	renderobjs_t dbquery_amenitypoints(void) const;
	renderobjs_t dbquery_barriers(void) const;
	renderobjs_t dbquery_bridge(void) const;
	renderobjs_t dbquery_bridges(void) const;
	renderobjs_t dbquery_bridgetext(void) const;
	renderobjs_t dbquery_buildings(void) const;
	renderobjs_t dbquery_buildingtext(void) const;
	renderobjs_t dbquery_capitalnames(void) const;
	renderobjs_t dbquery_cliffs(void) const;
	renderobjs_t dbquery_countrynames(void) const;
	renderobjs_t dbquery_countynames(void) const;
	renderobjs_t dbquery_entrances(void) const;
	renderobjs_t dbquery_ferryroutes(void) const;
	renderobjs_t dbquery_ferryroutestext(void) const;
	renderobjs_t dbquery_guideways(void) const;
	renderobjs_t dbquery_highwayareacasing(void) const;
	renderobjs_t dbquery_highwayareafill(void) const;
	renderobjs_t dbquery_icesheetoutlines(void) const;
	renderobjs_t dbquery_icesheetpoly(void) const;
	renderobjs_t dbquery_interpolation(void) const;
	renderobjs_t dbquery_junctions(void) const;
	renderobjs_t dbquery_landcover(void) const;
	renderobjs_t dbquery_landcoverareasymbols(void) const;
	renderobjs_t dbquery_landcoverline(void) const;
	renderobjs_t dbquery_landcoverlowzoom(void) const;
	renderobjs_t dbquery_landuseoverlay(void) const;
	renderobjs_t dbquery_marinasarea(void) const;
	renderobjs_t dbquery_necountries(void) const;
	renderobjs_t dbquery_ocean(void) const;
	renderobjs_t dbquery_oceanlz(void) const;
	renderobjs_t dbquery_pathstextname(void) const;
	renderobjs_t dbquery_piersline(void) const;
	renderobjs_t dbquery_pierspoly(void) const;
	renderobjs_t dbquery_placenamesmedium(void) const;
	renderobjs_t dbquery_placenamessmall(void) const;
	renderobjs_t dbquery_powerline(void) const;
	renderobjs_t dbquery_powerminorline(void) const;
	renderobjs_t dbquery_powertowers(void) const;
	renderobjs_t dbquery_protectedareas(void) const;
	renderobjs_t dbquery_protectedareastext(void) const;
	renderobjs_t dbquery_railwaystextname(void) const;
	renderobjs_t dbquery_roadsareatextname(void) const;
	renderobjs_t dbquery_roads(void) const;
	renderobjs_t dbquery_roadslowzoom(void) const;
	renderobjs_t dbquery_roadstextname(void) const;
	renderobjs_t dbquery_roadstextref(void) const;
	renderobjs_t dbquery_roadstextreflowzoom(void) const;
	renderobjs_t dbquery_roadstextrefminor(void) const;
	renderobjs_t dbquery_statenames(void) const;
	renderobjs_t dbquery_stations(void) const;
	renderobjs_t dbquery_textline(void) const;
	renderobjs_t dbquery_textpolylowzoom(void) const;
	renderobjs_t dbquery_tourismboundary(void) const;
	renderobjs_t dbquery_trees(void) const;
	renderobjs_t dbquery_tunnels(void) const;
	renderobjs_t dbquery_turningcircle(void) const;
	renderobjs_t dbquery_waterareas(void) const;
	renderobjs_t dbquery_waterbarriersline(void) const;
	renderobjs_t dbquery_waterbarrierspoint(void) const;
	renderobjs_t dbquery_waterbarrierspoly(void) const;
	renderobjs_t dbquery_waterlines(void) const;
	renderobjs_t dbquery_waterlinescasing(void) const;
	renderobjs_t dbquery_waterlineslowzoom(void) const;
	renderobjs_t dbquery_waterlinestext(void) const;
	renderobjs_t dbquery_waterwaybridges(void) const;
	renderobjs_t dbquery_carto(const Carto::Layer& layer, const std::string& tmpdir);
	renderobjs_t dbquery_carto_sql(const Carto::Layer::sqlptr_t& sql, const std::string& layerid, const std::string& tmpdir);
	renderobjs_t dbquery_carto_static(OSMStaticDB::layer_t layer, const std::string& tmpdir);
	static CartoCSS::Value convert(const CartoSQL::Value& v);
	double transformx(const Point& pt) const;
	double transformy(const Point& pt) const;
	RPoint transform(const Point& pt) const { return RPoint(transformx(pt), transformy(pt)); }
	RLine transform(const LineString& ls) const { RLine ln; for (const auto& x : ls) ln.push_back(transform(x)); return ln; }
	double pixelspermilex(void) const;
	double pixelspermiley(void) const;
	static const std::string& sublayer_name(const std::string& x);
	static const std::string& sublayer_name_file(const std::string& x);
	static bool set_color(const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::Color& col, double opacity = 1);
	static void set_compop(const Cairo::RefPtr<Cairo::Context>& cr, CartoCSS::compositeop_t compop);
	void set_path_helper(const Cairo::RefPtr<Cairo::Context>& cr, const LineString& ls) const;
	void set_path_helper(const Cairo::RefPtr<Cairo::Context>& cr, const MultiLineString& mls) const;
	void set_path_helper(const Cairo::RefPtr<Cairo::Context>& cr, const PolygonSimple& ps) const;
	void set_path_helper(const Cairo::RefPtr<Cairo::Context>& cr, const PolygonHole& ph) const;
	void set_path_helper(const Cairo::RefPtr<Cairo::Context>& cr, const MultiPolygonHole& mph) const;
	void set_path(const Cairo::RefPtr<Cairo::Context>& cr, const LineString& ls) const;
	void set_path(const Cairo::RefPtr<Cairo::Context>& cr, const MultiLineString& mls) const;
	void set_path(const Cairo::RefPtr<Cairo::Context>& cr, const PolygonSimple& ps) const;
	void set_path(const Cairo::RefPtr<Cairo::Context>& cr, const PolygonHole& ph) const;
	void set_path(const Cairo::RefPtr<Cairo::Context>& cr, const MultiPolygonHole& mph) const;
	void set_path(const Cairo::RefPtr<Cairo::Context>& cr, const PolygonSimple& ps, double offset) const;
	void set_path(const Cairo::RefPtr<Cairo::Context>& cr, const PolygonHole& ph, double offset) const;
	void set_path(const Cairo::RefPtr<Cairo::Context>& cr, const MultiPolygonHole& mph, double offset) const;
	static bool is_outofbox(const Point& p0, const Point& p1, const Rect& bbox);
	static MultiLineString remove_outofbox(const LineString& ls, const Rect& bbox);
	static MultiLineString remove_outofbox(const MultiLineString& mls, const Rect& bbox);	
	RenderObjPoints::ptr_t create_points(const Point& pt, id_t id, int32_t zorder) const;
	RenderObjPoints::ptr_t create_points(const MultiPolygonHole& mph, id_t id, int32_t zorder) const;
	RenderObjLines::ptr_t create_lines(const LineString& ls, id_t id, int32_t zorder) const;
	RenderObjLines::ptr_t create_lines(const MultiLineString& ls, id_t id, int32_t zorder) const;
	RenderObjLines::ptr_t create_lines(const MultiPolygonHole& mph, id_t id, int32_t zorder) const;
	RenderObjPolygons::ptr_t create_polygons(const MultiPolygonHole& mph, id_t id, int32_t zorder) const;
	RenderObjPoints::ptr_t create_points(const OSMDB::ObjPoint::const_ptr_t& pp) const;
	RenderObjPoints::ptr_t create_points(const OSMDB::ObjArea::const_ptr_t& pa) const;
	RenderObjLines::ptr_t create_lines(const OSMDB::ObjLine::const_ptr_t& pl) const;
	RenderObjLines::ptr_t create_lines(const OSMDB::ObjArea::const_ptr_t& pa) const;
	RenderObjPolygons::ptr_t create_polygons(const OSMDB::ObjArea::const_ptr_t& pa) const;
	RenderObjLines::ptr_t create_lines(const OSMStaticDB::ObjLine::const_ptr_t& pl) const;
	RenderObjPolygons::ptr_t create_polygons(const OSMStaticDB::ObjArea::const_ptr_t& pa) const;
	void copytags(const RenderObj::ptr_t& r, const OSMStaticDB::Object::const_ptr_t& p) const;
	bool set_pattern(double& width, double& height, const std::string& pat, const Cairo::RefPtr<Cairo::Context>& cr, bool repeat, bool center);
	static MultiPolygonHole convert_path(const Cairo::RefPtr<Cairo::Context>& cr, bool applyctm = true);
	bool detect_collision(const Cairo::RefPtr<Cairo::Context>& cr) const;
	void store_collision(const Cairo::RefPtr<Cairo::Context>& cr);
	void populate_fonts(void);
	Glib::RefPtr<Pango::Layout> text_layout(const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::BasicTextParams& par,
						double overridefontsize = std::numeric_limits<double>::quiet_NaN()) const;
	bool shield_to_source(double& width, double& height, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::ShieldParams& par,
			      double overridefontsize = std::numeric_limits<double>::quiet_NaN());

	static inline void set_color_obstacle(const Cairo::RefPtr<Cairo::Context>& cr) {
                cr->set_source_rgb(1, 0, 0);
        }

	static inline void set_color_obstaclebgnd(const Cairo::RefPtr<Cairo::Context>& cr) {
                cr->set_source_rgb(1, 1, 1);
        }

	static inline void set_color_arp(const Cairo::RefPtr<Cairo::Context>& cr) {
                cr->set_source_rgb(0, 0, 0);
        }

	static inline void set_color_arpbgnd(const Cairo::RefPtr<Cairo::Context>& cr) {
                cr->set_source_rgb(1, 1, 1);
        }
};

const CartoCSS::LayerID& to_str(CartoRender::layerid_t lid);
inline std::ostream& operator<<(std::ostream& os, CartoRender::layerid_t x) { return os << to_str(x); }

#endif /* CARTORENDER_HH */
