//
// C++ Implementation: cartosqlparserstyle
//
// Description: CartoCSS Style file parser
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#undef BOOST_SPIRIT_X3_DEBUG
#include <boost/fusion/include/vector.hpp>
#include <boost/fusion/include/make_deque.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/fusion/include/for_each.hpp>
#include <boost/fusion/include/pop_front.hpp>
#include <boost/fusion/adapted.hpp>

#include <boost/variant.hpp>

#include <boost/algorithm/string/predicate.hpp>

#include <glibmm.h>

#include "cartosqlparser.hh"

namespace CartoSQLParser {

	namespace ascii = boost::spirit::x3::ascii;
	namespace fusion = boost::fusion;

	namespace parser {
		using x3::get;
		using x3::no_case;
		using x3::no_skip;
		using x3::lit;
		using x3::string;
		using x3::omit;
		using x3::lexeme;
		using x3::repeat;
		using x3::eps;
		using x3::eol;
		using x3::eoi;
		using x3::attr;
		using x3::with;
		using x3::_val;
		using x3::_attr;
		using x3::_where;
		using x3::_pass;
		using x3::ulong_;
		using x3::long_;
		using x3::double_;
		using ascii::char_;
		using ascii::alnum;
		using ascii::alpha;
		using ascii::digit;
		using ascii::xdigit;
		using ascii::space;
		using fusion::at_c;

		//-----------------------------------------------------------------------------
		// Components
		//-----------------------------------------------------------------------------

		struct action_typename {
			Value::type_t m_type;
			action_typename(Value::type_t typ) : m_type(typ) {}
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = m_type;
			}
		};

		struct style_entry {
			std::string m_tag;
			Value::type_t m_type;
			style_entry(void) : m_type(Value::type_t::null) {}
		};

		struct action_styleentry_tag {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_tag = _attr(ctx);
			}
		};

		struct action_styleentry_type {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_type = _attr(ctx);
			}
		};

		struct action_stylefile {
			template<typename Context>
			void operator()(Context& ctx) {
				const std::string& tag(_attr(ctx).m_tag);
				if (!add(_val(ctx), tag,_attr(ctx).m_type))
					boost::throw_exception(x3::expectation_failure<decltype(_where(ctx).begin())>(_where(ctx).begin(), "duplicate tag name" + tag));
			}

			inline bool add(CartoSQL::osmschema_t& db, const std::string& tag, Value::type_t typ) {
				if (tag.empty() || typ == Value::type_t::null)
					return true;
				return db.insert(CartoSQL::osmschema_t::value_type(tag, typ)).second;
			}

			inline bool add(x3::unused_type, const std::string&, Value::type_t) {
				return true;
			}
		};

		using stylecomment_type = rule<struct stylecomment, const x3::unused_type>;
		using stylefile_type = rule<struct stylefile, CartoSQL::osmschema_t>;

		const rule<struct osmtypename, Value::type_t> osmtypename = "osmtypename";
		const rule<struct osmtype> osmtype = "osmtype";
		const rule<struct osmtag, std::string> osmtag = "osmtag";
		const rule<struct osmflags> osmflags = "osmflags";
		const rule<struct styleentry, style_entry> styleentry = "styleentry";
		const stylecomment_type stylecomment = "stylecomment";
		const rule<struct styleline, style_entry> styleline = "styleline";
		const stylefile_type stylefile = "stylefile";

		const auto osmtypename_def =
			((keyword("int")
			  | keyword("integer")
			  | keyword("tinyint")
			  | keyword("smallint")
			  | keyword("mediumint")
			  | keyword("bigint")
			  | (keyword("unsigned") >> keyword("big") >> keyword("int"))
			  | keyword("int2")
			  | keyword("int4")
			  | keyword("int8"))[action_typename{Value::type_t::integer}])
			| (((keyword("character") >> lit('(') >> (+digit) >> lit(')'))
			    | (keyword("varchar") >> lit('(') >> (+digit) >> lit(')'))
			    | (keyword("varying") >> keyword("character") >> lit('(') >> (+digit) >> lit(')'))
			    | (keyword("nchar") >> lit('(') >> (+digit) >> lit(')'))
			    | (keyword("native") >> keyword("character") >> lit('(') >> (+digit) >> lit(')'))
			    | (keyword("nvarchar") >> lit('(') >> (+digit) >> lit(')'))
			    | keyword("text")
			    | keyword("clob")
			    | keyword("blob"))[action_typename{Value::type_t::string}])
			| ((keyword("real")
			    | (keyword("double") >> -(keyword("precision")))
			    | keyword("float")
			    | keyword("numeric")
			    | (keyword("decimal") >> lit('(') >> (+digit) >> lit(')')))[action_typename{Value::type_t::floatingpoint}])
			| (keyword("boolean")[action_typename{Value::type_t::boolean}]);

		const auto osmtype_def = +((keyword("node") | keyword("way")) % lit(','));

		const auto osmtag_def = +char_("a-zA-Z0-9_:");

		const auto osmflags_def = keyword("linear") | keyword("polygon");

		const auto styleentry_def =
			osmtype > +(space - eol) > osmtag[action_styleentry_tag{}] > +(space - eol) >
			osmtypename[action_styleentry_type{}] > +(space - eol) > osmflags;

		const auto stylecomment_def = *(space - eol) >> lit('#') >> *(char_ - eol);

		const auto styleline_def = -(no_skip[styleentry][action_copy{}]);

		const auto stylefile_def = *(styleline[action_stylefile{}] >> eol) >> -(styleline[action_stylefile{}]) >> eoi;

		BOOST_SPIRIT_DEFINE(osmtypename, osmtype, osmtag, osmflags, styleentry, stylecomment, styleline, stylefile);
	};
};

void CartoSQL::parse_style(std::istream& is, std::ostream *msg)
{
	if (!is)
		throw std::runtime_error("Cannot read style");
	namespace x3 = boost::spirit::x3;
	using Iterator = boost::spirit::istream_iterator;
	using PosIterator = boost::spirit::line_pos_iterator<Iterator>;
	Iterator f(is >> std::noskipws), l;
	PosIterator pf(f), pi(pf), pl;
        CartoSQL::osmschema_t schema;
	try {
		ExprTable::const_ptr_t p;
		if (!x3::phrase_parse(pi, pl, CartoSQLParser::parser::stylefile,
				      CartoSQLParser::parser::stylecomment, schema))
			CartoSQLParser::parse_error(pf, pi, pl);
	} catch (const x3::expectation_failure<PosIterator>& x) {
		CartoSQLParser::parse_error(pf, x.where(), pl, x.which());
	}
	m_osmschema.swap(schema);
}

void CartoSQL::parse_style(const std::string& fn, std::ostream *msg)
{
	std::ifstream is(fn.c_str());
	if (!is.is_open())
		throw std::runtime_error("Cannot open file " + fn);
	parse_style(is, msg);
}
