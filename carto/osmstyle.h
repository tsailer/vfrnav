//
// C++ Interface: osmstyle
//
// Description: osm2pgsql style file parser
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef OSMSTYLE_HH
#define OSMSTYLE_HH

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <set>

class OsmStyleEntry {
public:
	enum class entity_t : uint8_t {
		none      = 0x00,
		node      = 0x01,
		way       = 0x02,
		relation  = 0x04
	};

	enum class flags_t : uint8_t {
		none      = 0x00,
		polygon   = 0x01,
		linear    = 0x02,
		nocolumn  = 0x04,
		delete_   = 0x08,
		all       = (polygon | linear | nocolumn | delete_),
		polylin   = (polygon | linear)
	};

	enum class datatype_t : uint8_t {
		none,
		text,
		real,
		int4
	};

	OsmStyleEntry(entity_t osmtyp = entity_t::none, const std::string& tag = "", datatype_t dt = datatype_t::none, flags_t flg = flags_t::none);

	entity_t get_osmtype(void) const { return m_osmtype; }
	void set_osmtype(entity_t t) { m_osmtype = t; }
	const std::string& get_tag(void) const { return m_tag; }
	void set_tag(const std::string& t) { m_tag = t; }
	datatype_t get_datatype(void) const { return m_datatype; }
	void set_datatype(datatype_t dt) { m_datatype = dt; }
	flags_t get_flags(void) const { return m_flags; }
	void set_flags(flags_t f) { m_flags = f; }

	int tag_compare(const std::string& tag) const;
	bool tag_match(const std::string& tag) const;

	bool operator==(const OsmStyleEntry& x) const { return !m_tag.compare(x.m_tag); }
	bool operator!=(const OsmStyleEntry& x) const { return !operator==(x); }
	bool operator<(const OsmStyleEntry& x) const { return m_tag.compare(x.m_tag) < 0; }
	bool operator<=(const OsmStyleEntry& x) const { return m_tag.compare(x.m_tag) <= 0; }
	bool operator>(const OsmStyleEntry& x) const { return m_tag.compare(x.m_tag) > 0; }
	bool operator>=(const OsmStyleEntry& x) const { return m_tag.compare(x.m_tag) >= 0; }

protected:
	std::string m_tag;
	entity_t m_osmtype;
	datatype_t m_datatype;
	flags_t m_flags;

};

class OsmStyle : public std::set<OsmStyleEntry> {
public:
	void parse(std::istream& is);
	void parse(const std::string& fn);
	const OsmStyleEntry *find(const std::string& tag) const;

	bool has_relation(void) const;
	void fix_relation(void);
};

inline OsmStyleEntry::entity_t operator|(OsmStyleEntry::entity_t x, OsmStyleEntry::entity_t y) { return (OsmStyleEntry::entity_t)((unsigned int)x | (unsigned int)y); }
inline OsmStyleEntry::entity_t operator&(OsmStyleEntry::entity_t x, OsmStyleEntry::entity_t y) { return (OsmStyleEntry::entity_t)((unsigned int)x & (unsigned int)y); }
inline OsmStyleEntry::entity_t operator^(OsmStyleEntry::entity_t x, OsmStyleEntry::entity_t y) { return (OsmStyleEntry::entity_t)((unsigned int)x ^ (unsigned int)y); }
inline OsmStyleEntry::entity_t operator~(OsmStyleEntry::entity_t x){ return (OsmStyleEntry::entity_t)~(unsigned int)x; }
inline OsmStyleEntry::entity_t& operator|=(OsmStyleEntry::entity_t& x, OsmStyleEntry::entity_t y) { x = x | y; return x; }
inline OsmStyleEntry::entity_t& operator&=(OsmStyleEntry::entity_t& x, OsmStyleEntry::entity_t y) { x = x & y; return x; }
inline OsmStyleEntry::entity_t& operator^=(OsmStyleEntry::entity_t& x, OsmStyleEntry::entity_t y) { x = x ^ y; return x; }

inline OsmStyleEntry::flags_t operator|(OsmStyleEntry::flags_t x, OsmStyleEntry::flags_t y) { return (OsmStyleEntry::flags_t)((unsigned int)x | (unsigned int)y); }
inline OsmStyleEntry::flags_t operator&(OsmStyleEntry::flags_t x, OsmStyleEntry::flags_t y) { return (OsmStyleEntry::flags_t)((unsigned int)x & (unsigned int)y); }
inline OsmStyleEntry::flags_t operator^(OsmStyleEntry::flags_t x, OsmStyleEntry::flags_t y) { return (OsmStyleEntry::flags_t)((unsigned int)x ^ (unsigned int)y); }
inline OsmStyleEntry::flags_t operator~(OsmStyleEntry::flags_t x){ return (OsmStyleEntry::flags_t)~(unsigned int)x; }
inline OsmStyleEntry::flags_t& operator|=(OsmStyleEntry::flags_t& x, OsmStyleEntry::flags_t y) { x = x | y; return x; }
inline OsmStyleEntry::flags_t& operator&=(OsmStyleEntry::flags_t& x, OsmStyleEntry::flags_t y) { x = x & y; return x; }
inline OsmStyleEntry::flags_t& operator^=(OsmStyleEntry::flags_t& x, OsmStyleEntry::flags_t y) { x = x ^ y; return x; }

#endif /* OSMSTYLE_HH */
