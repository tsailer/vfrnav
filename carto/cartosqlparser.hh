//
// C++ Interface: cartosqlparser
//
// Description: CartoCSS SQL parser
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#undef BOOST_SPIRIT_X3_DEBUG
//#include <boost/config/warning_disable.hpp>
#include <boost/spirit/home/x3.hpp>
#include <boost/spirit/home/x3/version.hpp>
#include <boost/spirit/home/x3/support/unused.hpp>
#include <boost/spirit/include/support_line_pos_iterator.hpp>
#include <boost/spirit/include/support_istream_iterator.hpp>

#include <boost/algorithm/string/predicate.hpp>

#include <glibmm.h>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>

#include "cartosql.h"
#include "geom.h"

namespace CartoSQLParser {

	namespace x3 = boost::spirit::x3;

	template<typename Iterator>
	void parse_error(const Iterator& begin, const Iterator& iter, const Iterator& end, const std::string& info = "") {
		std::ostringstream oss;
		oss << "parse error at";
		std::size_t ln(boost::spirit::get_line(iter));
		if (ln != static_cast<std::size_t>(-1))
			oss << " line " << ln;
		std::ostringstream line;
		line << boost::spirit::get_current_line(begin, iter, end);
		std::size_t col(boost::spirit::get_column(begin, iter, 8));
		oss << " column " << col << std::endl
		    << '\'' << line.str() << '\'' << std::endl
		    <<std::setw(col) << ' ' << "^- here";
		if (!info.empty())
			oss << std::endl << info;
		throw CartoSQL::SQLError(oss.str(), info, line.str(), ln, col);
	}

	template<typename Iterator>
	void parse_error(const Iterator& iter, const std::string& info = "") {
		std::ostringstream oss;
		oss << "parse error at";
		std::size_t ln(boost::spirit::get_line(iter));
		if (ln != static_cast<std::size_t>(-1))
			oss << " line " << ln;
		if (!info.empty())
			oss << std::endl << info;
		throw CartoSQL::SQLError(oss.str(), info, "", ln, CartoSQL::SQLError::invalid);
	}

	//-----------------------------------------------------------------------------
	// Parser Context Tag
	//-----------------------------------------------------------------------------

	struct parser_tag {};

	template<typename Iterator>
	struct cartosql_parser {
		cartosql_parser(CartoSQL& sql, Iterator const& first, Iterator const& last, std::ostream *msg = nullptr)
			: m_sql(sql), m_msg(msg), m_first(first), m_last(last) {
		}

		void push_cte(const boost::intrusive_ptr<const CartoSQL::ExprCommonTables>& p) {
			m_cte.push_back(p);
		}

		void pop_cte(void) {
			if (!m_cte.empty())
				m_cte.pop_back();
		}

	        const CartoSQL::ExprTable::const_ptr_t& find_cte(const std::string& name) const {
			for (typename cte_t::const_reverse_iterator i(m_cte.rbegin()), e(m_cte.rend()); i != e; ++i) {
				if (!*i)
					continue;
				const CartoSQL::ExprTable::const_ptr_t& p((*i)->find_table(name));
				if (p)
					return p;
			}
			static const CartoSQL::ExprTable::const_ptr_t null_table;
			return null_table;
		}

		void add_location(const CartoSQL::Expr::const_ptr_t& x, const Iterator& it) {
			if (!x)
				return;
			m_exprlocations[x] = it;
		}

        	bool find_location(Iterator& it, const CartoSQL::Expr::const_ptr_t& x) const {
			if (!x)
				return false;
			typename exprlocations_t::const_iterator i(m_exprlocations.find(x));
			if (i == m_exprlocations.end())
				return false;
			it = i->second;
			return true;
		}

		void add_location(const CartoSQL::ExprTable::const_ptr_t& x, const Iterator& it) {
			if (!x)
				return;
			m_tablelocations[x] = it;
		}

        	bool find_location(Iterator& it, const CartoSQL::ExprTable::const_ptr_t& x) const {
			if (!x)
				return false;
			typename tablelocations_t::const_iterator i(m_tablelocations.find(x));
			if (i == m_tablelocations.end())
				return false;
			it = i->second;
			return true;
		}

		void garbage_collect(void) {
			for (typename exprlocations_t::iterator i(m_exprlocations.begin()), e(m_exprlocations.end()); i != e; ) {
				typename exprlocations_t::iterator i0(i);
				++i;
				if (i0->first.get_count() <= 1)
					m_exprlocations.erase(i0);
			}
			for (typename tablelocations_t::iterator i(m_tablelocations.begin()), e(m_tablelocations.end()); i != e; ) {
				typename tablelocations_t::iterator i0(i);
				++i;
				if (i0->first.get_count() <= 1)
					m_tablelocations.erase(i0);
			}
		}

		void finalize(void) {
		}

		CartoSQL& m_sql;
		std::ostream *m_msg;
		Iterator m_first;
		Iterator m_last;
		typedef std::vector<boost::intrusive_ptr<const CartoSQL::ExprCommonTables> > cte_t;
		cte_t m_cte;
		typedef std::map<CartoSQL::Expr::const_ptr_t,Iterator> exprlocations_t;
		exprlocations_t m_exprlocations;
		typedef std::map<CartoSQL::ExprTable::const_ptr_t,Iterator> tablelocations_t;
		tablelocations_t m_tablelocations;
	};

	using iterator_type = boost::spirit::line_pos_iterator<boost::spirit::istream_iterator>;

	namespace skipper {
		using x3::rule;

		using skip_type = rule<class skip, const x3::unused_type>;

		extern const skip_type skip;

		BOOST_SPIRIT_DECLARE(skip_type);
	};

	using context_type = boost::spirit::x3::context<parser_tag, std::reference_wrapper<CartoSQLParser::cartosql_parser<iterator_type> >,
							x3::phrase_parse_context<skipper::skip_type>::type>;

	namespace parser {
		using x3::rule;

		typedef CartoSQL::Value Value;
		typedef CartoSQL::Table Table;
		typedef CartoSQL::Expr Expr;
		typedef CartoSQL::ExprValue ExprValue;
		typedef CartoSQL::ExprVariable ExprVariable;
		typedef CartoSQL::ExprFunc ExprFunc;
		typedef CartoSQL::ExprFuncAggregate ExprFuncAggregate;
		typedef CartoSQL::ExprFuncTable ExprFuncTable;
		typedef CartoSQL::ExprCase ExprCase;
		typedef CartoSQL::ExprOneChild ExprOneChild;
		typedef CartoSQL::ExprCast ExprCast;
		typedef CartoSQL::ExprColumn ExprColumn;
		typedef CartoSQL::ExprTable ExprTable;
		typedef CartoSQL::ExprTableSort ExprTableSort;
		typedef CartoSQL::ExprOSMTable ExprOSMTable;
		typedef CartoSQL::ExprLiteralTable ExprLiteralTable;
		typedef CartoSQL::ExprTableFunc ExprTableFunc;
		typedef CartoSQL::ExprSelectBase ExprSelectBase;
		typedef CartoSQL::ExprSelect ExprSelect;
		typedef CartoSQL::ExprOSMSelect ExprOSMSelect;
		typedef CartoSQL::ExprCompound ExprCompound;
		typedef CartoSQL::ExprJoin ExprJoin;
		typedef CartoSQL::ExprTableReference ExprTableReference;
		typedef CartoSQL::ExprCommonTables ExprCommonTables;

 		//-----------------------------------------------------------------------------
		// Operators
		//-----------------------------------------------------------------------------

#if defined(__GNUC__) || defined(__clang__)
#define INIT_PRIORITY(priority) __attribute__((init_priority(priority)))
#else
#define INIT_PRIORITY(priority)
#endif

		//-----------------------------------------------------------------------------
		// keywords
		//-----------------------------------------------------------------------------

		inline auto keyword(const char *kwd) {
			return x3::lexeme[x3::no_case[x3::lit(kwd)] >> !x3::ascii::char_("0-9a-zA-Z_")];
		};

		//-----------------------------------------------------------------------------
		// 5.2 <token> and <separator>
		//-----------------------------------------------------------------------------

		struct action_copy {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = _attr(ctx);
			}
		};

		template<typename T>
		struct action_setval {
			T m_val;
			action_setval(T val) : m_val(val) {}
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = m_val;
			}
		};

		struct action_append {
			template<typename T>
			struct append {
				void operator()(T& x, const typename T::value_type& y) const {
					x.push_back(y);
				}
				void operator()(T& x, const T& y) const {
				 	x.insert(x.end(), y.begin(), y.end());
				}
			};

			template<typename Context>
			void operator()(Context& ctx) {
				typedef typename std::remove_reference<decltype(_val(ctx))>::type vt_t;
				append<vt_t>{}(_val(ctx), _attr(ctx));
			}
		};

		template<>
		struct action_append::append<x3::unused_type> {
			template<typename T>
			void operator()(x3::unused_type& x, const T& y) const {
			}
		};

		using identifier_type = rule<class identifier, std::string>;

		extern const identifier_type identifier;

		BOOST_SPIRIT_DECLARE(identifier_type);

		//-----------------------------------------------------------------------------
		// Numeric Literal https://www.sqlite.org/syntax/numeric-literal.html
		//-----------------------------------------------------------------------------

		using numeric_literal_type = rule<struct numeric_literal, Value>;
		using unsigned_integer_type = rule<struct unsigned_integer, uint64_t>;
		using signed_integer_type = rule<struct signed_integer, int64_t>;

		extern const numeric_literal_type numeric_literal;
		extern const unsigned_integer_type unsigned_integer;
		extern const signed_integer_type signed_integer;

		BOOST_SPIRIT_DECLARE(numeric_literal_type, unsigned_integer_type, signed_integer_type);

		//-----------------------------------------------------------------------------
		// String and BLOB literals
		//-----------------------------------------------------------------------------

		using string_literal_type = rule<struct string_literal, std::string>;

		extern const string_literal_type string_literal;

		BOOST_SPIRIT_DECLARE(string_literal_type);

		//-----------------------------------------------------------------------------
		// literals
		//-----------------------------------------------------------------------------

		using literal_type = rule<struct literal, Value>;

		extern const literal_type literal;

		BOOST_SPIRIT_DECLARE(literal_type);

		//-----------------------------------------------------------------------------
		// Type Names (https://www.sqlite.org/datatype3.html)
		//-----------------------------------------------------------------------------

		using type_name_type = rule<struct type_name, Value::type_t>;

		extern const type_name_type type_name;

		BOOST_SPIRIT_DECLARE(type_name_type);

		//-----------------------------------------------------------------------------
		// Mathematical Functions (https://www.postgresql.org/docs/12/functions-math.html)
		//-----------------------------------------------------------------------------

		using func_math_type = rule<struct func_math, Expr::const_ptr_t>;

		extern const func_math_type func_math;

		BOOST_SPIRIT_DECLARE(func_math_type);

		//-----------------------------------------------------------------------------
		// String Functions (https://www.postgresql.org/docs/12/functions-string.html)
		//-----------------------------------------------------------------------------

		using func_exprlist_type = rule<struct func_exprlist, ExprFunc::exprlist_t>;
		using func_string_type = rule<struct func_string, Expr::const_ptr_t>;

		extern const func_exprlist_type func_exprlist;
		extern const func_string_type func_string;

		BOOST_SPIRIT_DECLARE(func_exprlist_type, func_string_type);

		//-----------------------------------------------------------------------------
		// Conditional Expressions (https://www.postgresql.org/docs/12/functions-conditional.html)
		//-----------------------------------------------------------------------------

		using func_conditional_type = rule<struct func_conditional, Expr::const_ptr_t>;

		extern const func_conditional_type func_conditional;

		BOOST_SPIRIT_DECLARE(func_conditional_type);

		//-----------------------------------------------------------------------------
		// Array Functions (https://www.postgresql.org/docs/12/functions-array.html)
		//-----------------------------------------------------------------------------

		using func_array_type = rule<struct func_array, Expr::const_ptr_t>;

		extern const func_array_type func_array;

		BOOST_SPIRIT_DECLARE(func_array_type);

		//-----------------------------------------------------------------------------
		// GIS Functions
		//-----------------------------------------------------------------------------

		using func_gis_type = rule<struct func_gis, Expr::const_ptr_t>;

		extern const func_gis_type func_gis;

		BOOST_SPIRIT_DECLARE(func_gis_type);

		//-----------------------------------------------------------------------------
		// Column Name
		//-----------------------------------------------------------------------------

		using column_name_type = rule<struct column_name, Expr::const_ptr_t>;

		extern const column_name_type column_name;

		BOOST_SPIRIT_DECLARE(column_name_type);

		//-----------------------------------------------------------------------------
		// Expressions (https://www.sqlite.org/lang_expr.html)
		//-----------------------------------------------------------------------------

		using window_defn_type = rule<struct window_defn>;
		using ordering_terms_type = rule<struct ordering_terms, ExprTableSort::Terms>;
		using expr_type = rule<struct expr, Expr::const_ptr_t>;

		extern const window_defn_type window_defn;
		extern const ordering_terms_type ordering_terms;
		extern const expr_type expr;

		BOOST_SPIRIT_DECLARE(window_defn_type, ordering_terms_type, expr_type);

		//------------------------------------------------------------------------------
		// Select Statement
		//------------------------------------------------------------------------------

		using select_stmt_type = rule<struct select_stmt, ExprTable::const_ptr_t>;

		extern const select_stmt_type select_stmt;

		BOOST_SPIRIT_DECLARE(select_stmt_type);
	};
};
