//
// C++ Implementation: cartosqlparserskip
//
// Description: CartoCSS SQL parser skipper
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cartosqlparser.hh"

#include <boost/variant.hpp>

namespace CartoSQLParser {

	namespace ascii = boost::spirit::x3::ascii;

	namespace skipper {
		using x3::lit;
		using x3::eol;
		using x3::eoi;
		using ascii::char_;
		using ascii::space;

		const rule<class single_line_comment> single_line_comment = "single_line_comment";
		const rule<class block_comment> block_comment = "block_comment";
		const skip_type skip INIT_PRIORITY(200) = "skip";

		const auto single_line_comment_def = lit("--") >> *(char_ - eol) >> (eol|eoi);
		const auto block_comment_def = (lit("/*") >> *(block_comment | (char_ - lit("*/")))) > lit("*/");
		const auto skip_def = space | single_line_comment | block_comment;

		BOOST_SPIRIT_DEFINE(single_line_comment, block_comment, skip);

		BOOST_SPIRIT_INSTANTIATE(skip_type, iterator_type, x3::unused_type);
	};

};
