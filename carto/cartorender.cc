//
// C++ Implementation: cartorender
//
// Description: CartoCSS Rendering
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glibmm.h>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cmath>

#include <pango/pango.h>

#include <librsvg/rsvg.h>

#define __RSVG_RSVG_H_INSIDE__
#include <librsvg/rsvg-cairo.h>
#undef __RSVG_RSVG_H_INSIDE__

#ifdef HAVE_EIGEN3
#include <Eigen/Dense>
#include <Eigen/Cholesky>
#include <unsupported/Eigen/Polynomials>
#endif

#include "cartorender.h"
#include "hibernate.h"

const CartoCSS::LayerID& to_str(CartoRender::layerid_t lid)
{
	switch (lid) {
	case CartoRender::layerid_t::addresses:
	{
		static const CartoCSS::LayerID r("addresses");
		return r;
	}

	case CartoRender::layerid_t::adminhighzoom:
	{
		static const CartoCSS::LayerID r("admin-high-zoom");
		return r;
	}

	case CartoRender::layerid_t::adminlowzoom:
	{
		static const CartoCSS::LayerID r("admin-low-zoom");
		return r;
	}

	case CartoRender::layerid_t::adminmidzoom:
	{
		static const CartoCSS::LayerID r("admin-mid-zoom");
		return r;
	}

	case CartoRender::layerid_t::admintext:
	{
		static const CartoCSS::LayerID r("admin-text");
		return r;
	}

	case CartoRender::layerid_t::aerialways:
	{
		static const CartoCSS::LayerID r("aerialways");
		return r;
	}

	case CartoRender::layerid_t::aeroways:
	{
		static const CartoCSS::LayerID r("aeroways");
		return r;
	}

	case CartoRender::layerid_t::amenityline:
	{
		static const CartoCSS::LayerID r("amenity-line");
		return r;
	}

	case CartoRender::layerid_t::amenitylowpriority:
	{
		static const CartoCSS::LayerID r("amenity-low-priority");
		return r;
	}

	case CartoRender::layerid_t::amenitypoints:
	{
		static const CartoCSS::LayerID r("amenity-points");
		return r;
	}

	case CartoRender::layerid_t::barriers:
	{
		static const CartoCSS::LayerID r("barriers");
		return r;
	}

	case CartoRender::layerid_t::bridge:
	{
		static const CartoCSS::LayerID r("bridge");
		return r;
	}

	case CartoRender::layerid_t::bridges:
	{
		static const CartoCSS::LayerID r("bridges");
		return r;
	}

	case CartoRender::layerid_t::bridgetext:
	{
		static const CartoCSS::LayerID r("bridge-text");
		return r;
	}

	case CartoRender::layerid_t::buildings:
	{
		static const CartoCSS::LayerID r("buildings");
		return r;
	}

	case CartoRender::layerid_t::buildingtext:
	{
		static const CartoCSS::LayerID r("building-text");
		return r;
	}

	case CartoRender::layerid_t::capitalnames:
	{
		static const CartoCSS::LayerID r("capital-names");
		return r;
	}

	case CartoRender::layerid_t::cliffs:
	{
		static const CartoCSS::LayerID r("cliffs");
		return r;
	}

	case CartoRender::layerid_t::countrynames:
	{
		static const CartoCSS::LayerID r("country-names");
		return r;
	}

	case CartoRender::layerid_t::countynames:
	{
		static const CartoCSS::LayerID r("county-names");
		return r;
	}

	case CartoRender::layerid_t::entrances:
	{
		static const CartoCSS::LayerID r("entrances");
		return r;
	}

	case CartoRender::layerid_t::ferryroutes:
	{
		static const CartoCSS::LayerID r("ferry-routes");
		return r;
	}

	case CartoRender::layerid_t::ferryroutestext:
	{
		static const CartoCSS::LayerID r("ferry-routes-text");
		return r;
	}

	case CartoRender::layerid_t::guideways:
	{
		static const CartoCSS::LayerID r("guideways");
		return r;
	}

	case CartoRender::layerid_t::highwayareacasing:
	{
		static const CartoCSS::LayerID r("highway-area-casing");
		return r;
	}

	case CartoRender::layerid_t::highwayareafill:
	{
		static const CartoCSS::LayerID r("highway-area-fill");
		return r;
	}

	case CartoRender::layerid_t::icesheetoutlines:
	{
		static const CartoCSS::LayerID r("icesheet-outlines");
		return r;
	}

	case CartoRender::layerid_t::icesheetpoly:
	{
		static const CartoCSS::LayerID r("icesheet-poly");
		return r;
	}

	case CartoRender::layerid_t::interpolation:
	{
		static const CartoCSS::LayerID r("interpolation");
		return r;
	}

	case CartoRender::layerid_t::junctions:
	{
		static const CartoCSS::LayerID r("junctions");
		return r;
	}

	case CartoRender::layerid_t::landcover:
	{
		static const CartoCSS::LayerID r("landcover");
		return r;
	}

	case CartoRender::layerid_t::landcoverareasymbols:
	{
		static const CartoCSS::LayerID r("landcover-area-symbols");
		return r;
	}

	case CartoRender::layerid_t::landcoverline:
	{
		static const CartoCSS::LayerID r("landcover-line");
		return r;
	}

	case CartoRender::layerid_t::landcoverlowzoom:
	{
		static const CartoCSS::LayerID r("landcover-low-zoom");
		return r;
	}

	case CartoRender::layerid_t::landuseoverlay:
	{
		static const CartoCSS::LayerID r("landuse-overlay");
		return r;
	}

	case CartoRender::layerid_t::marinasarea:
	{
		static const CartoCSS::LayerID r("marinas-area");
		return r;
	}

	case CartoRender::layerid_t::necountries:
	{
		static const CartoCSS::LayerID r("necountries");
		return r;
	}

	case CartoRender::layerid_t::ocean:
	{
		static const CartoCSS::LayerID r("ocean");
		return r;
	}

	case CartoRender::layerid_t::oceanlz:
	{
		static const CartoCSS::LayerID r("ocean-lz");
		return r;
	}

	case CartoRender::layerid_t::pathstextname:
	{
		static const CartoCSS::LayerID r("paths-text-name");
		return r;
	}

	case CartoRender::layerid_t::piersline:
	{
		static const CartoCSS::LayerID r("piers-line");
		return r;
	}

	case CartoRender::layerid_t::pierspoly:
	{
		static const CartoCSS::LayerID r("piers-poly");
		return r;
	}

	case CartoRender::layerid_t::placenamesmedium:
	{
		static const CartoCSS::LayerID r("placenames-medium");
		return r;
	}

	case CartoRender::layerid_t::placenamessmall:
	{
		static const CartoCSS::LayerID r("placenames-small");
		return r;
	}

	case CartoRender::layerid_t::powerline:
	{
		static const CartoCSS::LayerID r("power-line");
		return r;
	}

	case CartoRender::layerid_t::powerminorline:
	{
		static const CartoCSS::LayerID r("power-minorline");
		return r;
	}

	case CartoRender::layerid_t::powertowers:
	{
		static const CartoCSS::LayerID r("power-towers");
		return r;
	}

	case CartoRender::layerid_t::protectedareas:
	{
		static const CartoCSS::LayerID r("protected-areas");
		return r;
	}

	case CartoRender::layerid_t::protectedareastext:
	{
		static const CartoCSS::LayerID r("protected-areas-text");
		return r;
	}

	case CartoRender::layerid_t::railwaystextname:
	{
		static const CartoCSS::LayerID r("railways-text-name");
		return r;
	}

	case CartoRender::layerid_t::roadsareatextname:
	{
		static const CartoCSS::LayerID r("roads-area-text-name");
		return r;
	}

	case CartoRender::layerid_t::roadscasing:
	{
		static const CartoCSS::LayerID r("roads-casing");
		return r;
	}

	case CartoRender::layerid_t::roadsfill:
	{
		static const CartoCSS::LayerID r("roads-fill");
		return r;
	}

	case CartoRender::layerid_t::roadslowzoom:
	{
		static const CartoCSS::LayerID r("roads-low-zoom");
		return r;
	}

	case CartoRender::layerid_t::roadstextname:
	{
		static const CartoCSS::LayerID r("roads-text-name");
		return r;
	}

	case CartoRender::layerid_t::roadstextref:
	{
		static const CartoCSS::LayerID r("roads-text-ref");
		return r;
	}

	case CartoRender::layerid_t::roadstextreflowzoom:
	{
		static const CartoCSS::LayerID r("roads-text-ref-low-zoom");
		return r;
	}

	case CartoRender::layerid_t::roadstextrefminor:
	{
		static const CartoCSS::LayerID r("roads-text-ref-minor");
		return r;
	}

	case CartoRender::layerid_t::statenames:
	{
		static const CartoCSS::LayerID r("state-names");
		return r;
	}

	case CartoRender::layerid_t::stations:
	{
		static const CartoCSS::LayerID r("stations");
		return r;
	}

	case CartoRender::layerid_t::textline:
	{
		static const CartoCSS::LayerID r("text-line");
		return r;
	}

	case CartoRender::layerid_t::textlowpriority:
	{
		static const CartoCSS::LayerID r("text-low-priority");
		return r;
	}

	case CartoRender::layerid_t::textpoint:
	{
		static const CartoCSS::LayerID r("text-point");
		return r;
	}

	case CartoRender::layerid_t::textpolylowzoom:
	{
		static const CartoCSS::LayerID r("text-poly-low-zoom");
		return r;
	}

	case CartoRender::layerid_t::tourismboundary:
	{
		static const CartoCSS::LayerID r("tourism-boundary");
		return r;
	}

	case CartoRender::layerid_t::trees:
	{
		static const CartoCSS::LayerID r("trees");
		return r;
	}

	case CartoRender::layerid_t::tunnels:
	{
		static const CartoCSS::LayerID r("tunnels");
		return r;
	}

	case CartoRender::layerid_t::turningcirclecasing:
	{
		static const CartoCSS::LayerID r("turning-circle-casing");
		return r;
	}

	case CartoRender::layerid_t::turningcirclefill:
	{
		static const CartoCSS::LayerID r("turning-circle-fill");
		return r;
	}

	case CartoRender::layerid_t::waterareas:
	{
		static const CartoCSS::LayerID r("water-areas");
		return r;
	}

	case CartoRender::layerid_t::waterbarriersline:
	{
		static const CartoCSS::LayerID r("water-barriers-line");
		return r;
	}

	case CartoRender::layerid_t::waterbarrierspoint:
	{
		static const CartoCSS::LayerID r("water-barriers-point");
		return r;
	}

	case CartoRender::layerid_t::waterbarrierspoly:
	{
		static const CartoCSS::LayerID r("water-barriers-poly");
		return r;
	}

	case CartoRender::layerid_t::waterlines:
	{
		static const CartoCSS::LayerID r("water-lines");
		return r;
	}

	case CartoRender::layerid_t::waterlinescasing:
	{
		static const CartoCSS::LayerID r("water-lines-casing");
		return r;
	}

	case CartoRender::layerid_t::waterlineslowzoom:
	{
		static const CartoCSS::LayerID r("water-lines-low-zoom");
		return r;
	}

	case CartoRender::layerid_t::waterlinestext:
	{
		static const CartoCSS::LayerID r("water-lines-text");
		return r;
	}

	case CartoRender::layerid_t::waterwaybridges:
	{
		static const CartoCSS::LayerID r("waterway-bridges");
		return r;
	}

	case CartoRender::layerid_t::invalid:
	{
		static const CartoCSS::LayerID r("invalid");
		return r;
	}

	default:
	{
		static const CartoCSS::LayerID r("??");
		return r;
	}
	}
}

CartoRender::Obstacle::Obstacle(const Point& pt, double agl, double amsl, bool marked, bool lighted,
				const std::string& hyperlink)
	: m_pt(pt), m_agl(agl), m_amsl(amsl), m_hyperlink(hyperlink), m_marked(marked), m_lighted(lighted)
{
}

CartoRender::CoordObstacle::CoordObstacle(const Obstacle& ob, double x, double y)
	: Obstacle(ob), m_x(x), m_y(y)
{
}

CartoRender::RenderObj::RenderObj(const fields_t& fields, id_t id, int32_t zorder)
	: m_fields(fields), m_refcount(0), m_zorder(zorder)
{
	m_ids.push_back(id);
}

CartoRender::RenderObj::~RenderObj()
{
}

bool CartoRender::RenderObj::is_id(id_t id) const
{
	ids_t::const_iterator i(std::lower_bound(m_ids.begin(), m_ids.end(), id));
	return i != m_ids.end() && id == *i;
}

bool CartoRender::RenderObj::is_id(id_t id0, id_t id1) const
{
	if (id0 > id1)
		std::swap(id0, id1);
	ids_t::const_iterator i(std::lower_bound(m_ids.begin(), m_ids.end(), id0));
	if (i == m_ids.end())
		return false;
	if (id0 == *i)
		return true;
	i = std::lower_bound(i, m_ids.end(), id1);
	return i != m_ids.end() && id1 == *i;	
}

bool CartoRender::RenderObj::is_id_helper(ids_t& ids) const
{
	std::sort(ids.begin(), ids.end());
	ids_t::const_iterator i(m_ids.begin());
	for (id_t id : ids) {
		i = std::lower_bound(i, m_ids.end(), id);
		if (i == m_ids.end())
			return false;
		if (id == *i)
			return true;
	}
	return false;
}

void CartoRender::RenderObj::merge_ids(const ids_t& ids)
{
	ids_t r;
	r.reserve(m_ids.size() + ids.size());
	std::merge(m_ids.begin(), m_ids.end(), ids.begin(), ids.end(), std::back_inserter(r));
	m_ids.swap(r);
}

std::string CartoRender::RenderObj::get_ids_str(void) const
{
	std::ostringstream r;
	char sep(0);
	for (id_t id : get_ids()) {
		if (sep)
			r << sep;
		r << id;
		sep = ',';
	}
	if (sep)
		return r.str();
	return "-";
}

CartoRender::RenderObj::id_t CartoRender::RenderObj::get_ids_hash(void) const
{
	id_t r(0x811c9dc5);
 	for (id_t id : get_ids()) {
		r *= 0x01000193;
		r ^= id;
	}
	return r;
}

const CartoRender::RenderObj::fieldval_t& CartoRender::RenderObj::find_field(const std::string& key) const
{
	static const fieldval_t null;
	fields_t::const_iterator i(m_fields.find(key));
	if (i == m_fields.end())
		return null;
	return i->second;
}

void CartoRender::RenderObj::copy_field_string(const OSMDB& odb, const std::string& fld, OSMDB::Object::tagname_t val)
{
	if (val == OSMDB::Object::tagname_t::invalid) {
		set_field(fld);
		return;
	}
	const char *cp(odb.find_tagname(val));
	if (!cp) {
		set_field(fld);
		return;
	}
	set_field(fld, cp);
}

void CartoRender::RenderObj::copy_field_string(const OSMDB& odb, const std::string& fld, OSMDB::Object::tagname_t val, const std::string& dflt)
{
	if (val == OSMDB::Object::tagname_t::invalid) {
		set_field(fld, dflt);
		return;
	}
	const char *cp(odb.find_tagname(val));
	if (!cp) {
		set_field(fld, dflt);
		return;
	}
	set_field(fld, cp);
}

void CartoRender::RenderObj::copy_field_double(const OSMDB& odb, const std::string& fld, OSMDB::Object::tagname_t val)
{
	if (val == OSMDB::Object::tagname_t::invalid) {
		set_field(fld);
		return;
	}
	const char *cp(odb.find_tagname(val));
	if (!cp) {
		set_field(fld);
		return;
	}
	char *cp1;
	double v(strtod(cp, &cp1));
	if (cp1 == cp || *cp1) {
		set_field(fld);
		return;
	}
	set_field(fld, v);
}

void CartoRender::RenderObj::copy_field_double(const OSMDB& odb, const std::string& fld, OSMDB::Object::tagname_t val, double dflt)
{
	if (val == OSMDB::Object::tagname_t::invalid) {
		set_field(fld, dflt);
		return;
	}
	const char *cp(odb.find_tagname(val));
	if (!cp) {
		set_field(fld, dflt);
		return;
	}
	char *cp1;
	double v(strtod(cp, &cp1));
	if (cp1 == cp || *cp1) {
		set_field(fld, dflt);
		return;
	}
	set_field(fld, v);
}

void CartoRender::RenderObj::copy_field_int(const OSMDB& odb, const std::string& fld, OSMDB::Object::tagname_t val)
{
	if (val == OSMDB::Object::tagname_t::invalid) {
		set_field(fld);
		return;
	}
	const char *cp(odb.find_tagname(val));
	if (!cp) {
		set_field(fld);
		return;
	}
	char *cp1;
	int64_t v(strtoll(cp, &cp1, 10));
	if (cp1 == cp || *cp1) {
		set_field(fld);
		return;
	}
	set_field(fld, v);
}

void CartoRender::RenderObj::copy_field_int(const OSMDB& odb, const std::string& fld, OSMDB::Object::tagname_t val, int64_t dflt)
{
	if (val == OSMDB::Object::tagname_t::invalid) {
		set_field(fld, dflt);
		return;
	}
	const char *cp(odb.find_tagname(val));
	if (!cp) {
		set_field(fld, dflt);
		return;
	}
	char *cp1;
	int64_t v(strtoll(cp, &cp1, 10));
	if (cp1 == cp || *cp1) {
		set_field(fld, dflt);
		return;
	}
	set_field(fld, v);
}

std::string CartoRender::RenderObj::get_binary_attributes(void) const
{
	std::ostringstream attr;
	HibernateWriteStream ar(attr);
	const_cast<RenderObj *>(this)->hibernate_attr(ar);
	return attr.str();
}

std::ostream& CartoRender::RenderObj::print(std::ostream& os, unsigned int indent) const
{
	os << std::string(indent, ' ') << "IDs";
	{
		char sep(' ');
		for (id_t id : get_ids()) {
			os << sep << id;
			sep = ',';
		}
		if (sep == ' ')
			os << '-';
	}
	os << " #IDs " << get_ids().size() << " z " << get_zorder() << " clsid " << get_classid() << std::endl;
	for (const auto& fld : get_fields()) {
		os << std::string(indent + 2, ' ') << fld.first << " = ";
		if (fld.second.is_null())
			os << "null";
		else if (fld.second.is_string())
			os << '"' << static_cast<std::string>(fld.second) << '"';
		else
			os << static_cast<std::string>(fld.second);
		os << std::endl;
	}
	return os;
}

void CartoRender::RenderObj::set_line_context(const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::LineParams& par)
{
	CartoRender::set_color(cr, par.get_color(), par.get_opacity());
	if (par.get_dasharray().empty())
		cr->unset_dash();
	else
		cr->set_dash(par.get_dasharray(), 0);
	cr->set_line_width(par.get_width());
	switch (par.get_cap()) {
	case CartoCSS::LineParams::cap_t::butt:
		cr->set_line_cap(Cairo::LINE_CAP_BUTT);
		break;

				
	case CartoCSS::LineParams::cap_t::round:
		cr->set_line_cap(Cairo::LINE_CAP_ROUND);
		break;

	case CartoCSS::LineParams::cap_t::square:
		cr->set_line_cap(Cairo::LINE_CAP_SQUARE);
		break;

	default:
		break;
	}
	switch (par.get_join()) {
	case CartoCSS::LineParams::join_t::miter:
		cr->set_line_join(Cairo::LINE_JOIN_MITER);
		break;

	case CartoCSS::LineParams::join_t::round:     	
		cr->set_line_join(Cairo::LINE_JOIN_ROUND);
		break;

	case CartoCSS::LineParams::join_t::bevel:     	
		cr->set_line_join(Cairo::LINE_JOIN_BEVEL);
		break;

	default:
		break;
	}
	//cr->set_miter_limit();
}

bool CartoRender::RenderObj::draw_marker(double& width, double& height, CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr,
					 const CartoCSS::MarkerParams& par, double ptx, double pty, double rot) const
{
	if (par.get_file().empty()) {
		width = par.get_width();
		height = par.get_height();
		cr->save();
		cr->set_line_width(par.get_linewidth());
		cr->save();
		cr->translate(ptx, pty);
		cr->rotate(rot);
		cr->scale(0.5 * par.get_width(), 0.5 * par.get_height());
		// check collisions
		cr->begin_new_path();
		cr->rectangle(-1, -1, 2, 2);
		if (par.get_allowoverlap() || !render.detect_collision(cr)) {
			if (!par.get_ignoreplacement())
				render.store_collision(cr);
			cr->begin_new_path();
			cr->arc(0, 0, 1, 0, 2 * M_PI);
			cr->restore();
			render.set_color(cr, par.get_fill(), par.get_opacity());
			cr->fill_preserve();
			render.set_color(cr, par.get_linecolor(), par.get_opacity());
			cr->stroke();
			cr->restore();
			return true;
		}
		cr->begin_new_path();
		cr->restore();
		cr->restore();
		return false;
	}
	cr->save();
	cr->translate(ptx, pty);
	cr->rotate(rot);
	double pwidth, pheight;
	if (!render.set_pattern(pwidth, pheight, par.get_file(), cr, false, true))
		return false;
	width = pwidth;
	height = pheight;
	if (par.get_clip()) {
		cr->rectangle(-0.5 * par.get_width(), -0.5 * par.get_height(), par.get_width(), par.get_height());
		cr->clip();
		pwidth = std::min(pwidth, par.get_width());
		pheight = std::min(pheight, par.get_height());
	}
	cr->begin_new_path();
	cr->rectangle(-0.5 * pwidth, -0.5 * pheight, pwidth, pheight);
	if (par.get_allowoverlap() || !render.detect_collision(cr)) {
		if (!par.get_ignoreplacement())
			render.store_collision(cr);
		cr->begin_new_path();
		if (true) {
			cr->push_group();
			cr->paint();
			render.set_color(cr, par.get_fill(), 1);
			cr->set_operator(Cairo::OPERATOR_IN);
			cr->paint();
			cr->set_operator(Cairo::OPERATOR_OVER);
			cr->pop_group_to_source();
		}
		cr->paint_with_alpha(par.get_opacity());
		if (false) {
			cr->set_source_rgba(0, 0, 0, 1);
			cr->set_line_width(1);
			cr->move_to(-0.5 * par.get_width(), 0);
			cr->line_to(0.5 * par.get_width(), 0);
			cr->move_to(0, -0.5 * par.get_height());
			cr->line_to(0, 0.5 * par.get_height());
			cr->stroke();
		}
		cr->begin_new_path();
		cr->restore();
		return true;
	}
	cr->begin_new_path();
	cr->restore();
	return false;
}

void CartoRender::RenderObj::draw_marker(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::MarkerParams& par, const MultiPoint& coords) const
{
	if (coords.empty())
		return;
	for (const auto& pt : coords) {
		double width, height;
		draw_marker(width, height, render, cr, par, render.transformx(pt), render.transformy(pt));
	}
}

void CartoRender::RenderObj::draw_marker(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::MarkerParams& par, const LineString& coords) const
{
	RLine line(render.transform(coords));
	if (line.size() < 2)
		return;
	double linelen(line.get_length());
	double leninc(0.02 * linelen);
	// find initial placement
	double pos, width, height;
	RPoint pt;
	for (unsigned int x = 0; x < 100; ++x) {
		bool nowork(true);
		pos = 0.5 * linelen + x * leninc;
		if (pos < linelen) {
			nowork = false;
			RLine::size_type idx;
			pt = line.get_along(idx, pos);
			double rot(0);
			if (idx < 0 || idx >= line.size())
				rot = 0;
			if (idx + 1 >= line.size())
				rot = (line[idx] - line[idx - 1]).get_arg();
			else
				rot = (line[idx + 1] - line[idx]).get_arg();
			bool ok(draw_marker(width, height, render, cr, par, pt.get_x(), pt.get_y(), rot));
			if (!x && !std::isnan(width) && width > 0)
				leninc = std::max(leninc, 0.5 * width);
			if (ok)
				break;
		}
		if (!x)
			continue;
		pos = 0.5 * linelen - x * leninc;
		if (pos > 0) {
			nowork = false;
			RLine::size_type idx;
			pt = line.get_along(idx, pos);
			double rot(0);
			if (idx < 0 || idx >= line.size())
				rot = 0;
			if (idx + 1 >= line.size())
				rot = (line[idx] - line[idx - 1]).get_arg();
			else
				rot = (line[idx + 1] - line[idx]).get_arg();
			if (draw_marker(width, height, render, cr, par, pt.get_x(), pt.get_y(), rot))
				break;
		}
		pos = std::numeric_limits<double>::quiet_NaN();
		if (nowork)
			break;
	}
	if (std::isnan(pos))
		return;
	if (false)
		std::cerr << "marker: coord " << pt.get_x() << ',' << pt.get_y() << ' '
			  << width << 'x' << height << " file " << par.get_file() << std::endl;
	if (par.get_placement() != CartoCSS::placement_t::line || std::isnan(par.get_spacing()) || par.get_spacing() <= 0)
		return;
	for (double pos1(pos + par.get_spacing()); pos1 < linelen;) {
		RLine::size_type idx;
		pt = line.get_along(idx, pos1);
		double rot(0);
		if (idx < 0 || idx >= line.size())
			rot = 0;
		if (idx + 1 >= line.size())
			rot = (line[idx] - line[idx - 1]).get_arg();
		else
			rot = (line[idx + 1] - line[idx]).get_arg();
		if (draw_marker(width, height, render, cr, par, pt.get_x(), pt.get_y(), rot))
			pos1 += par.get_spacing();
		else
			pos1 += leninc;
	}
	for (double pos1(pos - par.get_spacing()); pos1 > 0;) {
		RLine::size_type idx;
		pt = line.get_along(idx, pos1);
		double rot(0);
		if (idx < 0 || idx >= line.size())
			rot = 0;
		if (idx + 1 >= line.size())
			rot = (line[idx] - line[idx - 1]).get_arg();
		else
			rot = (line[idx + 1] - line[idx]).get_arg();
		if (draw_marker(width, height, render, cr, par, pt.get_x(), pt.get_y(), rot))
			pos1 -= par.get_spacing();
		else
			pos1 -= leninc;
	}
}

class CartoRender::RenderObj::TextLayout {
public:
	TextLayout(void);
	TextLayout(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::TextParams& par,
		   double sizeoverride = std::numeric_limits<double>::quiet_NaN());
	bool is_valid(void) const { return !!m_layout; }
	bool check_collision(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::TextParams& par,
			     double x, double y, const CartoCSS::BasicTextParams::PlacementOrigin& po = CartoCSS::BasicTextParams::PlacementOrigin());
	void store_collision(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::TextParams& par,
			     double x, double y, const CartoCSS::BasicTextParams::PlacementOrigin& po = CartoCSS::BasicTextParams::PlacementOrigin());
	void draw(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::TextParams& par,
		  double x, double y, const CartoCSS::BasicTextParams::PlacementOrigin& po = CartoCSS::BasicTextParams::PlacementOrigin());

protected:
	static constexpr bool debug_collisions = false;
	Glib::RefPtr<Pango::Layout> m_layout;
	double m_originx;
	double m_originy;
	double m_width;
	double m_height;
	void compute_offset(double& x, double& y, const CartoCSS::TextParams& par,
			    const CartoCSS::BasicTextParams::PlacementOrigin& po = CartoCSS::BasicTextParams::PlacementOrigin());
			    
};

CartoRender::RenderObj::TextLayout::TextLayout(void)
	: m_originx(0), m_originy(0), m_width(0), m_height(0)
{
}

CartoRender::RenderObj::TextLayout::TextLayout(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::TextParams& par, double sizeoverride)
	: m_originx(0), m_originy(0), m_width(0), m_height(0)
{
	if (sizeoverride <= 0)
		sizeoverride = std::numeric_limits<double>::quiet_NaN();
	m_layout = render.text_layout(cr, par);
	if (!m_layout)
		return;
	m_layout->update_from_cairo_context(cr);
	Pango::Rectangle inkext(m_layout->get_ink_extents());
	m_width = inkext.get_width() * (1.0 / Pango::SCALE);
	m_height = inkext.get_height() * (1.0 / Pango::SCALE);
	m_originx = par.get_dx() - inkext.get_x() * (1.0 / Pango::SCALE) - 0.5 * m_width;
	m_originy = par.get_dy() - inkext.get_y() * (1.0 / Pango::SCALE) - 0.5 * m_height;
}

void CartoRender::RenderObj::TextLayout::compute_offset(double& x, double& y, const CartoCSS::TextParams& par,
							const CartoCSS::BasicTextParams::PlacementOrigin& po)
{
	if (!m_layout)
		return;
	CartoCSS::TextParams::horizontalalignment_t halign(po.get_horizontalalignment());
	CartoCSS::TextParams::verticalalignment_t valign(po.get_verticalalignment());
	if (halign == CartoCSS::TextParams::horizontalalignment_t::invalid)
		halign = par.get_horizontalalignment();
	if (valign == CartoCSS::TextParams::verticalalignment_t::invalid)
		valign = par.get_verticalalignment();
	if (valign == CartoCSS::TextParams::verticalalignment_t::auto_) {
		if (par.get_dy() < 0)
			valign = CartoCSS::TextParams::verticalalignment_t::bottom;
		else if (par.get_dy() > 0)
			valign = CartoCSS::TextParams::verticalalignment_t::top;
		else
			valign = CartoCSS::TextParams::verticalalignment_t::middle;
	}
	switch (halign) {
	case CartoCSS::TextParams::horizontalalignment_t::left:
		x += 0.5 * m_width;
		m_layout->set_alignment(Pango::ALIGN_LEFT);
		break;

	case CartoCSS::TextParams::horizontalalignment_t::right:
		x -= 0.5 * m_width;
		m_layout->set_alignment(Pango::ALIGN_RIGHT);
		break;

	default:
		m_layout->set_alignment(Pango::ALIGN_CENTER);
		break;
	}
	switch (valign) {
	case CartoCSS::TextParams::verticalalignment_t::top:
		y += 0.5 * m_height;
		break;

	case CartoCSS::TextParams::verticalalignment_t::bottom:
		y -= 0.5 * m_height;
		break;

	default:
		break;
	}
}

bool CartoRender::RenderObj::TextLayout::check_collision(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::TextParams& par,
							 double x, double y, const CartoCSS::BasicTextParams::PlacementOrigin& po)
{
	if (!m_layout)
		return true;
	compute_offset(x, y, par, po);
	x += m_originx - par.get_margin();
	y += m_originy - par.get_margin();
	cr->begin_new_path();
	for (Pango::LayoutIter iter(m_layout->get_iter());;) {
		Pango::Rectangle r(iter.get_line_ink_extents());
		cr->rectangle(x + r.get_x() * (1.0 / Pango::SCALE), y + r.get_y() * (1.0 / Pango::SCALE),
			      r.get_width() * (1.0 / Pango::SCALE) + 2 * par.get_margin(), r.get_height() * (1.0 / Pango::SCALE) + 2 * par.get_margin());
		if (!iter.next_line())
			break;
	}
	bool coll(render.detect_collision(cr));
	if (debug_collisions && !coll) {
		cr->set_source_rgba(1, 0, 1, 1);
		cr->set_line_width(3);
		cr->set_line_cap(Cairo::LINE_CAP_ROUND);
		cr->set_line_join(Cairo::LINE_JOIN_ROUND);
		cr->stroke();
	}
	cr->begin_new_path();
	return coll;
}

void CartoRender::RenderObj::TextLayout::store_collision(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::TextParams& par,
							 double x, double y, const CartoCSS::BasicTextParams::PlacementOrigin& po)
{
	if (!m_layout)
		return;
	compute_offset(x, y, par, po);
	x += m_originx;
	y += m_originy;
	cr->begin_new_path();
	for (Pango::LayoutIter iter(m_layout->get_iter());;) {
		Pango::Rectangle r(iter.get_line_ink_extents());
		cr->rectangle(x + r.get_x() * (1.0 / Pango::SCALE), y + r.get_y() * (1.0 / Pango::SCALE),
			      r.get_width() * (1.0 / Pango::SCALE), r.get_height() * (1.0 / Pango::SCALE));
		if (!iter.next_line())
			break;
	}
	render.store_collision(cr);
	if (debug_collisions) {
		cr->set_source_rgba(0, 0, 0, 1);
		cr->set_line_width(1);
		cr->set_line_cap(Cairo::LINE_CAP_ROUND);
		cr->set_line_join(Cairo::LINE_JOIN_ROUND);
		cr->stroke();
	}
	cr->begin_new_path();
}

void CartoRender::RenderObj::TextLayout::draw(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::TextParams& par,
					      double x, double y, const CartoCSS::BasicTextParams::PlacementOrigin& po)
{
	if (!m_layout)
		return;
	compute_offset(x, y, par, po);
	x += m_originx;
	y += m_originy;
	cr->begin_new_path();
	cr->push_group();
	cr->move_to(x, y);
	if (true) {
		pango_cairo_layout_path(cr->cobj(), m_layout->gobj());
		if (!std::isnan(par.get_haloradius()) && par.get_haloradius() > 0) {
			render.set_color(cr, par.get_halofill());
			cr->set_line_width(par.get_haloradius());
			cr->set_line_cap(Cairo::LINE_CAP_ROUND);
			cr->set_line_join(Cairo::LINE_JOIN_ROUND);
			cr->stroke_preserve();
		}
		render.set_color(cr, par.get_fill());
		cr->fill();
	} else {
		if (!std::isnan(par.get_haloradius()) && par.get_haloradius() > 0) {
			pango_cairo_layout_path(cr->cobj(), m_layout->gobj());
			render.set_color(cr, par.get_halofill());
			cr->set_line_width(par.get_haloradius());
			cr->set_line_cap(Cairo::LINE_CAP_ROUND);
			cr->set_line_join(Cairo::LINE_JOIN_ROUND);
			cr->stroke();
		}
		cr->begin_new_path();
		cr->move_to(x, y);
		render.set_color(cr, par.get_fill());
		m_layout->show_in_cairo_context(cr);
	}
	cr->pop_group_to_source();
	cr->paint_with_alpha(par.get_opacity());
}

void CartoRender::RenderObj::draw_text(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::TextParams& par, const MultiPoint& coords) const
{			
	if (coords.empty())
		return;
	if (par.get_name().empty())
		return;
	cr->save();
	typedef std::map<unsigned int,TextLayout> layoutmap_t;
	layoutmap_t layoutmap;
	layoutmap[0] = TextLayout(render, cr, par);
	if (!layoutmap[0].is_valid()) {
		cr->restore();
		return;
	}
	CartoCSS::BasicTextParams::placementorigins_t placements(par.parse_placements());
	for (const auto& pt : coords) {
		double ptx(render.transformx(pt)), pty(render.transformy(pt));
		for (const auto& placement : placements) {
			layoutmap_t::iterator lm(layoutmap.find(placement.get_fontsize()));
			if (lm == layoutmap.end()) {
				bool ok;
				std::tie(lm, ok) = layoutmap.insert(layoutmap_t::value_type(placement.get_fontsize(), TextLayout(render, cr, par, placement.get_fontsize())));
			}
			if (!lm->second.is_valid())
				continue;
			if (lm->second.check_collision(render, cr, par, ptx, pty, placement))
				continue;
			lm->second.store_collision(render, cr, par, ptx, pty, placement);
			lm->second.draw(render, cr, par, ptx, pty, placement);
			break;
		}
	}
	cr->begin_new_path();
	cr->restore();
}

class CartoRender::RenderObj::LineTextLayout {
public:
	LineTextLayout(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::TextParams& par, const LineString& coords, bool debug);
	~LineTextLayout();
	bool is_ok(void) const { return !!m_layout; }

	enum class r_t {
		ok,
		collision,
		outofrange
	};
	r_t paint(double pos);
	void layout(void);

protected:
	static constexpr unsigned int maxorder = 5U;
	static constexpr unsigned int extension = 8U;

	RLine m_line;
	Pango::Rectangle m_inkext;
	Glib::RefPtr<Pango::Layout> m_layout;
	CartoRender& m_render;
	Cairo::RefPtr<Cairo::Context> m_cr;
	const CartoCSS::TextParams& m_par;
	double m_linelen;
	double m_leninc;
	bool m_debug;
};

constexpr unsigned int CartoRender::RenderObj::LineTextLayout::maxorder;
constexpr unsigned int CartoRender::RenderObj::LineTextLayout::extension;

CartoRender::RenderObj::LineTextLayout::LineTextLayout(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::TextParams& par,
						       const LineString& coords, bool debug)
	: m_render(render), m_cr(cr), m_par(par), m_linelen(0), m_leninc(0), m_debug(debug)
{
	if (!m_cr)
		return;
	m_cr->save();
	if (par.get_name().empty())
		return;
	m_line = render.transform(coords);
	if (m_line.size() < 2)
		return;
	m_layout = render.text_layout(m_cr, m_par);
	m_layout->update_from_cairo_context(m_cr);
	m_inkext = m_layout->get_ink_extents();
	m_linelen = m_line.get_length();
	if (m_linelen <= m_inkext.get_width() * (0.5 / Pango::SCALE)) {
		m_layout.reset();
		return;
	}
	m_leninc = std::max(0.02 * m_linelen, m_inkext.get_width() * (0.5 / Pango::SCALE));
	
}

CartoRender::RenderObj::LineTextLayout::~LineTextLayout()
{
	if (m_cr) {
		m_cr->begin_new_path();
		m_cr->restore();
	}
}

CartoRender::RenderObj::LineTextLayout::r_t CartoRender::RenderObj::LineTextLayout::paint(double pos)
{
	if (!is_ok() || std::isnan(pos))
		return r_t::outofrange;
	double pos0(pos - m_inkext.get_width() * (0.5 / Pango::SCALE)), pos1(pos + m_inkext.get_width() * (0.5 / Pango::SCALE));
	if (pos0 < 0 || pos1 > m_linelen)
		return r_t::outofrange;
	RLine linefrac(m_line.get_line_fraction(pos0, pos1));
	if (m_debug) {
		std::cerr << "LineTextLayout::paint: pos " << pos << " [" << pos0 << "..." << pos1 << ']' << std::endl
			  << "  line: " << m_line.to_str() << std::endl
			  << "  linefrac: " << linefrac.to_str() << std::endl
			  << "  maxsegangle: " << (linefrac.get_max_segment_angle() * (180.0 / M_PI))
			  << " width: " << m_inkext.get_width() * (1.0 / Pango::SCALE)
			  << " linelen: " << m_linelen << " linefraclen: " << linefrac.get_length() << std::endl;
		if (!false) {
			RLine::size_type i0, i1;
			RPoint p0(m_line.get_along(i0, pos0));
			RPoint p1(m_line.get_along(i1, pos1));
			std::cerr << "  first point: " << p0.to_str() << " index: " << i0 << std::endl
				  << "  second point: " << p1.to_str() << " index: " << i1 << std::endl;
		}
	}
	if (linefrac.size() < 2 || linefrac.get_max_segment_angle() > (M_PI / 180.0) * m_par.get_maxcharangledelta())
		return r_t::collision;
	// line direction, upright parameter
	if (m_par.get_upright() == CartoCSS::TextParams::upright_t::right) {
		// do nothing
	} else if (m_par.get_upright() == CartoCSS::TextParams::upright_t::left) {
		linefrac.reverse();
	} else {
		if (linefrac.front().get_x() <= linefrac.back().get_x()) {
			if (m_par.get_upright() == CartoCSS::TextParams::upright_t::left_only)
				return r_t::collision;
		} else {
			if (m_par.get_upright() == CartoCSS::TextParams::upright_t::right_only)
				return r_t::collision;
			linefrac.reverse();
		}
	}
	RLine::PolyParam pp(linefrac.polyparameterize2(maxorder, extension));
	if (!pp.is_valid())
		return r_t::collision;
	// draw collision rectangle
	{
		m_cr->begin_new_path();
		{
			double x, y;
			pp.transform(x, y, 0, m_par.get_dy());
			m_cr->move_to(x, y);
		}
		double y0(m_par.get_dy() - m_inkext.get_height() * (0.5 / Pango::SCALE) - m_par.get_margin());
		double y1(m_par.get_dy() + m_inkext.get_height() * (0.5 / Pango::SCALE) + m_par.get_margin());
		double x0(-m_par.get_margin());
		double x1(m_par.get_margin() + m_inkext.get_width() * (1.0 / Pango::SCALE));
		double xinc((x1 - x0) * (1.0 / 64));
		for (int i = 0; i <= 64; ++i) {
			double x, y;
			pp.transform(x, y, x0 + i * xinc, y0);
			m_cr->line_to(x, y);
		}
		for (int i = 64; i >= 0; --i) {
			double x, y;
			pp.transform(x, y, x0 + i * xinc, y1);
			m_cr->line_to(x, y);
		}
		m_cr->close_path();
	}
	{
		bool coll(m_render.detect_collision(m_cr));
		if (false) {
			m_cr->set_source_rgba(1, 0, 1, 1);
			m_cr->set_line_width(1);
			std::vector<double> dasharray;
			dasharray.push_back(2);
			dasharray.push_back(2);
			m_cr->set_dash(dasharray, 0);
			m_cr->set_line_cap(Cairo::LINE_CAP_ROUND);
			m_cr->set_line_join(Cairo::LINE_JOIN_ROUND);
			m_cr->stroke();
			m_cr->unset_dash();
		}
		m_cr->begin_new_path();
		if (coll)
			return r_t::collision;
	}
	{
		{
			double x, y;
			pp.transform(x, y, 0, m_par.get_dy());
			m_cr->move_to(x, y);
		}
		double y0(m_par.get_dy() - m_inkext.get_height() * (0.5 / Pango::SCALE));
		double y1(m_par.get_dy() + m_inkext.get_height() * (0.5 / Pango::SCALE));
		double x1(m_inkext.get_width() * (1.0 / Pango::SCALE));
		double xinc(x1 * (1.0 / 64));
		for (int i = 0; i <= 64; ++i) {
			double x, y;
			pp.transform(x, y, i * xinc, y0);
			m_cr->line_to(x, y);
		}
		for (int i = 64; i >= 0; --i) {
			double x, y;
			pp.transform(x, y, i * xinc, y1);
			m_cr->line_to(x, y);
		}
		m_cr->close_path();
	}
	m_render.store_collision(m_cr);
	if (false) {
		m_cr->set_source_rgba(1, 0, 1, 1);
		m_cr->set_line_width(2);
		m_cr->set_line_cap(Cairo::LINE_CAP_ROUND);
		m_cr->set_line_join(Cairo::LINE_JOIN_ROUND);
		m_cr->stroke();
	}
	m_cr->begin_new_path();
	m_cr->move_to(-m_inkext.get_x() * (1.0 / Pango::SCALE), m_par.get_dy() - (2 * m_inkext.get_y() + m_inkext.get_height()) * (0.5 / Pango::SCALE));
	pango_cairo_layout_path(m_cr->cobj(), m_layout->gobj());
	pp.transform_path(m_cr);
	if (!std::isnan(m_par.get_haloradius()) && m_par.get_haloradius() > 0) {
		m_render.set_color(m_cr, m_par.get_halofill());
		m_cr->set_line_width(m_par.get_haloradius());
		m_cr->set_line_cap(Cairo::LINE_CAP_ROUND);
		m_cr->set_line_join(Cairo::LINE_JOIN_ROUND);
		m_cr->stroke_preserve();
	}
	m_render.set_color(m_cr, m_par.get_fill());
	m_cr->fill();
	if (false) {
		// visualize polygon approximation
		m_cr->begin_new_path();
		double x1(m_inkext.get_width() * (1.0 / Pango::SCALE));
		double xinc(x1 * (1.0 / 64));
		for (int i = 0; i <= 64; ++i) {
			double x, y;
			pp.transform(x, y, i * xinc, 0);
			if (i)
				m_cr->line_to(x, y);
			else
				m_cr->move_to(x, y);
		}
		m_cr->set_source_rgba(1, 0, 1, 1);
		m_cr->set_line_width(1);
		m_cr->set_line_cap(Cairo::LINE_CAP_ROUND);
		m_cr->set_line_join(Cairo::LINE_JOIN_ROUND);
		m_cr->stroke();
	}
	return r_t::ok;	
}

void CartoRender::RenderObj::LineTextLayout::layout(void)
{
	
	// find initial placement
	double pos;
	for (unsigned int x = 0; x < 100; ++x) {
		bool nowork(true);
		pos = 0.5 * m_linelen + x * m_leninc;
		r_t r(paint(pos));
		if (r == r_t::ok)
			break;
		if (r == r_t::collision)
			nowork = false;
		if (x) {
			pos = 0.5 * m_linelen - x * m_leninc;
			r = paint(pos);
			if (r == r_t::ok)
				break;
			if (r == r_t::collision)
				nowork = false;
		}
		pos = std::numeric_limits<double>::quiet_NaN();
		if (nowork)
			break;
	}
	if (std::isnan(pos))
		return;
	if (m_par.get_placement() != CartoCSS::placement_t::line || std::isnan(m_par.get_spacing()) || m_par.get_spacing() <= 0)
		return;
	for (double pos1(pos + m_par.get_spacing() + m_inkext.get_width() * (1.0 / Pango::SCALE)); pos1 < m_linelen;) {
		r_t r(paint(pos1));
		if (r == r_t::outofrange)
			break;
		switch (r) {
		case r_t::collision:
			pos1 += m_leninc;
			break;

		default:
			pos1 += m_par.get_spacing() + m_inkext.get_width() * (1.0 / Pango::SCALE);
		}
	}
	for (double pos1(pos - m_par.get_spacing() - m_inkext.get_width() * (1.0 / Pango::SCALE)); pos1 > 0;) {
		r_t r(paint(pos1));
		if (r == r_t::outofrange)
			break;
		switch (r) {
		case r_t::collision:
			pos1 -= m_leninc;
			break;

		default:
			pos1 -= m_par.get_spacing() + m_inkext.get_width() * (1.0 / Pango::SCALE);
		}
	}
}

void CartoRender::RenderObj::draw_text(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::TextParams& par, const LineString& coords) const
{
	LineTextLayout layout(render, cr, par, coords, false && is_id(4782682));
	if (!layout.is_ok())
		return;
	layout.layout();
}

void CartoRender::RenderObj::draw_shields(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::ShieldParams& par, const MultiPoint& coords) const
{
	if (coords.empty())
		return;
	cr->save();
	double width, height;
	if (!render.shield_to_source(width, height, cr, par) ||
	    std::isnan(width) || std::isnan(height)) {
		cr->restore();
		return;
	}
	CartoCSS::BasicTextParams::placementorigins_t placements(par.parse_placements());
	for (const auto& pt : coords) {
		double ptx(render.transformx(pt)), pty(render.transformy(pt));
		for (const auto& placement : placements) {
			// ignore fontsize (as shield file does not scale anyway
			if (placement.get_fontsize())
				continue;
			double orgnx(ptx), orgny(pty);
			switch (placement.get_horizontalalignment()) {
			case CartoCSS::TextParams::horizontalalignment_t::left:
				break;

			case CartoCSS::TextParams::horizontalalignment_t::right:
				orgnx -= width;
				break;

			default:
				orgnx -= 0.5 * width;
				break;
			}
			switch (placement.get_verticalalignment()) {
			case CartoCSS::TextParams::verticalalignment_t::top:
				break;

			case CartoCSS::TextParams::verticalalignment_t::bottom:
				orgny -= height;
				break;

			default:
				orgny -= 0.5 * height;
				break;
			}
			cr->begin_new_path();
			cr->rectangle(orgnx - par.get_margin(), orgny - par.get_margin(),
				      width + 2 * par.get_margin(), height + 2 * par.get_margin());
			if (render.detect_collision(cr))
				continue;
			cr->begin_new_path();
			cr->rectangle(orgnx, orgny, width, height);
			render.store_collision(cr);
			cr->begin_new_path();
			Cairo::Matrix matrix;
			cr->get_source()->get_matrix(matrix);
			Cairo::Matrix matrix2(matrix);
			matrix2.translate(-orgnx, -orgny);
			cr->get_source()->set_matrix(matrix2);
			cr->paint_with_alpha(par.get_opacity());
			cr->get_source()->set_matrix(matrix);
			break;
		}
	}
	cr->begin_new_path();
	cr->restore();
}

void CartoRender::RenderObj::draw_shields(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::ShieldParams& par, const MultiLineString& coords) const
{
	cr->save();
	double width, height;
	if (!render.shield_to_source(width, height, cr, par) ||
	    std::isnan(width) || std::isnan(height)) {
		cr->restore();
		return;
	}
	for (const LineString& ls : coords) {
		RLine line(render.transform(ls));
		double linelen(line.get_length());
		double leninc(std::max(0.02 * linelen, 1.5 * std::max(width, height)));
		// find initial placement
		double pos;
		RPoint pt;
		for (unsigned int x = 0; x < 100; ++x) {
			bool nowork(true);
			pos = 0.5 * linelen + x * leninc;
			if (pos < linelen) {
				nowork = false;
				RLine::size_type idx;
				pt = line.get_along(idx, pos);
				cr->begin_new_path();
				cr->rectangle(pt.get_x() - 0.5 * width - par.get_margin(), pt.get_y() - 0.5 * height - par.get_margin(),
					      width + 2 * par.get_margin(), height + 2 * par.get_margin());
				if (!render.detect_collision(cr))
					break;
			}
			if (!x)
				continue;
			pos = 0.5 * linelen - x * leninc;
			if (pos > 0) {
				nowork = false;
				RLine::size_type idx;
				pt = line.get_along(idx, pos);
				cr->begin_new_path();
				cr->rectangle(pt.get_x() - 0.5 * width - par.get_margin(), pt.get_y() - 0.5 * height - par.get_margin(),
					      width + 2 * par.get_margin(), height + 2 * par.get_margin());
				if (!render.detect_collision(cr))
					break;
			}
			pos = std::numeric_limits<double>::quiet_NaN();
			if (nowork)
				break;
		}
		if (std::isnan(pos))
			continue;
		cr->begin_new_path();
		cr->rectangle(pt.get_x() - 0.5 * width, pt.get_y() - 0.5 * height, width, height);
		render.store_collision(cr);
		if (false) {
			std::cerr << "shield: coord " << pt.get_x() << ',' << pt.get_y() << ' '
				  << width << 'x' << height << " file " << par.get_file()
				  << " text \"" << par.get_name() << '"' << std::endl;
			Cairo::RefPtr<Cairo::Pattern> pat(cr->get_source());
			cr->set_source_rgba(1, 0, 1, 1);
			cr->set_line_width(3);
			cr->set_line_cap(Cairo::LINE_CAP_ROUND);
			cr->set_line_join(Cairo::LINE_JOIN_ROUND);
			cr->stroke();
			cr->set_source(pat);
		}
		cr->begin_new_path();
		{
			Cairo::Matrix matrix;
			cr->get_source()->get_matrix(matrix);
			Cairo::Matrix matrix2(matrix);
			matrix2.translate(0.5 * width - pt.get_x(), 0.5 * height - pt.get_y());
			cr->get_source()->set_matrix(matrix2);
			cr->paint_with_alpha(par.get_opacity());
			cr->get_source()->set_matrix(matrix);
		}
		if (par.get_placement() != CartoCSS::placement_t::line || std::isnan(par.get_spacing()) || par.get_spacing() <= 0)
			continue;
		for (double pos1(pos + par.get_spacing()); pos1 < linelen;) {
			RLine::size_type idx;
			pt = line.get_along(idx, pos1);
			cr->begin_new_path();
			cr->rectangle(pt.get_x() - 0.5 * width - par.get_margin(), pt.get_y() - 0.5 * height - par.get_margin(),
				      width + 2 * par.get_margin(), height + 2 * par.get_margin());
			if (render.detect_collision(cr)) {
				pos1 += leninc;
				continue;
			}
			cr->begin_new_path();
			cr->rectangle(pt.get_x() - 0.5 * width, pt.get_y() - 0.5 * height, width, height);
			render.store_collision(cr);
			cr->begin_new_path();
			{
				Cairo::Matrix matrix;
				cr->get_source()->get_matrix(matrix);
				Cairo::Matrix matrix2(matrix);
				matrix2.translate(0.5 * width - pt.get_x(), 0.5 * height - pt.get_y());
				cr->get_source()->set_matrix(matrix2);
				cr->paint_with_alpha(par.get_opacity());
				cr->get_source()->set_matrix(matrix);
			}
			pos1 += par.get_spacing();
		}
		for (double pos1(pos - par.get_spacing()); pos1 > 0;) {
			RLine::size_type idx;
			pt = line.get_along(idx, pos1);
			cr->begin_new_path();
			cr->rectangle(pt.get_x() - 0.5 * width - par.get_margin(), pt.get_y() - 0.5 * height - par.get_margin(),
				      width + 2 * par.get_margin(), height + 2 * par.get_margin());
			if (render.detect_collision(cr)) {
				pos1 -= leninc;
				continue;
			}
			cr->begin_new_path();
			cr->rectangle(pt.get_x() - 0.5 * width, pt.get_y() - 0.5 * height, width, height);
			render.store_collision(cr);
			cr->begin_new_path();
			{
				Cairo::Matrix matrix;
				cr->get_source()->get_matrix(matrix);
				Cairo::Matrix matrix2(matrix);
				matrix2.translate(0.5 * width - pt.get_x(), 0.5 * height - pt.get_y());
				cr->get_source()->set_matrix(matrix2);
				cr->paint_with_alpha(par.get_opacity());
				cr->get_source()->set_matrix(matrix);
			}
			pos1 -= par.get_spacing();
		}
	}
	cr->restore();
}

void CartoRender::RenderObj::draw_linepattern(CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::LinePatternParams& par, const LineString& coords) const
{
	if (coords.size() < 2)
		return;
	double width, height;
	cr->save();
	if (!render.set_pattern(width, height, par.get_file(), cr, true, false)) {
		cr->restore();
		return;
	}
	RLine line(render.transform(coords));
	{
		double len(line.get_length());
		if (len < width) {
			cr->restore();
			return;
		}
	}
	if (false) {
		cr->move_to(line.front().get_x(), line.front().get_y());
		for (RLine::size_type i(1), n(line.size()); i < n; ++i)
			cr->line_to(line[i].get_x(), line[i].get_y());
		cr->set_source_rgba(1, 0, 1, 1);
		cr->set_line_width(3);
		cr->set_line_cap(Cairo::LINE_CAP_ROUND);
		cr->set_line_join(Cairo::LINE_JOIN_ROUND);
		cr->stroke();
	}
	double len(0);
	Cairo::RefPtr<Cairo::Pattern> pattern(cr->get_source());
	for (RLine::size_type i(1), n(line.size()); i < n; ++i) {
		RPoint diff(line[i] - line[i-1]);
		double slen(diff.get_length());
		cr->push_group();
		cr->save();
		cr->rectangle(0, 0, slen, height);
		cr->clip();
		{
			Cairo::Matrix matrix;
			pattern->get_matrix(matrix);
			Cairo::Matrix matrix2(matrix);
			matrix2.translate(-len, 0);
			pattern->set_matrix(matrix2);
	       		cr->set_source(pattern);
			cr->paint();
			pattern->set_matrix(matrix);
		}
		cr->restore();
		cr->pop_group_to_source();
		{
			Cairo::RefPtr<Cairo::Pattern> pat(cr->get_source());
			Cairo::Matrix matrix;
			pat->get_matrix(matrix);
			matrix.translate(0, 0.5 * height);
			matrix.rotate(diff.get_arg());
			matrix.translate(-line[i-1].get_x(), -line[i-1].get_y());
			pat->set_matrix(matrix);
		}
		cr->paint();
		len += slen;
	}
	cr->restore();
}

CartoRender::RenderObjPoints::RenderObjPoints(const MultiPoint& pts, const fields_t& fields, id_t id, int32_t zorder)
	: RenderObj(fields, id, zorder), m_points(pts)
{
}

CartoRender::RenderObjPoints::~RenderObjPoints()
{
}

void CartoRender::RenderObjPoints::draw(std::ostream *debugout, CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::ObjectParameters& par) const
{
	if (!par.get_shield().empty())
		for (const auto& sh : par.get_shield())
			draw_shields(render, cr, sh.second, m_points);
	if (!par.get_marker().empty())
		for (const auto& mkr : par.get_marker())
			draw_marker(render, cr, mkr.second, m_points);
	if (!par.get_text().empty())
		for (const auto& txt : par.get_text())
			draw_text(render, cr, txt.second, m_points);
	if (log_missing_symbolizer && debugout && (!par.get_line().empty() ||
						   !par.get_linepattern().empty() ||
						   !par.get_polygon().empty() ||
						   !par.get_polygonpattern().empty())) {
		print(*debugout << "No drawing code for:" << std::endl, 2);
		par.print(*debugout, 2) << std::endl;
	}
}

std::ostream& CartoRender::RenderObjPoints::print(std::ostream& os, unsigned int indent) const
{
	RenderObj::print(os, indent);
	get_points().print(os << std::string(indent + 2, ' ') << "POINTS ") << " #points " << get_points().size() << std::endl;
	return os;
}

CartoRender::RenderObjLines::RenderObjLines(const MultiLineString& ln, const fields_t& fields, id_t id, int32_t zorder)
	: RenderObj(fields, id, zorder), m_lines(ln)
{
}

CartoRender::RenderObjLines::~RenderObjLines()
{
}

void CartoRender::RenderObjLines::draw(std::ostream *debugout, CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::ObjectParameters& par) const
{
	bool missing_symbolizer(false);
	if (!par.get_line().empty()) {
		cr->save();
		cr->begin_new_path();
		render.set_path(cr, m_lines);
		for (const auto& ln : par.get_line()) {
			set_line_context(cr, ln.second);
			//double get_offset(void) const { return m_offset; }
			cr->stroke_preserve();
		}
		cr->begin_new_path();
		cr->restore();
	}
	for (const auto& ln : par.get_linepattern())
		for (const LineString& ls : m_lines)
			draw_linepattern(render, cr, ln.second, ls);
	for (const auto& mkr : par.get_marker())
		for (const LineString& ls : m_lines)
			draw_marker(render, cr, mkr.second, ls);
	for (const auto& sh : par.get_shield())
		draw_shields(render, cr, sh.second, m_lines);
	for (const auto& txt : par.get_text())
		for (const LineString& ls : m_lines)
			draw_text(render, cr, txt.second, ls);
	if (log_missing_symbolizer && debugout && (missing_symbolizer ||
						   !par.get_polygon().empty() ||
						   !par.get_polygonpattern().empty())) {
		print(*debugout << "No drawing code for:" << std::endl, 2);
		par.print(*debugout, 2) << std::endl;
	}
}

std::ostream& CartoRender::RenderObjLines::print(std::ostream& os, unsigned int indent) const
{
	RenderObj::print(os, indent);
	get_lines().print(os << std::string(indent + 2, ' ') << "LINES ") << " #lines " << get_lines().size() << std::endl;
	return os;
}

CartoRender::RenderObjPolygons::RenderObjPolygons(const MultiPolygonHole& p, const fields_t& fields, id_t id, int32_t zorder)
	: RenderObj(fields, id, zorder), m_polygons(p)
{
}

CartoRender::RenderObjPolygons::~RenderObjPolygons()
{
}

void CartoRender::RenderObjPolygons::draw(std::ostream *debugout, CartoRender& render, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::ObjectParameters& par) const
{
	bool missing_symbolizer(false);
	if (!par.get_polygon().empty() || !par.get_polygonpattern().empty()) {
		cr->save();
		cr->begin_new_path();
		render.set_path(cr, m_polygons);
		for (const auto& pp : par.get_polygon()) {
			render.set_color(cr, pp.second.get_fill());
			cr->fill_preserve();
		}
		if (!par.get_polygonpattern().empty())
			cr->clip_preserve();
		for (const auto& pp : par.get_polygonpattern()) {
			double pwidth, pheight;
			if (!render.set_pattern(pwidth, pheight, pp.second.get_file(), cr, true, false))
				continue;
			cr->paint_with_alpha(pp.second.get_opacity());
		}
		cr->begin_new_path();
		cr->restore();
	}
	for (const auto& ln : par.get_line()) {
		cr->save();
		cr->begin_new_path();
		render.set_path(cr, m_polygons, ln.second.get_offset());
		set_line_context(cr, ln.second);
		cr->stroke_preserve();
 		cr->begin_new_path();
		cr->restore();
	}
	for (const auto& ln : par.get_linepattern()) {
		for (const PolygonHole& ph : m_polygons) {
			{
				LineString ls(ph.get_exterior());
				ls.push_back(ls.front());
				draw_linepattern(render, cr, ln.second, ls);
			}
			for (unsigned int i(0), n(ph.get_nrinterior()); i < n; ++i) {
				LineString ls(ph[i]);
				ls.push_back(ls.front());
				draw_linepattern(render, cr, ln.second, ls);
			}
		}
	}
	for (const auto& mkr : par.get_marker()) {
		for (const PolygonHole& ph : m_polygons) {
			{
				LineString ls(ph.get_exterior());
				ls.push_back(ls.front());
				draw_marker(render, cr, mkr.second, ls);
			}
			for (unsigned int i(0), n(ph.get_nrinterior()); i < n; ++i) {
				LineString ls(ph[i]);
				ls.push_back(ls.front());
				draw_marker(render, cr, mkr.second, ls);
			}
		}
	}
	for (const auto& sh : par.get_shield()) {
		MultiLineString mls;
		for (const PolygonHole& ph : m_polygons) {
			{
				mls.push_back(ph.get_exterior());
				mls.back().push_back(mls.back().front());
			}
			for (unsigned int i(0), n(ph.get_nrinterior()); i < n; ++i) {
			        mls.push_back(ph[i]);
				mls.back().push_back(mls.back().front());
			}
		}
		draw_shields(render, cr, sh.second, mls);
	}
	for (const auto& txt : par.get_text()) {
		if (txt.second.get_placement() == CartoCSS::placement_t::interior) {
			RenderObjPoints::ptr_t p(render.create_points(m_polygons, 0, 0));
			if (!p)
				continue;
			draw_text(render, cr, txt.second, p->get_points());
			continue;
		}
		for (const PolygonHole& ph : m_polygons) {
			{
				LineString ls(ph.get_exterior());
				ls.push_back(ls.front());
				draw_text(render, cr, txt.second, ls);
			}
			for (unsigned int i(0), n(ph.get_nrinterior()); i < n; ++i) {
				LineString ls(ph[i]);
				ls.push_back(ls.front());
				draw_text(render, cr, txt.second, ls);
			}
		}
	}
	if (log_missing_symbolizer && debugout && missing_symbolizer) {
		print(*debugout << "No drawing code for:" << std::endl, 2);
		par.print(*debugout, 2) << std::endl;
	}
}

std::ostream& CartoRender::RenderObjPolygons::print(std::ostream& os, unsigned int indent) const
{
	RenderObj::print(os, indent);
	get_polygons().print(os << std::string(indent + 2, ' ') << "POLYS ") << " #polys " << get_polygons().size() << std::endl;
	return os;
}

CartoCSS::Expr::const_ptr_t CartoRender::RenderObjVisitor::modify(const CartoCSS::Expr& e)
{
	{
		CartoCSS::ClassID clsid;
		if (e.is_type(clsid))
			return CartoCSS::Expr::create(m_obj && m_obj->get_classid() == clsid);
	}
	{
		std::string fld;
		if (e.is_field(fld)) {
			if (!m_obj)
				return CartoCSS::Expr::create();
			return CartoCSS::Expr::create(m_obj->find_field(fld));
		}
	}
	return CartoCSS::Expr::const_ptr_t();
}

CartoRender::LayerAllocVisitor::~LayerAllocVisitor()
{
	if (m_stack.empty())
		return;
	std::cerr << "Layer stack not empty:";
	while (!m_stack.empty()) {
		std::cerr << ' ' << m_stack.top().first;
		m_stack.pop();
	}
	std::cerr << std::endl;
}

void CartoRender::LayerAllocVisitor::visit(const CartoCSS::Expr& e)
{
	if (!m_curblock)
		return;
	std::string v;
	if (!e.is_copylayer(v))
		return;
	m_stack.push(stackel_t(v, m_curblock));
}

void CartoRender::LayerAllocVisitor::visit(const CartoCSS::SubBlock& s)
{
	m_curblock = &s;
}

void CartoRender::LayerAllocVisitor::visitstatements(const CartoCSS::SubBlock& s)
{
	m_curblock = nullptr;
}

void CartoRender::LayerAllocVisitor::visitend(const CartoCSS::SubBlock& s)
{
	while (!m_stack.empty() && m_stack.top().second == &s)
		m_stack.pop();
}

void CartoRender::LayerAllocVisitor::visit(const CartoCSS::VariableAssignments& v)
{
	if (m_stack.empty())
		m_alloc.allocate(std::string());
	else
		m_alloc.allocate(m_stack.top().first);
}

CartoCSS::Expr::const_ptr_t CartoRender::LayerSelectVisitor::modify(const CartoCSS::Expr& e)
{
	std::string v;
	if (!e.is_copylayer(v))
		return CartoCSS::Expr::const_ptr_t();
	return CartoCSS::Expr::create(v == m_layer);
}

void CartoRender::UnresolvedVisitor::visit(const CartoCSS::SubBlock& e)
{
	m_unresolved = true;
}

void CartoRender::ApplyErrorVisitor::error(const CartoCSS::VariableAssignments::Assignment& assignment)
{
	m_error = true;
	if (!m_dbg)
		return;
	assignment.print(*m_dbg << "ERROR applying variable" << std::endl, 2);
}

void CartoRender::ApplyErrorVisitor::error(const CartoCSS::VariableAssignments::Assignment& assignment, const std::exception& e)
{
	m_error = true;
	if (!m_dbg)
		return;
	assignment.print(*m_dbg << "ERROR exception " << e.what() << " applying variable" << std::endl, 2);
}

void CartoRender::ApplyErrorVisitor::error(const CartoCSS::SubBlock& s, const std::set<std::string>& layers)
{
	m_error = true;
	if (!m_dbg)
		return;
	*m_dbg << "ERROR in condition";
	if (!layers.empty()) {
		*m_dbg << " (layers";
		for (const std::string& l : layers)
			*m_dbg << ' ' << l;
		*m_dbg << ')';
	}
	s.get_conditions().print(*m_dbg << std::endl, 2) << std::endl;
	if (!true)
		return;
	*m_dbg << ' ';
	for (const CartoCSS::SubBlock::ConditionChain& cc : s.get_conditions()) {
		std::string layer;
		if (cc.is_condcopylayer(layer))
			*m_dbg << " [true: " << layer << ']';
		else
			*m_dbg << " [false]";
	}
	*m_dbg << std::endl;
}

CartoRender::Pattern::Pattern(void)
	: m_refcount(0)
{
}

CartoRender::Pattern::~Pattern()
{
}

class CartoRender::PatternSVG : public Pattern {
public:
	PatternSVG(const std::string& fn, const Cairo::RefPtr<Cairo::Context>& cr);

	virtual bool set_context(double& width, double& height, const Cairo::RefPtr<Cairo::Context>& cr, bool repeat, bool center);

protected:
	static constexpr double scale = 1;
	Cairo::RefPtr<Cairo::Surface> m_surface;
	int m_width;
	int m_height;
};

CartoRender::PatternSVG::PatternSVG(const std::string& fn, const Cairo::RefPtr<Cairo::Context>& cr)
	: m_width(0), m_height(0)
{
	if (!cr)
		return;
	GError *err(0);
	RsvgHandle *h(rsvg_handle_new_from_file(fn.c_str(), &err));
	if (!h) {
		std::cerr << "Cannot open " << fn;
		if (err)
			std::cerr << ": " << err->message;
		std::cerr << std::endl;
		g_error_free(err);
		err = 0;
		return;
	}
	RsvgDimensionData dim;
	rsvg_handle_get_dimensions(h, &dim);
	if (false)
		std::cerr << "Pattern " << fn << " WxH " << dim.width << ' ' << dim.height << std::endl;
	m_width = dim.width;
	m_height = dim.height;
	m_surface = Cairo::Surface::create(cr->get_target(), Cairo::CONTENT_COLOR_ALPHA, m_width, m_height);
	Cairo::RefPtr<Cairo::Context> ctx(Cairo::Context::create(m_surface));
	bool ok(rsvg_handle_render_cairo(h, ctx->cobj()));
	g_object_unref(G_OBJECT(h));
	if (!ok) {
		std::cerr << "Cannot render " << fn << std::endl;
		m_surface.clear();
	}
}

bool CartoRender::PatternSVG::set_context(double& width, double& height, const Cairo::RefPtr<Cairo::Context>& cr, bool repeat, bool center)
{
	if (!cr || !m_surface)
		return false;
	Cairo::RefPtr<Cairo::SurfacePattern> pat(Cairo::SurfacePattern::create(m_surface));
	pat->set_extend(repeat ? Cairo::EXTEND_REPEAT : Cairo::EXTEND_NONE);
	pat->set_filter(Cairo::FILTER_FAST);
	{
		Cairo::Matrix matrix(pat->get_matrix());
		matrix.scale(scale, scale);
		pat->set_matrix(matrix);
	}
	if (center) {
		Cairo::Matrix matrix(pat->get_matrix());
		matrix.translate(0.5 * m_width, 0.5 * m_height);
		pat->set_matrix(matrix);
	}
	cr->set_source(pat);
	width = m_width * (1.0 / scale);
	height = m_height * (1.0 / scale);
	return true;
}

class CartoRender::PatternPNG : public Pattern {
public:
	PatternPNG(const std::string& fn);

	virtual bool set_context(double& width, double& height, const Cairo::RefPtr<Cairo::Context>& cr, bool repeat, bool center);

protected:
	static constexpr double scale = 1;
	Cairo::RefPtr<Cairo::ImageSurface> m_surface;
};

CartoRender::PatternPNG::PatternPNG(const std::string& fn)
{
	m_surface = Cairo::ImageSurface::create_from_png(fn);
	if (!m_surface)
		std::cerr << "Cannot read PNG file " << fn << std::endl;
	
}

bool CartoRender::PatternPNG::set_context(double& width, double& height, const Cairo::RefPtr<Cairo::Context>& cr, bool repeat, bool center)
{
	if (!cr || !m_surface)
		return false;
	Cairo::RefPtr<Cairo::SurfacePattern> pat(Cairo::SurfacePattern::create(m_surface));
	pat->set_extend(repeat ? Cairo::EXTEND_REPEAT : Cairo::EXTEND_NONE);
	pat->set_filter(Cairo::FILTER_FAST);
	{
		Cairo::Matrix matrix(pat->get_matrix());
		matrix.scale(scale, scale);
		pat->set_matrix(matrix);
	}
	if (center) {
		Cairo::Matrix matrix(pat->get_matrix());
		matrix.translate(0.5 * m_surface->get_width(), 0.5 * m_surface->get_height());
		pat->set_matrix(matrix);
	}
	cr->set_source(pat);
	width = m_surface->get_width() * (1.0 / scale);
	height = m_surface->get_height() * (1.0 / scale);
	return true;
}

std::string CartoRender::RPoint::to_str(void) const
{
	std::ostringstream oss;
	oss << '(' << std::fixed << std::setprecision(2) << m_x << ',' << m_y << ')';
	return oss.str();
}

// signed triangle area
double CartoRender::RPoint::area(const RPoint& p0, const RPoint& p1, const RPoint& p2)
{
	double p2x = p2.get_x();
	double p2y = p2.get_y();
	double p0x = p0.get_x() - p2x;
	double p0y = p0.get_y() - p2y;
	double p1x = p1.get_x() - p2x;
	double p1y = p1.get_y() - p2y;
	return 0.5 * (p0x * (p1y - p0y) - p0y * (p1x - p0x));
}

CartoRender::RPoint CartoRender::RLine::PolyParam::transform(const RPoint& p) const
{
	double x, y;
	transform(x, y, p.get_x(), p.get_y());
	return RPoint(x, y);
}

void CartoRender::RLine::PolyParam::transform(double& rx, double& ry, double x, double y) const
{
	double dx(-m_polyy.evalderiv(x));
	double dy(m_polyx.evalderiv(x));
	if (true) {
		double s(1.0 / sqrt(dx * dx + dy * dy));
		dx *= s;
		dy *= s;
	}	
	rx = m_polyx.eval(x) + y * dx;
	ry = m_polyy.eval(x) + y * dy;
}

void CartoRender::RLine::PolyParam::transform_path(const Cairo::RefPtr<Cairo::Context>& cr) const
{
	if (!cr)
		return;
	cairo_path_t *current_path(cairo_copy_path(cr->cobj()));
	cairo_new_path(cr->cobj());
	// transform path
	for (int i=0; i < current_path->num_data; i += current_path->data[i].header.length) {
		cairo_path_data_t *data(&current_path->data[i]);
		switch (data->header.type) {
		case CAIRO_PATH_CURVE_TO:
			transform(data[3].point.x, data[3].point.y, data[3].point.x, data[3].point.y);
			transform(data[2].point.x, data[2].point.y, data[2].point.x, data[2].point.y);
		case CAIRO_PATH_MOVE_TO:
		case CAIRO_PATH_LINE_TO:
			transform(data[1].point.x, data[1].point.y, data[1].point.x, data[1].point.y);
			break;
		case CAIRO_PATH_CLOSE_PATH:
			break;
		default:
			g_assert_not_reached();
		}
	}
	cairo_append_path(cr->cobj(), current_path);
	cairo_path_destroy(current_path);
}

double CartoRender::RLine::get_length(void) const
{
	double len(0);
	for (size_type i(1), n(size()); i < n; ++i)
		len += (operator[](i) - operator[](i - 1)).get_length();
	return len;
}

CartoRender::RPoint CartoRender::RLine::get_along(size_type& idx, double dist) const
{
	if (empty() || std::isnan(dist))
		return RPoint(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN());
	if (dist <= 0) {
		idx = 0;
		return front();
	}
	for (size_type i(1), n(size()); i < n; ++i) {
		RPoint diff(operator[](i) - operator[](i - 1));
		double len(diff.get_length());
		if (dist >= len || len <= 0) {
			dist -= len;
			continue;
		}
		idx = i - 1;
		return operator[](i - 1) + diff * (dist / len);
	}
	idx = size() - 1;
	return back();
}

CartoRender::RLine CartoRender::RLine::get_line_fraction(double distb, double diste) const
{
	if (std::isnan(distb) || std::isnan(diste))
		return RLine();
	size_type i0, i1;
	RPoint p0(get_along(i0, distb));
	RPoint p1(get_along(i1, diste));
	if (i0 > i1)
		return RLine();
	RLine r;
	r.push_back(p0);
	if (i0 < i1) {
		++i0;
		const RPoint& p(operator[](i0));
		if (p != p0 && p != p1)
			r.push_back(p);
	}
	while (i0 < i1) {
		++i0;
		const RPoint& p(operator[](i0));
		if (p != p1)
			r.push_back(p);
	}
	r.push_back(p1);
	return r;
}

double CartoRender::RLine::get_max_segment_angle(void) const
{
	double a(0);
	double aprev;
	for (size_type i(1), n(size()); i < n; ++i) {
		double a1(-aprev);
		aprev = (operator[](i) - operator[](i-1)).get_arg();
		if (i < 2)
			continue;
		a1 += aprev;
		a = std::max(a, std::fabs(a1));
	}
	return a;
}

void CartoRender::RLine::visvalingam_whyatt(double thr)
{
	for (;;) {
		size_type n(size());
		if (n < 3)
			break;
		size_type index(n);
		double area(std::numeric_limits<double>::max());
		for (size_type i(2); i < n; ++i) {
			double a(std::fabs(RPoint::area(operator[](i-2), operator[](i-1), operator[](i))));
			if (a >= area)
				continue;
			area = a;
			index = i;
		}
		if (index >= n || area > thr)
			break;
		erase(begin() + index);
	}
}

#ifdef HAVE_EIGEN3

namespace {

#ifdef __WIN32__
__attribute__((force_align_arg_pointer))
#endif

void least_squares_solution(Eigen::VectorXd& r0, Eigen::VectorXd& r1, const Eigen::MatrixXd& A, const Eigen::VectorXd& b0, const Eigen::VectorXd& b1)
{
	if (false) {
		// traditional
		Eigen::MatrixXd R((A.transpose() * A).inverse() * A.transpose());
		r0 = R * b0;
		r1 = R * b1;
	} else if (true) {
		// cholesky
		typedef Eigen::LDLT<Eigen::MatrixXd> ldlt_t;
		//LDLT< typename MatrixBase< Derived >::PlainObject >
		ldlt_t ldlt(A.transpose() * A);
		r0 = ldlt.solve(A.transpose() * b0);
		r1 = ldlt.solve(A.transpose() * b1);
	} else if (false) {
		// householder QR
		typedef Eigen::ColPivHouseholderQR<Eigen::MatrixXd> householder_t;
		householder_t householder(A);
		r0 = householder.solve(b0);
		r1 = householder.solve(b1);
	} else if (false) {
		// jacobi SVD
		typedef Eigen::JacobiSVD<Eigen::MatrixXd> jacobi_t;
		jacobi_t jacobi(A, Eigen::ComputeThinU | Eigen::ComputeThinV);
		r0 = jacobi.solve(b0);
		r1 = jacobi.solve(b1);
	} else {
		typedef Eigen::JacobiSVD<Eigen::MatrixXd, Eigen::FullPivHouseholderQRPreconditioner> jacobi_t;
		jacobi_t jacobi(A, Eigen::ComputeFullU | Eigen::ComputeFullV);
		r0 = jacobi.solve(b0);
		r1 = jacobi.solve(b1);
	}
}

};

CartoRender::RLine::PolyParam CartoRender::RLine::polyparameterize(unsigned int maxorder) const
{
	maxorder = std::min(maxorder, static_cast<unsigned>(size()));
	if (maxorder < 2)
		return PolyParam(PolyParam::Poly1D(), PolyParam::Poly1D());
	if (size() == 2) {
		PolyParam::Poly1D x, y;
		RPoint d(back() - front());
		double invl(1.0 / d.get_length());
		x.push_back(front().get_x());
		y.push_back(front().get_y());
		x.push_back(d.get_x() * invl);
		y.push_back(d.get_y() * invl);
		return PolyParam(x, y);
	}
	Eigen::MatrixXd m(size(), maxorder);
	Eigen::VectorXd vx(size()), vy(size());
	double len(0);
	for (size_type i(0), n(size()); i < n; ++i) {
		if (i)
			len += (operator[](i) - operator[](i-1)).get_length();
		double len1(1);
		for (unsigned int j = 0; j < maxorder; ++j, len1 *= len)
			m(i, j) = len1;
		vx(i) = operator[](i).get_x();
		vy(i) = operator[](i).get_y();
	}
	Eigen::VectorXd rx, ry;
	least_squares_solution(rx, ry, m, vx, vy);
	return PolyParam(PolyParam::Poly1D(rx.data(), rx.data() + rx.size()), PolyParam::Poly1D(ry.data(), ry.data() + ry.size()));
}

CartoRender::RLine::PolyParam CartoRender::RLine::polyparameterize2(unsigned int maxorder, unsigned int extension) const
{
	maxorder = std::min(maxorder, static_cast<unsigned>(size()));
	if (maxorder < 2)
		return PolyParam(PolyParam::Poly1D(), PolyParam::Poly1D());
	if (size() == 2) {
		PolyParam::Poly1D x, y;
		RPoint d(back() - front());
		double invl(1.0 / d.get_length());
		x.push_back(front().get_x());
		y.push_back(front().get_y());
		x.push_back(d.get_x() * invl);
		y.push_back(d.get_y() * invl);
		return PolyParam(x, y);
	}
	static constexpr unsigned int samples = 64;
	Eigen::MatrixXd m(2 * extension + samples + 1, maxorder);
	Eigen::VectorXd vx(2 * extension + samples + 1), vy(2 * extension + samples + 1);
	const double length(get_length());
	const double linc(length * (1.0 / samples));
	for (unsigned int i = 0; i <= samples; ++i) {
		double len(i * linc);
		size_type index;
		RPoint pt(get_along(index, len));
		double len1(1);
		for (unsigned int j = 0; j < maxorder; ++j, len1 *= len)
			m(2 * extension + i, j) = len1;
		vx(2 * extension + i) = pt.get_x();
		vy(2 * extension + i) = pt.get_y();
	}
	if (extension) {
		const RPoint& p0(operator[](0));
		const RPoint& p1(operator[](size()-1));
		const RPoint d0((p0 - operator[](1)).get_unitvector() * linc);
		const RPoint d1((p1 - operator[](size()-2)).get_unitvector() * linc);
		for (unsigned int i = 1; i <= extension; ++i) {
			double len(i * -linc);
			RPoint pt(p0 + d0 * i);
			double len1(1);
			for (unsigned int j = 0; j < maxorder; ++j, len1 *= len)
				m(2 * i - 2, j) = len1;
			vx(2 * i - 2) = pt.get_x();
			vy(2 * i - 2) = pt.get_y();
			len = length + i * linc;
			pt = p1 + d1 * i;
			len1 = 1;
			for (unsigned int j = 0; j < maxorder; ++j, len1 *= len)
				m(2 * i - 1, j) = len1;
			vx(2 * i - 1) = pt.get_x();
			vy(2 * i - 1) = pt.get_y();
		}
	}
	Eigen::VectorXd rx, ry;
	least_squares_solution(rx, ry, m, vx, vy);
	if (false) {
		std::cerr << "polyparameterize2:" << std::endl
			  << "  m = ";
		char sep('[');
		for (unsigned int i = 0; i <= samples + 2 * extension; ++i) {
			std::cerr << ' ' << sep;
			sep = ';';
			for (unsigned int j = 0; j < maxorder; ++j)
				std::cerr << ' ' << m(i, j);
		}
		std::cerr << " ];" << std::endl << "  vx = [";
		for (unsigned int i = 0; i <= samples + 2 * extension; ++i)
			std::cerr << ' ' << vx(i);
		std::cerr << " ];" << std::endl << "  vy = [";
		for (unsigned int i = 0; i <= samples + 2 * extension; ++i)
			std::cerr << ' ' << vy(i);
		std::cerr << " ];" << std::endl << "  rx = [";
		for (unsigned int i = 0; i < maxorder; ++i)
			std::cerr << ' ' << rx(i);
		std::cerr << " ];" << std::endl << "  ry = [";
		for (unsigned int i = 0; i < maxorder; ++i)
			std::cerr << ' ' << ry(i);
		std::cerr << " ];" << std::endl;
	}
	return PolyParam(PolyParam::Poly1D(rx.data(), rx.data() + rx.size()), PolyParam::Poly1D(ry.data(), ry.data() + ry.size()));
}

#else

CartoRender::RLine::PolyParam CartoRender::RLine::polyparameterize(unsigned int maxorder) const
{
	if (size() < 2)
		return PolyParam(PolyParam::Poly1D(), PolyParam::Poly1D());
	PolyParam::Poly1D x, y;
	RPoint d(back() - front());
	double invl(1.0 / d.get_length());
	x.push_back(front().get_x());
	y.push_back(front().get_y());
	x.push_back(d.get_x() * invl);
	y.push_back(d.get_y() * invl);
	return PolyParam(x, y);
}

CartoRender::RLine::PolyParam CartoRender::RLine::polyparameterize2(unsigned int maxorder) const
{
	return polyparameterize(maxorder);
}

#endif

std::string CartoRender::RLine::to_str(void) const
{
	std::string r;
	char sep('[');
	for (const_iterator i(begin()), e(end()); i != e; ++i) {
		r.push_back(sep);
		sep = ' ';
		r.append(i->to_str());
	}
	if (sep != ' ')
		r.push_back(sep);
	r.push_back(']');
	return r;
}

constexpr OSMDB::Object::id_t CartoRender::debugobject_off;
constexpr bool CartoRender::log_missing_symbolizer;
constexpr double CartoRender::obstacle_cairo_radius;
constexpr double CartoRender::obstacle_cairo_fontsize;
constexpr bool CartoRender::obstacle_preferamsl;
constexpr double CartoRender::arp_cairo_radius;
constexpr double CartoRender::points_per_inch;
constexpr double CartoRender::pixels_per_inch;

CartoRender::CartoRender(void)
	: m_carto(nullptr), m_zoom(10), m_scale(std::numeric_limits<double>::quiet_NaN()),
	  m_area2pixelscale(std::numeric_limits<double>::quiet_NaN()),
	  m_yoffset(std::numeric_limits<double>::quiet_NaN()), m_debugobject(debugobject_off), m_cssdebuglevel(0U)
{
	m_bbox.set_invalid();
	m_arp.set_invalid();
	clear();
}

CartoRender::CartoRender(const Carto& carto)
	: m_carto(&carto), m_zoom(10), m_scale(std::numeric_limits<double>::quiet_NaN()),
	  m_area2pixelscale(std::numeric_limits<double>::quiet_NaN()),
	  m_yoffset(std::numeric_limits<double>::quiet_NaN()), m_debugobject(debugobject_off), m_cssdebuglevel(0U)
{
	m_bbox.set_invalid();
	m_arp.set_invalid();
	clear();
}

const OSMDB *CartoRender::get_osmdb(void) const
{
	if (!m_carto)
		return nullptr;
	return m_carto->get_osmdb();
}

const OSMStaticDB *CartoRender::get_osmsdb(void) const
{
	if (!m_carto)
		return nullptr;
	return m_carto->get_osmstaticdb();
}

const CartoCSS *CartoRender::get_style(void) const
{
	if (!m_carto)
		return nullptr;
	return &m_carto->get_css();
}

const CartoSQL *CartoRender::get_sql(void) const
{
	if (!m_carto)
		return nullptr;
	return &m_carto->get_sql();
}

void CartoRender::set_carto(const Carto *carto)
{
	m_carto = carto;
	clear();
}

void CartoRender::clear(void)
{
	m_points.clear();
	m_lines.clear();
	m_roads.clear();
	m_areas.clear();
	m_boundarylinesland.clear();
	m_icesheetpolygons.clear();
	m_icesheetoutlines.clear();
	m_waterpolygons.clear();
	m_simplifiedwaterpolygons.clear();
	m_collisions.clear();
	m_patterncache.clear();
}

void CartoRender::set_bbox(const Rect& bbox, double max_width_in_points, double max_height_in_points)
{
	m_bbox = bbox;
	m_scale = m_area2pixelscale = m_yoffset = std::numeric_limits<double>::quiet_NaN();
	clear();
	if (m_bbox.is_invalid())
		return;
	{
		Point pt(m_bbox.get_northeast() - m_bbox.get_southwest());
		if (pt.get_lon() <= 0 || pt.get_lat() <= 0)
			return;
		double yoffs(m_bbox.get_northeast().get_lat_webmercator_dbl(0));
		double extw(pt.get_lon_webmercator_dbl(0) - 128);
		double exth(m_bbox.get_southwest().get_lat_webmercator_dbl(0) - yoffs);
		if (std::isnan(extw) || std::isnan(exth) || extw <= 0 || exth <= 0)
			return;
		m_scale = std::min(max_width_in_points / extw, max_height_in_points / exth);
		m_area2pixelscale = extw * exth * m_scale * m_scale * 0.5
			* (pixels_per_inch / points_per_inch) * (pixels_per_inch / points_per_inch)
			* (1.0 / pt.get_lon()) * (1.0 / pt.get_lat());
		m_yoffset = yoffs;
	}
	if (false)
		std::cerr << "Map scale: x " << pixelspermilex() << " y " << pixelspermiley() << " pixels/mile" << std::endl;
}

Rect CartoRender::get_extended_bbox(void) const
{
	Point pdiff(m_bbox.get_northeast() - m_bbox.get_southwest());
	pdiff.set_lon(pdiff.get_lon() >> 4);
	pdiff.set_lat(pdiff.get_lat() >> 4);
	return Rect(m_bbox.get_southwest() - pdiff, m_bbox.get_northeast() + pdiff);
}

double CartoRender::transformx(const Point& pt) const
{
	return ((pt - m_bbox.get_southwest()).get_lon_webmercator_dbl(0) - 128) * m_scale;
}

double CartoRender::transformy(const Point& pt) const
{
	return (pt.get_lat_webmercator_dbl(0) - m_yoffset) * m_scale;
}

double CartoRender::get_width(void) const
{
	return transformx(m_bbox.get_northeast());
}

double CartoRender::get_height(void) const
{
	return transformy(m_bbox.get_southwest());
}

double CartoRender::pixelspermilex(void) const
{
	Point p0(m_bbox.boxcenter());
	Point p1(p0.spheric_course_distance_nmi_dbl(90, 0.5));
	p0 -= p1 - p0;
	return ((p1 - p0).get_lon_webmercator_dbl(0) - 128) * m_scale;
}

double CartoRender::pixelspermiley(void) const
{
	Point p0(m_bbox.boxcenter());
	Point p1(p0.spheric_course_distance_nmi_dbl(0, 0.5));
	p0 -= p1 - p0;
	return (p0.get_lat_webmercator_dbl(0) - p1.get_lat_webmercator_dbl(0)) * m_scale;
}

void CartoRender::load(void)
{
	clear();
	if (m_bbox.is_invalid())
		return;
	{
		const OSMDB *odb(get_osmdb());
		if (odb) {
			m_points = odb->find(m_bbox, OSMDB::typemask_t::point);
			m_lines = odb->find(m_bbox, OSMDB::typemask_t::line);
			m_roads = odb->find(m_bbox, OSMDB::typemask_t::road);
			m_areas = odb->find(m_bbox, OSMDB::typemask_t::area);
		}
	}
	{
		const OSMStaticDB *sdb(get_osmsdb());
		if (sdb) {
			if (get_zoom() >= 1 && get_zoom() <= 3)
				m_boundarylinesland = sdb->find(m_bbox, OSMStaticDB::layer_t::boundarylinesland);
			if (get_zoom() >= 5) {
				m_icesheetpolygons = sdb->find(m_bbox, OSMStaticDB::layer_t::icesheetpolygons);
				m_icesheetoutlines = sdb->find(m_bbox, OSMStaticDB::layer_t::icesheetoutlines);
			}
			if (get_zoom() >= 10)
				m_waterpolygons = sdb->find(m_bbox, OSMStaticDB::layer_t::waterpolygons);
			if (get_zoom() <= 9)
				m_simplifiedwaterpolygons = sdb->find(m_bbox, OSMStaticDB::layer_t::simplifiedwaterpolygons);		
		}
	}
}

bool CartoRender::empty(void) const
{
	return m_points.empty() && m_lines.empty() && m_roads.empty() && m_areas.empty();
}

const std::string& CartoRender::sublayer_name(const std::string& x)
{
	static const std::string defaultname("*default*");
	if (x.empty())
		return defaultname;
	return x;
}

const std::string& CartoRender::sublayer_name_file(const std::string& x)
{
	static const std::string defaultname("*default*");
	if (x.empty())
		return defaultname;
	return x;
}

bool CartoRender::set_pattern(double& width, double& height, const std::string& pat, const Cairo::RefPtr<Cairo::Context>& cr, bool repeat, bool center)
{
	width = height = std::numeric_limits<double>::quiet_NaN();
	{
		patterncache_t::iterator i(m_patterncache.find(pat));
		if (i != m_patterncache.end())
			return i->second->set_context(width, height, cr, repeat, center);
	}
	if (pat.size() < 5) {
		std::cerr << "Invalid pattern file: \"" << pat << '"' << std::endl;
		return false;
	}
	Pattern::ptr_t pattern;
	if (!pat.compare(pat.size() - 4, 4, ".svg")) {
		pattern.reset(new PatternSVG(pat, cr));
	} else if (!pat.compare(pat.size() - 4, 4, ".png")) {
		pattern.reset(new PatternPNG(pat));
	} else {
		std::cerr << "Invalid pattern file: \"" << pat << '"' << std::endl;
		return false;
	}
	m_patterncache[pat] = pattern;
	return pattern->set_context(width, height, cr, repeat, center);
}

CartoRender::coordobstacles_t CartoRender::draw(const Cairo::RefPtr<Cairo::Context>& cr,
						const Cairo::RefPtr<Cairo::PdfSurface>& surface)
{
	static constexpr double obstacle_cairo_radius_1 = 1.2 * obstacle_cairo_radius;
	static constexpr double obstacle_cairo_radius_x1 = 1.2 * 0.50000 * obstacle_cairo_radius;
	static constexpr double obstacle_cairo_radius_y1 = 1.2 * 0.86603 * obstacle_cairo_radius;
	static constexpr double obstacle_cairo_radius_2 = 1.8 * obstacle_cairo_radius;
	static constexpr double obstacle_cairo_radius_x2 = 1.8 * 0.50000 * obstacle_cairo_radius;
	static constexpr double obstacle_cairo_radius_y2 = 1.8 * 0.86603 * obstacle_cairo_radius;
	if (m_bbox.is_invalid() || !m_carto)
		return coordobstacles_t();
	const CartoCSS *style(get_style());
	if (!style)
		return coordobstacles_t();
	m_collisions.clear();
	m_patterncache.clear();
	populate_fonts();
	std::ostream *debugout = &std::cerr;
	std::ofstream debugoutput;
	std::string tmpdir(Glib::get_tmp_dir());
	if (m_cssdebuglevel || m_debugobject != debugobject_off) {
		std::string tmpdir1(Glib::build_filename(tmpdir, "cartorender_XXXXXX"));
		gchar *td(g_strdup(tmpdir1.c_str()));
		gchar *td1(g_mkdtemp(td));
		if (!td1) {
			g_free(td);
			throw std::runtime_error("Cannot create the temporary directory " + tmpdir1);
		}
		tmpdir = td1;
		g_free(td);
		debugoutput.open(Glib::build_filename(tmpdir, "log"));
		debugout = &debugoutput;
	}
	// simplify CSS
	CartoCSS::Statement::const_ptr_t css(style->get_statements());
	if (!css)
		return coordobstacles_t();
	if (m_cssdebuglevel >= 1U) {
		std::ofstream ofs(Glib::build_filename(tmpdir, "css.initial"));
		css->print(ofs << "INITIAL CSS" << std::endl, 0);
	}
	{
		CartoCSS::Statement::const_ptr_t p(css->simplify("zoom", CartoCSS::Value(static_cast<int64_t>(get_zoom()))));
		if (p)
			css = p;
	}
	if (m_cssdebuglevel >= 2U) {
		std::ofstream ofs(Glib::build_filename(tmpdir, "css.zoom"));
		css->print(ofs << "CSS AFTER zoom=" << get_zoom() << std::endl, 0);
	}
	// prefill collisions
	// ARP
	if (!m_arp.is_invalid() && m_bbox.is_inside(m_arp)) {
		double xc(transformx(m_arp)), yc(transformy(m_arp));
		cr->begin_new_path();
		cr->rectangle(xc - arp_cairo_radius, yc - arp_cairo_radius, 2 * arp_cairo_radius, 2 * arp_cairo_radius);
		store_collision(cr);
		cr->begin_new_path();
	}
	// Obstacles
	for (obstacles_t::const_iterator oi(m_obstacles.begin()), oe(m_obstacles.end()); oi != oe; ++oi) {
		if (oi->get_pt().is_invalid() || !m_bbox.is_inside(oi->get_pt()))
			continue;
		double xc(transformx(oi->get_pt())), yc(transformy(oi->get_pt()));
		cr->begin_new_path();
		cr->rectangle(xc - obstacle_cairo_radius_2, yc - obstacle_cairo_radius_2, 2 * obstacle_cairo_radius_2, 2 * obstacle_cairo_radius_2);
		store_collision(cr);
		cr->begin_new_path();
		{
			double val(std::numeric_limits<double>::quiet_NaN());
			if (!std::isnan(oi->get_agl()) && (!obstacle_preferamsl || std::isnan(oi->get_amsl()))) {
				val = oi->get_agl();
				cr->select_font_face("sans", Cairo::FONT_SLANT_NORMAL, Cairo::FONT_WEIGHT_NORMAL);
				cr->set_font_size(obstacle_cairo_fontsize);
			} else if (!std::isnan(oi->get_amsl())) {
				val = oi->get_amsl();
				cr->select_font_face("sans", Cairo::FONT_SLANT_ITALIC, Cairo::FONT_WEIGHT_NORMAL);
				cr->set_font_size(obstacle_cairo_fontsize);
			}
			if (!std::isnan(val)) {
				val *= Point::m_to_ft_dbl;
				std::ostringstream oss;
				oss << std::fixed << std::setprecision(0) << val;
				Cairo::TextExtents ext;
				cr->get_text_extents(oss.str(), ext);
				cr->rectangle(1.2 * obstacle_cairo_radius - ext.x_bearing, -0.5 * ext.height - ext.y_bearing, ext.width, ext.height);
				store_collision(cr);
			}
		}
		cr->begin_new_path();
	}
	// clip region
	{
		double width(get_width()), height(get_height());
		if (surface)
			surface->set_size(width, height);
		cr->save();
		// clip
		cr->rectangle(0, 0, width, height);
		cr->clip();
	}
	// background
	cr->set_operator(Cairo::OPERATOR_SOURCE);
	if (set_color(cr, style->get_mapparameters().get_backgroundcol()))
		cr->paint();
	cr->set_operator(Cairo::OPERATOR_OVER);
	// layers
	for (CartoRender::layerid_t lid(CartoRender::layerid_t::first); lid <= CartoRender::layerid_t::last;
	     lid = static_cast<CartoRender::layerid_t>(static_cast<unsigned int>(lid) + 1)) {
		if (!is_layer_enabled(lid, get_zoom()))
			continue;
		// CSS simplification and information gathering
		CartoCSS::Statement::const_ptr_t layercss(css->simplify(to_str(lid)));
		if (!layercss)
			layercss = css;
		if (m_cssdebuglevel >= 3U) {
			std::string fn(Glib::build_filename(tmpdir, "css.layer." + to_str(lid)));
			std::ofstream ofs(fn.c_str());
			layercss->print(ofs << "CSS FOR LAYER " << lid << std::endl, 0);
		}
		if (layercss->is_empty())
			continue;
		// get layer objects
		renderobjs_t renderobjs(dbquery(lid));
		if (!false && debugout) {
			std::size_t osmobj(0);
			for (const RenderObj::const_ptr_t& ro : renderobjs)
				if (ro)
					osmobj += ro->get_ids().size();
			*debugout << "Layer " << lid << ": " << renderobjs.size() << " (" << osmobj << ") objects" << std::endl;
		}
		if (renderobjs.empty())
			continue;
		if (false && debugout) {
			*debugout << ' ';
			for (const RenderObj::const_ptr_t& ro : renderobjs) {
				if (ro)
					*debugout << ' ' << ro->get_ids_str();
				else
					*debugout << " /";
			}
			*debugout << std::endl;
		}
		// allocate sublayers
		CartoCSS::LayerAllocator layeralloc;
		{
			LayerAllocVisitor vis(layeralloc);
			layercss->visit(vis);
		}
		if (!false && debugout) {
			*debugout << "Layer " << lid << ": sublayers";
			for (CartoCSS::LayerAllocator::layer_t slid(CartoCSS::LayerAllocator::layer_t::first); slid != layeralloc.get_nextlayer();
			     slid = layeralloc.inc(slid))
				*debugout << ' ' << sublayer_name(layeralloc.get_name(slid));
			*debugout << std::endl;
		}
		CartoCSS::LayerGlobalParameters glbparams;
		{
			ApplyErrorVisitor vis(debugout);
			layercss->apply(vis, glbparams, layeralloc);
			if (debugout && vis.is_error())
				layercss->print(*debugout << "ERROR APPLYING GLOBAL PARAMETERS" << std::endl, 0);
		}
		typedef std::vector<CartoCSS::LayerParameters> renderobjparams_t;
		renderobjparams_t objparams;
		objparams.resize(renderobjs.size());
		typedef std::vector<bool> renderobjena_t;
		renderobjena_t objena;
		objena.resize(renderobjs.size());
		// CSS styling
		for (renderobjs_t::size_type i(0), n(renderobjs.size()); i < n; ++i) {
			CartoCSS::Statement::const_ptr_t objcss;
			{
				RenderObjVisitor vis(renderobjs[i]);
				objcss = layercss->simplify(vis);
			}
			if (!objcss)
				objcss = layercss;
			if (m_debugobject != debugobject_off && renderobjs[i]->is_id(m_debugobject) && debugout)
				renderobjs[i]->print(*debugout << "Layer " << lid << std::endl, 2);
			if (m_cssdebuglevel >= 4U || (m_debugobject != debugobject_off && renderobjs[i]->is_id(m_debugobject))) {
				std::ostringstream fn;
				fn << "css.layer." << lid << ".obj." << i;
				std::ofstream ofs(Glib::build_filename(tmpdir, fn.str()).c_str());
				renderobjs[i]->RenderObj::print(ofs << "OBJECT " << i << '/' << n << " CSS LAYER " << lid << std::endl, 0) << std::endl;
				objcss->print(ofs << "CSS LAYER " << lid << " OBJECT " << i << '/' << n << (objcss->is_empty() ? " EMPTY" : "") << std::endl, 0);
				if (!objcss->is_empty()) {
					CartoCSS::prefixallocators_t pallocs;
					CartoCSS::LayerParameters par;
					ApplyErrorVisitor vis(&ofs);
					objcss->apply(vis, par, layeralloc, pallocs);
					par.print(ofs << "PARAMETERS" << std::endl, layeralloc, pallocs, 2);
				}
			}
			if (objcss->is_empty())
				continue;
			objena[i] = true;
			CartoCSS::prefixallocators_t pallocs;
			ApplyErrorVisitor vis(debugout);
			objcss->apply(vis, objparams[i], layeralloc, pallocs);
			if (debugout && vis.is_error())
				objcss->print(*debugout << "ERROR APPLYING OBJECT PARAMETERS" << std::endl, 0);
		}
		for (CartoCSS::LayerAllocator::layer_t slid(CartoCSS::LayerAllocator::layer_t::first); slid < layeralloc.get_nextlayer();
		     slid = CartoCSS::LayerAllocator::inc(slid)) {
			const CartoCSS::GlobalParameters& glbpar(glbparams[slid]);
			cr->push_group();
			for (renderobjs_t::size_type i(0), n(renderobjs.size()); i < n; ++i) {
				if (!objena[i])
					continue;
				const RenderObj::const_ptr_t& ro(renderobjs[i]);
				if (!ro)
					continue;
				CartoCSS::LayerParameters::iterator parit(objparams[i].find(slid));
				if (parit == objparams[i].end())
					continue;
				ro->draw(debugout, *this, cr, parit->second);
				if (m_debugobject != debugobject_off && ro->is_id(m_debugobject) && debugout) {
					ro->print(*debugout << "Layer " << lid << " sublayer " << layeralloc.get_name(slid) << std::endl, 2);
					parit->second.print(*debugout, 2) << std::endl;
				}
			}
			cr->pop_group_to_source();
        		set_compop(cr, glbpar.get_compop());
			//FIXME: image filters
			cr->paint_with_alpha(glbpar.get_opacity());
			cr->set_operator(Cairo::OPERATOR_OVER);
		}
	}
	// ARP
	if (!m_arp.is_invalid() && m_bbox.is_inside(m_arp)) {
		double xc(transformx(m_arp)), yc(transformy(m_arp));
		cr->save();
		cr->translate(xc, yc);
		set_color_arpbgnd(cr);
		cr->set_line_width(0.5 * arp_cairo_radius);
		cr->arc(0.0, 0.0, arp_cairo_radius, 0.0, 2.0 * M_PI);
		cr->close_path();
		cr->move_to(-arp_cairo_radius, 0);
		cr->line_to(arp_cairo_radius, 0);
		cr->move_to(0, -arp_cairo_radius);
		cr->line_to(0, arp_cairo_radius);
		cr->stroke();
		set_color_arp(cr);
		cr->set_line_width(0.2 * arp_cairo_radius);
		cr->arc(0.0, 0.0, arp_cairo_radius, 0.0, 2.0 * M_PI);
		cr->close_path();
		cr->move_to(-arp_cairo_radius, 0);
		cr->line_to(arp_cairo_radius, 0);
		cr->move_to(0, -arp_cairo_radius);
		cr->line_to(0, arp_cairo_radius);
		cr->stroke();
		cr->restore();
	}
	// Obstacles
	coordobstacles_t ob;
	for (obstacles_t::const_iterator oi(m_obstacles.begin()), oe(m_obstacles.end()); oi != oe; ++oi) {
		if (oi->get_pt().is_invalid() || !m_bbox.is_inside(oi->get_pt()))
			continue;
		double xc(transformx(oi->get_pt())), yc(transformy(oi->get_pt()));
		ob.push_back(CoordObstacle(*oi, xc, yc));
		cr->save();
		cr->translate(xc, yc);
		set_color_obstaclebgnd(cr);
		cr->set_line_width(0.6 * obstacle_cairo_radius);
		cr->arc(0.0, 0.0, obstacle_cairo_radius, 0.0, 2.0 * M_PI);
		cr->close_path();
		cr->move_to(-obstacle_cairo_radius, 0);
		cr->line_to(obstacle_cairo_radius, 0);
		cr->move_to(0, -obstacle_cairo_radius);
		cr->line_to(0, obstacle_cairo_radius);
		if (oi->is_lighted()) {
			cr->move_to(0, -obstacle_cairo_radius_1);
			cr->line_to(0, -obstacle_cairo_radius_2);
			cr->move_to(obstacle_cairo_radius_x1, -obstacle_cairo_radius_y1);
			cr->line_to(obstacle_cairo_radius_x2, -obstacle_cairo_radius_y2);
			cr->move_to(-obstacle_cairo_radius_x1, -obstacle_cairo_radius_y1);
			cr->line_to(-obstacle_cairo_radius_x2, -obstacle_cairo_radius_y2);
		}
		cr->stroke();
		set_color_obstacle(cr);
		cr->set_line_width(0.2 * obstacle_cairo_radius);
		cr->arc(0.0, 0.0, obstacle_cairo_radius, 0.0, 2.0 * M_PI);
		cr->stroke();
		cr->move_to(-obstacle_cairo_radius, 0);
		cr->line_to(obstacle_cairo_radius, 0);
		cr->move_to(0, -obstacle_cairo_radius);
		cr->line_to(0, obstacle_cairo_radius);
		if (oi->is_lighted()) {
			cr->move_to(0, -obstacle_cairo_radius_1);
			cr->line_to(0, -obstacle_cairo_radius_2);
			cr->move_to(obstacle_cairo_radius_x1, -obstacle_cairo_radius_y1);
			cr->line_to(obstacle_cairo_radius_x2, -obstacle_cairo_radius_y2);
			cr->move_to(-obstacle_cairo_radius_x1, -obstacle_cairo_radius_y1);
			cr->line_to(-obstacle_cairo_radius_x2, -obstacle_cairo_radius_y2);
		}
		cr->stroke();
		{
			double val(std::numeric_limits<double>::quiet_NaN());
			if (!std::isnan(oi->get_agl()) && (!obstacle_preferamsl || std::isnan(oi->get_amsl()))) {
				val = oi->get_agl();
				cr->select_font_face("sans", Cairo::FONT_SLANT_NORMAL, Cairo::FONT_WEIGHT_NORMAL);
				cr->set_font_size(obstacle_cairo_fontsize);
			} else if (!std::isnan(oi->get_amsl())) {
				val = oi->get_amsl();
				cr->select_font_face("sans", Cairo::FONT_SLANT_ITALIC, Cairo::FONT_WEIGHT_NORMAL);
				cr->set_font_size(obstacle_cairo_fontsize);
			}
			if (!std::isnan(val)) {
				val *= Point::m_to_ft_dbl;
				std::ostringstream oss;
				oss << std::fixed << std::setprecision(0) << val;
				Cairo::TextExtents ext;
				cr->get_text_extents(oss.str(), ext);
				cr->move_to(1.2 * obstacle_cairo_radius - ext.x_bearing, -0.5 * ext.height - ext.y_bearing);
				set_color_obstaclebgnd(cr);
				cr->text_path(oss.str());
				cr->set_line_width(0.5 * obstacle_cairo_radius);
				cr->stroke_preserve();
				set_color_obstacle(cr);
				cr->fill();
			}
		}
		cr->restore();
	}
	// end
	cr->restore();
	m_fontlist.clear();
	return ob;
}

class CartoRender::SQLCalcError : public CartoSQL::CalcOSMVariables {
public:
	SQLCalcError(const CartoSQL *csql = nullptr) : CalcOSMVariables(csql), m_error(false) {}

	bool is_error(void) const { return m_error; }
	void reset(void) { m_error = false; m_os.str(""); }
	std::string get_string(void) const { return m_os.str(); }

	virtual void error(const CartoSQL::Expr& e, const char *msg) {
		if (is_error())
			return;
		m_error = true;
		m_os << "ERROR: " << (msg ? msg : "unknown") << std::endl;
		CartoSQL::PrintContextSQL ctx(get_csql());
		e.print(m_os, 2, ctx) << std::endl;
	}

	virtual void error(const CartoSQL::Expr& e, const CartoSQL::Value& v, const char *msg) {
		if (is_error())
			return;
		m_error = true;
		m_os << "ERROR: " << (msg ? msg : "unknown") << ": " << v.get_sqlstring() << std::endl;
		CartoSQL::PrintContextSQL ctx(get_csql());
		e.print(m_os, 2, ctx) << std::endl;
	}

	virtual void error(const CartoSQL::ExprTable& e, const char *msg) {
		if (is_error())
			return;
		m_error = true;
		m_os << "ERROR: " << (msg ? msg : "unknown") << std::endl;
		CartoSQL::PrintContextSQL ctx(get_csql());
		e.print(m_os, 2, ctx) << std::endl;
	}

	virtual void error(const CartoSQL::ExprTable& e, const CartoSQL::Value& v, const char *msg) {
		if (is_error())
			return;
		m_error = true;
		m_os << "ERROR: " << (msg ? msg : "unknown") << ": " << v.get_sqlstring() << std::endl;
		CartoSQL::PrintContextSQL ctx(get_csql());
		e.print(m_os, 2, ctx) << std::endl;
	}

protected:
	std::ostringstream m_os;
	bool m_error;
};

CartoRender::renderobjs_t CartoRender::dbquery_carto(const Carto::Layer& layer, const std::string& tmpdir)
{
	switch (layer.get_layertype()) {
	case Carto::Layer::layertype_t::osm:
		return dbquery_carto_sql(layer.get_sqlexpr(), layer.get_id(), tmpdir);

	case Carto::Layer::layertype_t::osmstatic:
		return dbquery_carto_static(layer.get_staticlayer(), tmpdir);

	default:
		return renderobjs_t();
	}
}

CartoCSS::Value CartoRender::convert(const CartoSQL::Value& v)
{
	CartoCSS::Value val;
	switch (v.get_type()) {
	case CartoSQL::Value::type_t::string:
		val = v.get_string();
		break;

	case CartoSQL::Value::type_t::floatingpoint:
		val = v.get_double();
		break;
			
	case CartoSQL::Value::type_t::integer:
		val = v.get_integer();
		break;
	
	case CartoSQL::Value::type_t::boolean:
		val = v.get_boolean();
		break;

	default:
		break;
	}
	return val;
}

CartoRender::renderobjs_t CartoRender::dbquery_carto_sql(const Carto::Layer::sqlptr_t& sql, const std::string& layerid, const std::string& tmpdir)
{
	static constexpr double standardized_pixel_size = 0.28e-3; /*0.28mm*/
	if (!sql || !m_carto)
		return renderobjs_t();
	SQLCalcError calc(&m_carto->get_sql());
	calc.set_bbox(m_bbox);
	// https://github.com/openstreetmap/mapnik-stylesheets/blob/master/zoom-to-scale.txt
	{
		double map_width_pixels(get_width() * (pixels_per_inch / points_per_inch));
		double map_height_pixels(get_height() * (pixels_per_inch / points_per_inch));
		double map_width_m, map_height_m;
		{
			Point pdiff(get_bbox().get_northeast() - get_bbox().get_southwest());
			Point p0(get_bbox().get_southwest() + Point(0, pdiff.get_lat() >> 1));
			Point p1(get_bbox().get_northeast() - Point(0, pdiff.get_lat() >> 1));
			Point p2(get_bbox().get_southwest() + Point(pdiff.get_lon() >> 1, 0));
			Point p3(get_bbox().get_northeast() - Point(pdiff.get_lon() >> 1, 0));
			map_width_m = p0.spheric_distance_km_dbl(p1) * 1e3;
			map_height_m = p2.spheric_distance_km_dbl(p3) * 1e3;
		}
		calc.set_scale_denominator(map_width_m / (map_width_pixels * standardized_pixel_size));
		calc.set_pixel_width(map_width_m / map_width_pixels);
		calc.set_pixel_height(map_height_m / map_height_pixels);
	}
	CartoSQL::ExprTable::const_ptr_t exprs(sql->replace_variable(calc));
	if (!exprs)
		exprs = sql;
	std::ofstream log;
	if (m_cssdebuglevel >= 3U) {
		std::string fn(Glib::build_filename(tmpdir, "sql.layer." + layerid));
		log.open(fn.c_str());
	}
	if (log.is_open()) {
		log << "SCALE_DENOMINATOR = " << calc.get_scale_denominator() << std::endl
		    << "PIXEL_WIDTH = " << calc.get_pixel_width() << 'm' << std::endl
		    << "PIXEL_HEIGHT = " << calc.get_pixel_height() << 'm' << std::endl << std::endl;
		m_carto->get_sql().print_expr(log << "SQL QUERY: ", exprs, 2) << std::endl << std::endl;
	}
	const CartoSQL::ExprTable::Columns& cols(exprs->get_columns());
	const unsigned int ncols(cols.size());
	const unsigned int waycol(cols.find(CartoSQL::ExprTable::ColumnName("", "way")));
	if (waycol >= ncols) {
		if (log.is_open())
			log << "ERROR: WAY COLUMN NOT FOUND" << std::endl;
		return renderobjs_t();
	}
	const std::string& classid(cols[waycol].get_table());
	const unsigned int zorder(cols.find(CartoSQL::ExprTable::ColumnName("", "z_order")));
	CartoSQL::Table tbl;
	Glib::TimeVal tv, tv1;
	tv1.assign_current_time();
	if (!exprs->calculate(calc, tbl)) {
		if (log.is_open())
			log << "CALCULATE ERROR: " << calc.get_string() << std::endl;
	} else if (log.is_open()) {
		tv.assign_current_time();
		tv -= tv1;
		log << "RESULT: (" << tv.as_double() << "s), " << tbl.size() << " rows" << std::endl;
		if (m_cssdebuglevel >= 4U)
			CartoSQL::print_table(log, tbl, exprs, 2, " | ", CartoSQL::Value::sqlstringflags_t::none);
	}
	renderobjs_t robjs;
	for (const CartoSQL::TableRow& tr : tbl) {
		if (tr.size() <= waycol)
			continue;
		RenderObj::ptr_t pobj;
		{
			const CartoSQL::Value& way(tr[waycol]);
			switch (way.get_type()) {
			case CartoSQL::Value::type_t::points:
				pobj = RenderObj::ptr_t(new RenderObjPoints(way.get_points()));
				break;

			case CartoSQL::Value::type_t::lines:
				pobj = RenderObj::ptr_t(new RenderObjLines(way.get_lines()));
				break;

			case CartoSQL::Value::type_t::polygons:
				pobj = RenderObj::ptr_t(new RenderObjPolygons(way.get_polygons()));
				break;

			case CartoSQL::Value::type_t::osmdbobject:
				switch (way.get_osmdbobject()->get_type()) {
				case OSMDB::Object::type_t::point:
				{
					MultiPoint mp;
					mp.push_back(way.get_point());
					pobj = RenderObj::ptr_t(new RenderObjPoints(mp));
					break;
				}

				case OSMDB::Object::type_t::line:
				case OSMDB::Object::type_t::road:
				{
					MultiLineString mls;
					mls.push_back(way.get_line());
					pobj = RenderObj::ptr_t(new RenderObjLines(mls));
					break;
				}

				case OSMDB::Object::type_t::area:
					pobj = RenderObj::ptr_t(new RenderObjPolygons(way.get_polygons()));
					break;

				default:
					continue;
				}
				break;

			default:
				continue;
			}
		}
		{
			RenderObj::ids_t id;
			id.insert(id.end(), tr.get_osmidset().begin(), tr.get_osmidset().end());
			pobj->set_ids(id);
		}
		pobj->set_classid(classid);
		RenderObj::fields_t fld;
		for (unsigned int colidx(0); colidx < ncols; ++colidx) {
			if (colidx >= tr.size())
				break;
			if (colidx == waycol)
				continue;
			const CartoSQL::Value& v(tr[colidx]);
			if (colidx == zorder) {
				if (v.is_integer())
					pobj->set_zorder(v.get_integer());
				continue;
			}
			const CartoSQL::ExprTable::Column& col(cols[colidx]);
			CartoCSS::Value val(convert(v));
			if (false && val.is_null())
				continue;
			fld[col.get_name()] = val;
		}
		pobj->set_fields(fld);
		robjs.push_back(pobj);
	}
	return robjs;
}

CartoRender::renderobjs_t CartoRender::dbquery_carto_static(OSMStaticDB::layer_t layer, const std::string& tmpdir)
{
	const OSMStaticDB *sdb(get_osmsdb());
	if (!sdb)
		return renderobjs_t();
	switch (layer) {
	case OSMStaticDB::layer_t::boundarylinesland:
	case OSMStaticDB::layer_t::icesheetoutlines:
	{
		OSMStaticDB::objects_t objs(sdb->find(m_bbox, layer));
		renderobjs_t robjs;
		for (const OSMStaticDB::Object::const_ptr_t& pobj : objs) {
			RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMStaticDB::ObjLine>(pobj)));
			if (!ro)
				continue;
			copytags(ro, pobj);
			robjs.push_back(ro);
		}
		return robjs;
	}

	case OSMStaticDB::layer_t::icesheetpolygons:
	case OSMStaticDB::layer_t::waterpolygons:
	case OSMStaticDB::layer_t::simplifiedwaterpolygons:
	{
		OSMStaticDB::objects_t objs(sdb->find(m_bbox, layer));
		renderobjs_t robjs;
		for (const OSMStaticDB::Object::const_ptr_t& pobj : objs) {
			RenderObjPolygons::ptr_t ro(create_polygons(boost::dynamic_pointer_cast<const OSMStaticDB::ObjArea>(pobj)));
			if (!ro)
				continue;
			copytags(ro, pobj);
			robjs.push_back(ro);
		}
		return robjs;
	}

	default:
		return renderobjs_t();
	}
}

CartoRender::coordobstacles_t CartoRender::drawsql(const Cairo::RefPtr<Cairo::Context>& cr,
						   const Cairo::RefPtr<Cairo::PdfSurface>& surface)
{
	static constexpr double obstacle_cairo_radius_1 = 1.2 * obstacle_cairo_radius;
	static constexpr double obstacle_cairo_radius_x1 = 1.2 * 0.50000 * obstacle_cairo_radius;
	static constexpr double obstacle_cairo_radius_y1 = 1.2 * 0.86603 * obstacle_cairo_radius;
	static constexpr double obstacle_cairo_radius_2 = 1.8 * obstacle_cairo_radius;
	static constexpr double obstacle_cairo_radius_x2 = 1.8 * 0.50000 * obstacle_cairo_radius;
	static constexpr double obstacle_cairo_radius_y2 = 1.8 * 0.86603 * obstacle_cairo_radius;
	if (m_bbox.is_invalid() || !m_carto)
		return coordobstacles_t();
	const CartoCSS *style(get_style());
	if (!style)
		return coordobstacles_t();
	m_collisions.clear();
	m_patterncache.clear();
	populate_fonts();
	std::ostream *debugout = &std::cerr;
	std::ofstream debugoutput;
	std::string tmpdir(Glib::get_tmp_dir());
	if (m_cssdebuglevel || m_debugobject != debugobject_off) {
		std::string tmpdir1(Glib::build_filename(tmpdir, "cartorender_XXXXXX"));
		gchar *td(g_strdup(tmpdir1.c_str()));
		gchar *td1(g_mkdtemp(td));
		if (!td1) {
			g_free(td);
			throw std::runtime_error("Cannot create the temporary directory " + tmpdir1);
		}
		tmpdir = td1;
		g_free(td);
		debugoutput.open(Glib::build_filename(tmpdir, "log"));
		debugout = &debugoutput;
	}
	// simplify CSS
	CartoCSS::Statement::const_ptr_t css(style->get_statements());
	if (!css)
		return coordobstacles_t();
	if (m_cssdebuglevel >= 1U) {
		std::ofstream ofs(Glib::build_filename(tmpdir, "css.initial"));
		css->print(ofs << "INITIAL CSS" << std::endl, 0);
	}
	{
		CartoCSS::Statement::const_ptr_t p(css->simplify("zoom", CartoCSS::Value(static_cast<int64_t>(get_zoom()))));
		if (p)
			css = p;
	}
	if (m_cssdebuglevel >= 2U) {
		std::ofstream ofs(Glib::build_filename(tmpdir, "css.zoom"));
		css->print(ofs << "CSS AFTER zoom=" << get_zoom() << std::endl, 0);
	}
	// prefill collisions
	// ARP
	if (!m_arp.is_invalid() && m_bbox.is_inside(m_arp)) {
		double xc(transformx(m_arp)), yc(transformy(m_arp));
		cr->begin_new_path();
		cr->rectangle(xc - arp_cairo_radius, yc - arp_cairo_radius, 2 * arp_cairo_radius, 2 * arp_cairo_radius);
		store_collision(cr);
		cr->begin_new_path();
	}
	// Obstacles
	for (obstacles_t::const_iterator oi(m_obstacles.begin()), oe(m_obstacles.end()); oi != oe; ++oi) {
		if (oi->get_pt().is_invalid() || !m_bbox.is_inside(oi->get_pt()))
			continue;
		double xc(transformx(oi->get_pt())), yc(transformy(oi->get_pt()));
		cr->begin_new_path();
		cr->rectangle(xc - obstacle_cairo_radius_2, yc - obstacle_cairo_radius_2, 2 * obstacle_cairo_radius_2, 2 * obstacle_cairo_radius_2);
		store_collision(cr);
		cr->begin_new_path();
		{
			double val(std::numeric_limits<double>::quiet_NaN());
			if (!std::isnan(oi->get_agl()) && (!obstacle_preferamsl || std::isnan(oi->get_amsl()))) {
				val = oi->get_agl();
				cr->select_font_face("sans", Cairo::FONT_SLANT_NORMAL, Cairo::FONT_WEIGHT_NORMAL);
				cr->set_font_size(obstacle_cairo_fontsize);
			} else if (!std::isnan(oi->get_amsl())) {
				val = oi->get_amsl();
				cr->select_font_face("sans", Cairo::FONT_SLANT_ITALIC, Cairo::FONT_WEIGHT_NORMAL);
				cr->set_font_size(obstacle_cairo_fontsize);
			}
			if (!std::isnan(val)) {
				val *= Point::m_to_ft_dbl;
				std::ostringstream oss;
				oss << std::fixed << std::setprecision(0) << val;
				Cairo::TextExtents ext;
				cr->get_text_extents(oss.str(), ext);
				cr->rectangle(1.2 * obstacle_cairo_radius - ext.x_bearing, -0.5 * ext.height - ext.y_bearing, ext.width, ext.height);
				store_collision(cr);
			}
		}
		cr->begin_new_path();
	}
	// clip region
	{
		double width(get_width()), height(get_height());
		if (surface)
			surface->set_size(width, height);
		cr->save();
		// clip
		cr->rectangle(0, 0, width, height);
		cr->clip();
	}
	// background
	cr->set_operator(Cairo::OPERATOR_SOURCE);
	if (set_color(cr, style->get_mapparameters().get_backgroundcol()))
		cr->paint();
	cr->set_operator(Cairo::OPERATOR_OVER);
	typedef std::map<Carto::Layer::samelayer_t,renderobjs_t> renderobjcache_t;
	renderobjcache_t renderobjcache;
	// layers
	for (Carto::Layer::samelayer_t layeridx(0), nlayers(m_carto->get_layers().size()); layeridx < nlayers; ++layeridx) {
		const Carto::Layer& layer(m_carto->get_layers()[layeridx]);
		if (get_zoom() < layer.get_minzoom() || get_zoom() > layer.get_maxzoom())
			continue;
		// CSS simplification and information gathering
		CartoCSS::Statement::const_ptr_t layercss(css->simplify(CartoCSS::LayerID(layer.get_id())));
		if (!layercss)
			layercss = css;
		if (m_cssdebuglevel >= 3U) {
			std::string fn(Glib::build_filename(tmpdir, "css.layer." + layer.get_id()));
			std::ofstream ofs(fn.c_str());
			layercss->print(ofs << "CSS FOR LAYER " << layer.get_id() << std::endl, 0);
		}
		if (layercss->is_empty()) {
			if (layer.get_layertype() == Carto::Layer::layertype_t::samelayer) {
				if (!layer.is_save())
					renderobjcache.erase(layer.get_samelayer());
			} else {
				if (layer.is_save())
					renderobjcache[layeridx] = dbquery_carto(layer, tmpdir);
			}
			continue;
		}
		// get layer objects
		renderobjs_t renderobjs;
		if (layer.get_layertype() == Carto::Layer::layertype_t::samelayer) {
			renderobjcache_t::iterator i(renderobjcache.find(layer.get_samelayer()));
			if (i == renderobjcache.end()) {
				*debugout << "Layer " << layer.get_id() << ": not found in cache" << std::endl;
				continue;
			}
			if (layer.is_save()) {
				renderobjs = i->second;
			} else {
				renderobjs.swap(i->second);
				renderobjcache.erase(i);
			}
		} else {
			renderobjs = dbquery_carto(layer, tmpdir);
			if (layer.is_save())
				renderobjcache[layeridx] = renderobjs;
		}
		if (!false && debugout) {
			std::size_t osmobj(0);
			for (const RenderObj::const_ptr_t& ro : renderobjs)
				if (ro)
					osmobj += ro->get_ids().size();
			*debugout << "Layer " << layer.get_id() << ": " << renderobjs.size() << " (" << osmobj << ") objects" << std::endl;
		}
		if (renderobjs.empty())
			continue;
		if (false && debugout) {
			*debugout << ' ';
			for (const RenderObj::const_ptr_t& ro : renderobjs) {
				if (ro)
					*debugout << ' ' << ro->get_ids_str();
				else
					*debugout << " /";
			}
			*debugout << std::endl;
		}
		// allocate sublayers
		CartoCSS::LayerAllocator layeralloc;
		{
			LayerAllocVisitor vis(layeralloc);
			layercss->visit(vis);
		}
		if (!false && debugout) {
			*debugout << "Layer " << layer.get_id() << ": sublayers";
			for (CartoCSS::LayerAllocator::layer_t slid(CartoCSS::LayerAllocator::layer_t::first); slid != layeralloc.get_nextlayer();
			     slid = layeralloc.inc(slid))
				*debugout << ' ' << sublayer_name(layeralloc.get_name(slid));
			*debugout << std::endl;
		}
		CartoCSS::LayerGlobalParameters glbparams;
		{
			ApplyErrorVisitor vis(debugout);
			layercss->apply(vis, glbparams, layeralloc);
			if (debugout && vis.is_error())
				layercss->print(*debugout << "ERROR APPLYING GLOBAL PARAMETERS" << std::endl, 0);
		}
		typedef std::vector<CartoCSS::LayerParameters> renderobjparams_t;
		renderobjparams_t objparams;
		objparams.resize(renderobjs.size());
		typedef std::vector<bool> renderobjena_t;
		renderobjena_t objena;
		objena.resize(renderobjs.size());
		// CSS styling
		for (renderobjs_t::size_type i(0), n(renderobjs.size()); i < n; ++i) {
			CartoCSS::Statement::const_ptr_t objcss;
			{
				RenderObjVisitor vis(renderobjs[i]);
				objcss = layercss->simplify(vis);
			}
			if (!objcss)
				objcss = layercss;
			if (m_debugobject != debugobject_off && renderobjs[i]->is_id(m_debugobject) && debugout)
				renderobjs[i]->print(*debugout << "Layer " << layer.get_id() << std::endl, 2);
			if (m_cssdebuglevel >= 4U || (m_debugobject != debugobject_off && renderobjs[i]->is_id(m_debugobject))) {
				std::ostringstream fn;
				fn << "css.layer." << layer.get_id() << ".obj." << i;
				std::ofstream ofs(Glib::build_filename(tmpdir, fn.str()).c_str());
				renderobjs[i]->RenderObj::print(ofs << "OBJECT " << i << '/' << n << " CSS LAYER " << layer.get_id() << std::endl, 0) << std::endl;
				objcss->print(ofs << "CSS LAYER " << layer.get_id() << " OBJECT " << i << '/' << n << (objcss->is_empty() ? " EMPTY" : "") << std::endl, 0);
				if (!objcss->is_empty()) {
					CartoCSS::prefixallocators_t pallocs;
					CartoCSS::LayerParameters par;
					ApplyErrorVisitor vis(&ofs);
					objcss->apply(vis, par, layeralloc, pallocs);
					par.print(ofs << "PARAMETERS" << std::endl, layeralloc, pallocs, 2);
				}
			}
			if (objcss->is_empty())
				continue;
			objena[i] = true;
			CartoCSS::prefixallocators_t pallocs;
			ApplyErrorVisitor vis(debugout);
			objcss->apply(vis, objparams[i], layeralloc, pallocs);
			if (debugout && vis.is_error())
				objcss->print(*debugout << "ERROR APPLYING OBJECT PARAMETERS" << std::endl, 0);
		}
		for (CartoCSS::LayerAllocator::layer_t slid(CartoCSS::LayerAllocator::layer_t::first); slid < layeralloc.get_nextlayer();
		     slid = CartoCSS::LayerAllocator::inc(slid)) {
			const CartoCSS::GlobalParameters& glbpar(glbparams[slid]);
			cr->push_group();
			for (renderobjs_t::size_type i(0), n(renderobjs.size()); i < n; ++i) {
				if (!objena[i])
					continue;
				const RenderObj::const_ptr_t& ro(renderobjs[i]);
				if (!ro)
					continue;
				CartoCSS::LayerParameters::iterator parit(objparams[i].find(slid));
				if (parit == objparams[i].end())
					continue;
				ro->draw(debugout, *this, cr, parit->second);
				if (m_debugobject != debugobject_off && ro->is_id(m_debugobject) && debugout) {
					ro->print(*debugout << "Layer " << layer.get_id() << " sublayer " << layeralloc.get_name(slid) << std::endl, 2);
					parit->second.print(*debugout, 2) << std::endl;
				}
			}
			cr->pop_group_to_source();
        		set_compop(cr, glbpar.get_compop());
			//FIXME: image filters
			cr->paint_with_alpha(glbpar.get_opacity());
			cr->set_operator(Cairo::OPERATOR_OVER);
		}
	}
	// ARP
	if (!m_arp.is_invalid() && m_bbox.is_inside(m_arp)) {
		double xc(transformx(m_arp)), yc(transformy(m_arp));
		cr->save();
		cr->translate(xc, yc);
		set_color_arpbgnd(cr);
		cr->set_line_width(0.5 * arp_cairo_radius);
		cr->arc(0.0, 0.0, arp_cairo_radius, 0.0, 2.0 * M_PI);
		cr->close_path();
		cr->move_to(-arp_cairo_radius, 0);
		cr->line_to(arp_cairo_radius, 0);
		cr->move_to(0, -arp_cairo_radius);
		cr->line_to(0, arp_cairo_radius);
		cr->stroke();
		set_color_arp(cr);
		cr->set_line_width(0.2 * arp_cairo_radius);
		cr->arc(0.0, 0.0, arp_cairo_radius, 0.0, 2.0 * M_PI);
		cr->close_path();
		cr->move_to(-arp_cairo_radius, 0);
		cr->line_to(arp_cairo_radius, 0);
		cr->move_to(0, -arp_cairo_radius);
		cr->line_to(0, arp_cairo_radius);
		cr->stroke();
		cr->restore();
	}
	// Obstacles
	coordobstacles_t ob;
	for (obstacles_t::const_iterator oi(m_obstacles.begin()), oe(m_obstacles.end()); oi != oe; ++oi) {
		if (oi->get_pt().is_invalid() || !m_bbox.is_inside(oi->get_pt()))
			continue;
		double xc(transformx(oi->get_pt())), yc(transformy(oi->get_pt()));
		ob.push_back(CoordObstacle(*oi, xc, yc));
		cr->save();
		cr->translate(xc, yc);
		set_color_obstaclebgnd(cr);
		cr->set_line_width(0.6 * obstacle_cairo_radius);
		cr->arc(0.0, 0.0, obstacle_cairo_radius, 0.0, 2.0 * M_PI);
		cr->close_path();
		cr->move_to(-obstacle_cairo_radius, 0);
		cr->line_to(obstacle_cairo_radius, 0);
		cr->move_to(0, -obstacle_cairo_radius);
		cr->line_to(0, obstacle_cairo_radius);
		if (oi->is_lighted()) {
			cr->move_to(0, -obstacle_cairo_radius_1);
			cr->line_to(0, -obstacle_cairo_radius_2);
			cr->move_to(obstacle_cairo_radius_x1, -obstacle_cairo_radius_y1);
			cr->line_to(obstacle_cairo_radius_x2, -obstacle_cairo_radius_y2);
			cr->move_to(-obstacle_cairo_radius_x1, -obstacle_cairo_radius_y1);
			cr->line_to(-obstacle_cairo_radius_x2, -obstacle_cairo_radius_y2);
		}
		cr->stroke();
		set_color_obstacle(cr);
		cr->set_line_width(0.2 * obstacle_cairo_radius);
		cr->arc(0.0, 0.0, obstacle_cairo_radius, 0.0, 2.0 * M_PI);
		cr->stroke();
		cr->move_to(-obstacle_cairo_radius, 0);
		cr->line_to(obstacle_cairo_radius, 0);
		cr->move_to(0, -obstacle_cairo_radius);
		cr->line_to(0, obstacle_cairo_radius);
		if (oi->is_lighted()) {
			cr->move_to(0, -obstacle_cairo_radius_1);
			cr->line_to(0, -obstacle_cairo_radius_2);
			cr->move_to(obstacle_cairo_radius_x1, -obstacle_cairo_radius_y1);
			cr->line_to(obstacle_cairo_radius_x2, -obstacle_cairo_radius_y2);
			cr->move_to(-obstacle_cairo_radius_x1, -obstacle_cairo_radius_y1);
			cr->line_to(-obstacle_cairo_radius_x2, -obstacle_cairo_radius_y2);
		}
		cr->stroke();
		{
			double val(std::numeric_limits<double>::quiet_NaN());
			if (!std::isnan(oi->get_agl()) && (!obstacle_preferamsl || std::isnan(oi->get_amsl()))) {
				val = oi->get_agl();
				cr->select_font_face("sans", Cairo::FONT_SLANT_NORMAL, Cairo::FONT_WEIGHT_NORMAL);
				cr->set_font_size(obstacle_cairo_fontsize);
			} else if (!std::isnan(oi->get_amsl())) {
				val = oi->get_amsl();
				cr->select_font_face("sans", Cairo::FONT_SLANT_ITALIC, Cairo::FONT_WEIGHT_NORMAL);
				cr->set_font_size(obstacle_cairo_fontsize);
			}
			if (!std::isnan(val)) {
				val *= Point::m_to_ft_dbl;
				std::ostringstream oss;
				oss << std::fixed << std::setprecision(0) << val;
				Cairo::TextExtents ext;
				cr->get_text_extents(oss.str(), ext);
				cr->move_to(1.2 * obstacle_cairo_radius - ext.x_bearing, -0.5 * ext.height - ext.y_bearing);
				set_color_obstaclebgnd(cr);
				cr->text_path(oss.str());
				cr->set_line_width(0.5 * obstacle_cairo_radius);
				cr->stroke_preserve();
				set_color_obstacle(cr);
				cr->fill();
			}
		}
		cr->restore();
	}
	// end
	cr->restore();
	m_fontlist.clear();
	return ob;
}

bool CartoRender::set_color(const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::Color& col, double opacity)
{
	if (!col.is_valid() || !cr)
		return false;
	cr->set_source_rgba(col.get_red(), col.get_green(), col.get_blue(), col.get_alpha() * opacity);
	return true;
}

void CartoRender::set_compop(const Cairo::RefPtr<Cairo::Context>& cr, CartoCSS::compositeop_t compop)
{
	if (!cr)
		return;
	switch (compop) {
	case CartoCSS::compositeop_t::clear:
		cr->set_operator(Cairo::OPERATOR_CLEAR);
		break;

	case CartoCSS::compositeop_t::src:
		cr->set_operator(Cairo::OPERATOR_SOURCE);
		break;

	case CartoCSS::compositeop_t::dst:
		cr->set_operator(Cairo::OPERATOR_DEST);
		break;

	case CartoCSS::compositeop_t::srcover:
		cr->set_operator(Cairo::OPERATOR_OVER);
		break;

	case CartoCSS::compositeop_t::dstover:
		cr->set_operator(Cairo::OPERATOR_DEST_OVER);
		break;
 	
	case CartoCSS::compositeop_t::srcin:
		cr->set_operator(Cairo::OPERATOR_IN);
		break;

	case CartoCSS::compositeop_t::dstin:
		cr->set_operator(Cairo::OPERATOR_DEST_IN);
		break;

	case CartoCSS::compositeop_t::srcout:
		cr->set_operator(Cairo::OPERATOR_OUT);
		break;
 	
	case CartoCSS::compositeop_t::dstout:
		cr->set_operator(Cairo::OPERATOR_DEST_OUT);
		break;

	case CartoCSS::compositeop_t::srcatop:
		cr->set_operator(Cairo::OPERATOR_ATOP);
		break;

	case CartoCSS::compositeop_t::dstatop:
		cr->set_operator(Cairo::OPERATOR_DEST_ATOP);
		break;

	case CartoCSS::compositeop_t::xor_:
		cr->set_operator(Cairo::OPERATOR_XOR);
		break;

	case CartoCSS::compositeop_t::plus:
		cr->set_operator(Cairo::OPERATOR_ADD);
		break;

	case CartoCSS::compositeop_t::saturation:
		cr->set_operator(Cairo::OPERATOR_SATURATE);
		break;

	case CartoCSS::compositeop_t::minus:
		break;

	case CartoCSS::compositeop_t::multiply:
   		cr->set_operator(static_cast<Cairo::Operator>(CAIRO_OPERATOR_MULTIPLY));
		break;

	case CartoCSS::compositeop_t::screen:
    		cr->set_operator(static_cast<Cairo::Operator>(CAIRO_OPERATOR_SCREEN));
		break;

	case CartoCSS::compositeop_t::overlay:
    		cr->set_operator(static_cast<Cairo::Operator>(CAIRO_OPERATOR_OVERLAY));
		break;

	case CartoCSS::compositeop_t::darken:
    		cr->set_operator(static_cast<Cairo::Operator>(CAIRO_OPERATOR_DARKEN));
		break;

	case CartoCSS::compositeop_t::lighten:
    		cr->set_operator(static_cast<Cairo::Operator>(CAIRO_OPERATOR_LIGHTEN));
		break;

	case CartoCSS::compositeop_t::colordodge:
    		cr->set_operator(static_cast<Cairo::Operator>(CAIRO_OPERATOR_COLOR_DODGE));
		break;

	case CartoCSS::compositeop_t::colorburn:
    		cr->set_operator(static_cast<Cairo::Operator>(CAIRO_OPERATOR_COLOR_BURN));
		break;

	case CartoCSS::compositeop_t::hardlight:
    		cr->set_operator(static_cast<Cairo::Operator>(CAIRO_OPERATOR_HARD_LIGHT));
		break;

	case CartoCSS::compositeop_t::softlight:
  		cr->set_operator(static_cast<Cairo::Operator>(CAIRO_OPERATOR_SOFT_LIGHT));
		break;

	case CartoCSS::compositeop_t::difference:
 		cr->set_operator(static_cast<Cairo::Operator>(CAIRO_OPERATOR_DIFFERENCE));
		break;

	case CartoCSS::compositeop_t::exclusion:
    		cr->set_operator(static_cast<Cairo::Operator>(CAIRO_OPERATOR_EXCLUSION));
		break;

	case CartoCSS::compositeop_t::contrast:
	case CartoCSS::compositeop_t::invert:
	case CartoCSS::compositeop_t::invertrgb:
	case CartoCSS::compositeop_t::grainmerge:
	case CartoCSS::compositeop_t::grainextract:
		break;

	case CartoCSS::compositeop_t::hue:
    		cr->set_operator(static_cast<Cairo::Operator>(CAIRO_OPERATOR_HSL_HUE));
		break;

	case CartoCSS::compositeop_t::color:
    		cr->set_operator(static_cast<Cairo::Operator>(CAIRO_OPERATOR_HSL_COLOR));
		break;

	case CartoCSS::compositeop_t::value:
     		cr->set_operator(static_cast<Cairo::Operator>(CAIRO_OPERATOR_HSL_LUMINOSITY));
		break;

	default:
		// not supported
    		//cr->set_operator(static_cast<Cairo::Operator>(CAIRO_OPERATOR_HSL_SATURATION));
 		break;
	}
}

void CartoRender::set_path_helper(const Cairo::RefPtr<Cairo::Context>& cr, const LineString& ls) const
{
	if (!cr || ls.size() < 2 || !ls.get_bbox().is_intersect(m_bbox))
		return;
	cr->begin_new_sub_path();
	for (LineString::const_iterator pb(ls.begin()), pe(ls.end()), pi(pb); pi != pe; ++pi) {
		double x(transformx(*pi));
		double y(transformy(*pi));
		if (pi == pb)
			cr->move_to(x, y);
		else
			cr->line_to(x, y);
	}
	if (ls.front() == ls.back())
		cr->close_path();
}

void CartoRender::set_path_helper(const Cairo::RefPtr<Cairo::Context>& cr, const MultiLineString& mls) const
{
	for (const auto& ls : mls)
		set_path_helper(cr, ls);
}

void CartoRender::set_path_helper(const Cairo::RefPtr<Cairo::Context>& cr, const PolygonSimple& ps) const
{
	if (!cr || ps.size() < 2 || !ps.get_bbox().is_intersect(m_bbox))
		return;
	cr->begin_new_sub_path();
	for (PolygonSimple::const_iterator pb(ps.begin()), pe(ps.end()), pi(pb); pi != pe; ++pi) {
		double x(transformx(*pi));
		double y(transformy(*pi));
		if (pi == pb)
			cr->move_to(x, y);
		else
			cr->line_to(x, y);
	}
	cr->close_path();
}

void CartoRender::set_path_helper(const Cairo::RefPtr<Cairo::Context>& cr, const PolygonHole& ph) const
{
	set_path_helper(cr, ph.get_exterior());
	for (unsigned int i(0), n(ph.get_nrinterior()); i < n; ++i)
		set_path_helper(cr, ph[i]);
}

void CartoRender::set_path_helper(const Cairo::RefPtr<Cairo::Context>& cr, const MultiPolygonHole& mph) const
{
	for (const auto& ph : mph)
		set_path_helper(cr, ph);
}

bool CartoRender::is_outofbox(const Point& p0, const Point& p1, const Rect& bbox)
{
	Point x0(p0 - bbox.get_southwest());
	Point x1(p1 - bbox.get_southwest());
	if (x0.get_lon() < 0 && x1.get_lon() < 0)
		return true;
	if (x0.get_lat() < 0 && x1.get_lat() < 0)
		return true;
	Point sz(bbox.get_northeast() - bbox.get_southwest());
	if (x0.get_lon() > sz.get_lon() && x1.get_lon() > sz.get_lon())
		return true;
	if (x0.get_lat() > sz.get_lat() && x1.get_lat() > sz.get_lat())
		return true;
	return false;
}

MultiLineString CartoRender::remove_outofbox(const LineString& ls, const Rect& bbox)
{
	MultiLineString mls;
	for (LineString::size_type i(1), n(ls.size()); i < n; ) {
		if (is_outofbox(ls[i-1], ls[i], bbox)) {
			++i;
			continue;
		}
		LineString::size_type j(i + 1);
		while (j < n && !is_outofbox(ls[j-1], ls[j], bbox))
			++j;
		mls.push_back(LineString());
		mls.back().insert(mls.back().end(), ls.begin() + (i - 1), ls.begin() + j);
		i = j;
	}
	return mls;
}

MultiLineString CartoRender::remove_outofbox(const MultiLineString& mls, const Rect& bbox)
{
	MultiLineString r;
	for (const LineString& ls : mls) {
		MultiLineString r1(remove_outofbox(ls, bbox));
		r.insert(r.end(), r1.begin(), r1.end());
	}
	return r;
}

void CartoRender::set_path(const Cairo::RefPtr<Cairo::Context>& cr, const LineString& ls) const
{
	Rect bbox(get_extended_bbox());
	if (ls.is_all_points_inside(bbox)) {
		set_path_helper(cr, ls);
		return;
	}
	set_path_helper(cr, remove_outofbox(ls, bbox));
}

void CartoRender::set_path(const Cairo::RefPtr<Cairo::Context>& cr, const MultiLineString& mls) const
{
	Rect bbox(get_extended_bbox());
	if (mls.is_all_points_inside(bbox)) {
		set_path_helper(cr, mls);
		return;
	}
	set_path_helper(cr, remove_outofbox(mls, bbox));
}

void CartoRender::set_path(const Cairo::RefPtr<Cairo::Context>& cr, const PolygonSimple& ps) const
{
	Rect bbox(get_extended_bbox());
	if (ps.is_all_points_inside(bbox)) {
		set_path_helper(cr, ps);
		return;
	}
	MultiPolygonHole mph;
	mph.push_back(PolygonHole(ps));
	{
		MultiPolygonHole mph1;
		{
			PolygonSimple ps;
			ps.push_back(bbox.get_northwest());
			ps.push_back(bbox.get_southwest());
			ps.push_back(bbox.get_southeast());
			ps.push_back(bbox.get_northeast());
			mph1.push_back(PolygonHole(ps));
		}
		mph.geos_intersect(mph1);
	}
	set_path_helper(cr, mph);
}

void CartoRender::set_path(const Cairo::RefPtr<Cairo::Context>& cr, const PolygonHole& ph) const
{
	Rect bbox(get_extended_bbox());
	if (ph.is_all_points_inside(bbox)) {
		set_path_helper(cr, ph);
		return;
	}
	MultiPolygonHole mph;
	mph.push_back(ph);
	{
		MultiPolygonHole mph1;
		{
			PolygonSimple ps;
			ps.push_back(bbox.get_northwest());
			ps.push_back(bbox.get_southwest());
			ps.push_back(bbox.get_southeast());
			ps.push_back(bbox.get_northeast());
			mph1.push_back(PolygonHole(ps));
		}
		mph.geos_intersect(mph1);
	}
	set_path_helper(cr, mph);
}

void CartoRender::set_path(const Cairo::RefPtr<Cairo::Context>& cr, const MultiPolygonHole& mph) const
{
	Rect bbox(get_extended_bbox());
	if (mph.is_all_points_inside(bbox)) {
		set_path_helper(cr, mph);
		return;
	}
	MultiPolygonHole mph0(mph);
	{
		MultiPolygonHole mph1;
		{
			PolygonSimple ps;
			ps.push_back(bbox.get_northwest());
			ps.push_back(bbox.get_southwest());
			ps.push_back(bbox.get_southeast());
			ps.push_back(bbox.get_northeast());
			mph1.push_back(PolygonHole(ps));
		}
		mph0.geos_intersect(mph1);
	}
	set_path_helper(cr, mph0);
}

void CartoRender::set_path(const Cairo::RefPtr<Cairo::Context>& cr, const PolygonSimple& ps, double offset) const
{
	if (std::isnan(offset) || offset == 0) {
		set_path(cr, ps);
		return;
	}
	MultiPolygonHole mph0;
	mph0.push_back(PolygonHole(ps));
	Rect bbox(get_extended_bbox());
	if (!mph0.is_all_points_inside(bbox)) {
		MultiPolygonHole mph1;
		{
			PolygonSimple ps;
			ps.push_back(bbox.get_northwest());
			ps.push_back(bbox.get_southwest());
			ps.push_back(bbox.get_southeast());
			ps.push_back(bbox.get_northeast());
			mph1.push_back(PolygonHole(ps));
		}
		mph0.geos_intersect(mph1);
	}
	{
		double ppm(pixelspermilex() + pixelspermiley());
		ppm *= 0.5;
		MultiPolygonHole mph1(mph0.make_fat_lines(std::fabs(offset) / ppm, true, 15, false));
		if (offset > 0)
			mph0.geos_union(mph1);
		else
			mph0.geos_subtract(mph1);
	}
	set_path_helper(cr, mph0);	
}

void CartoRender::set_path(const Cairo::RefPtr<Cairo::Context>& cr, const PolygonHole& ph, double offset) const
{
	if (std::isnan(offset) || offset == 0) {
		set_path(cr, ph);
		return;
	}
	MultiPolygonHole mph0;
	mph0.push_back(ph);
	Rect bbox(get_extended_bbox());
	if (!mph0.is_all_points_inside(bbox)) {
		MultiPolygonHole mph1;
		{
			PolygonSimple ps;
			ps.push_back(bbox.get_northwest());
			ps.push_back(bbox.get_southwest());
			ps.push_back(bbox.get_southeast());
			ps.push_back(bbox.get_northeast());
			mph1.push_back(PolygonHole(ps));
		}
		mph0.geos_intersect(mph1);
	}
	{
		double ppm(pixelspermilex() + pixelspermiley());
		ppm *= 0.5;
		MultiPolygonHole mph1(mph0.make_fat_lines(std::fabs(offset) / ppm, true, 15, false));
		if (offset > 0)
			mph0.geos_union(mph1);
		else
			mph0.geos_subtract(mph1);
	}
	set_path_helper(cr, mph0);
}

void CartoRender::set_path(const Cairo::RefPtr<Cairo::Context>& cr, const MultiPolygonHole& mph, double offset) const
{
	if (std::isnan(offset) || offset == 0) {
		set_path(cr, mph);
		return;
	}
	MultiPolygonHole mph0(mph);
	Rect bbox(get_extended_bbox());
	if (!mph0.is_all_points_inside(bbox)) {
		MultiPolygonHole mph1;
		{
			PolygonSimple ps;
			ps.push_back(bbox.get_northwest());
			ps.push_back(bbox.get_southwest());
			ps.push_back(bbox.get_southeast());
			ps.push_back(bbox.get_northeast());
			mph1.push_back(PolygonHole(ps));
		}
		mph0.geos_intersect(mph1);
	}
	{
		double ppm(pixelspermilex() + pixelspermiley());
		ppm *= 0.5;
		MultiPolygonHole mph1(mph0.make_fat_lines(std::fabs(offset) / ppm, true, 15, false));
		if (offset > 0)
			mph0.geos_union(mph1);
		else
			mph0.geos_subtract(mph1);
	}
	set_path_helper(cr, mph0);
}

CartoRender::RenderObjPoints::ptr_t CartoRender::create_points(const Point& pt, id_t id, int32_t zorder) const
{
	if (pt.is_invalid() || !m_bbox.is_inside(pt))
		return RenderObjPoints::ptr_t();
	MultiPoint mp;
	mp.push_back(pt);
	RenderObjPoints::ptr_t ro(new RenderObjPoints(mp, RenderObj::fields_t(), id, zorder));
	return ro;
}

CartoRender::RenderObjPoints::ptr_t CartoRender::create_points(const MultiPolygonHole& mph, id_t id, int32_t zorder) const
{
	double pixelarea(0);
	MultiPoint mp;
	{
		int64_t barea(-1);
		bool binside(false);
		for (const auto& ph : mph) {
			if (!ph.get_exterior().is_overlap(m_bbox))
				continue;
			int64_t area(std::abs(ph.area2()));
			pixelarea += area;
			Point pt(ph.get_exterior().centroid());
			bool inside(!!ph.windingnumber(pt));
			if (!mp.empty() && area <= barea && !(inside && !binside))
				continue;
			mp.clear();
			mp.push_back(pt);
			barea = area;
			binside = inside;
		}
	}
	if (mp.empty())
		return RenderObjPoints::ptr_t();
	pixelarea *= m_area2pixelscale;
	RenderObjPoints::ptr_t ro(new RenderObjPoints(mp, RenderObj::fields_t(), id, zorder));
	ro->set_field("way_pixels", pixelarea);
	return ro;
}

CartoRender::RenderObjLines::ptr_t CartoRender::create_lines(const LineString& ls, id_t id, int32_t zorder) const
{
	if (!ls.is_overlap(m_bbox))
		return RenderObjLines::ptr_t();
	MultiLineString mls;
	mls.push_back(ls);
	RenderObjLines::ptr_t ro(new RenderObjLines(mls, RenderObj::fields_t(), id, zorder));
	return ro;
}

CartoRender::RenderObjLines::ptr_t CartoRender::create_lines(const MultiLineString& ls, id_t id, int32_t zorder) const
{
	if (!ls.is_overlap(m_bbox))
		return RenderObjLines::ptr_t();
	RenderObjLines::ptr_t ro(new RenderObjLines(ls, RenderObj::fields_t(), id, zorder));
	return ro;
}

CartoRender::RenderObjLines::ptr_t CartoRender::create_lines(const MultiPolygonHole& mph, id_t id, int32_t zorder) const
{
	MultiLineString mls;
	double pixelarea(0);
	for (const auto& ph : mph) {
		LineString ls(ph.get_exterior());
		if (ls.size() < 2)
			continue;
		ls.push_back(ls.front());
		if (!ls.is_overlap(m_bbox))
			continue;
		mls.push_back(ls);
		pixelarea += std::abs(ph.get_exterior().area2());
	}
	if (mls.empty())
		return RenderObjLines::ptr_t();
	pixelarea *= m_area2pixelscale;
	RenderObjLines::ptr_t ro(new RenderObjLines(mls, RenderObj::fields_t(), id, zorder));
	ro->set_field("way_pixels", pixelarea);
	return ro;
}

CartoRender::RenderObjPolygons::ptr_t CartoRender::create_polygons(const MultiPolygonHole& mph, id_t id, int32_t zorder) const
{
	if (mph.empty())
		return RenderObjPolygons::ptr_t();
	RenderObjPolygons::ptr_t ro(new RenderObjPolygons(mph, RenderObj::fields_t(), id, zorder));
	MultiPolygonHole& mph1(ro->get_polygons());
	double pixelarea(0);
	for (MultiPolygonHole::iterator i(mph1.begin()), e(mph1.end()); i != e; ) {
		if (i->get_exterior().is_overlap(m_bbox)) {
			pixelarea += std::abs(i->area2());
			++i;
			continue;
		}
		i = mph1.erase(i);
		e = mph1.end();
	}
	pixelarea *= m_area2pixelscale;
	if (pixelarea < 1)
		return RenderObjPolygons::ptr_t();
	ro->set_field("way_pixels", pixelarea);
	return ro;
}

CartoRender::RenderObjPoints::ptr_t CartoRender::create_points(const OSMDB::ObjPoint::const_ptr_t& pp) const
{
	if (!pp)
		return RenderObjPoints::ptr_t();
	return create_points(pp->get_location(), pp->get_id(), pp->get_zorder());
}

CartoRender::RenderObjPoints::ptr_t CartoRender::create_points(const OSMDB::ObjArea::const_ptr_t& pa) const
{
	if (!pa)
		return RenderObjPoints::ptr_t();
	return create_points(pa->get_area(), pa->get_id(), pa->get_zorder());
}

CartoRender::RenderObjLines::ptr_t CartoRender::create_lines(const OSMDB::ObjLine::const_ptr_t& pl) const
{
	if (!pl)
		return RenderObjLines::ptr_t();
	return create_lines(pl->get_line(), pl->get_id(), pl->get_zorder());
}

CartoRender::RenderObjLines::ptr_t CartoRender::create_lines(const OSMDB::ObjArea::const_ptr_t& pa) const
{
	if (!pa)
		return RenderObjLines::ptr_t();
	return create_lines(pa->get_area(), pa->get_id(), pa->get_zorder());
}

CartoRender::RenderObjPolygons::ptr_t CartoRender::create_polygons(const OSMDB::ObjArea::const_ptr_t& pa) const
{
	if (!pa)
		return RenderObjPolygons::ptr_t();
	return create_polygons(pa->get_area(), pa->get_id(), pa->get_zorder());
}

CartoRender::RenderObjLines::ptr_t CartoRender::create_lines(const OSMStaticDB::ObjLine::const_ptr_t& pl) const
{
	if (!pl)
		return RenderObjLines::ptr_t();
	return create_lines(pl->get_line(), pl->get_id(), 0);
}

CartoRender::RenderObjPolygons::ptr_t CartoRender::create_polygons(const OSMStaticDB::ObjArea::const_ptr_t& pa) const
{
	if (!pa)
		return RenderObjPolygons::ptr_t();
	return create_polygons(pa->get_area(), pa->get_id(), 0);
}

void CartoRender::copytags(const RenderObj::ptr_t& r, const OSMStaticDB::Object::const_ptr_t& p) const
{
	const OSMStaticDB *sdb(get_osmsdb());
	if (!r || !p || !sdb)
		return;
	for (const auto& t : p->get_tags()) {
		const char *k(sdb->find_tagkey(std::get<OSMStaticDB::Object::tagkey_t>(t)));
		const char *v(sdb->find_tagname(std::get<OSMStaticDB::Object::tagname_t>(t)));
		if (!k)
			continue;
		if (!v) {
			r->set_field(k);
			continue;
		}
		r->set_field(k, v);
	}
}

MultiPolygonHole CartoRender::convert_path(const Cairo::RefPtr<Cairo::Context>& cr, bool applyctm)
{
	if (!cr)
		return MultiPolygonHole();
	cairo_path_t *cpath(cairo_copy_path_flat(cr->cobj()));
	if (!cpath)
		return MultiPolygonHole();
	MultiPolygonHole mph;
	PolygonSimple ps;
	static constexpr bool debug = false;
	for (int i = 0; i < cpath->num_data; i += cpath->data[i].header.length) {
		cairo_path_data_t *data(&cpath->data[i]);
		switch (data->header.type) {
		case CAIRO_PATH_MOVE_TO:
		{
			double x1(data[1].point.x), y1(data[1].point.y);
			if (applyctm)
				cr->user_to_device(x1, y1);
			ps.clear();
			ps.push_back(Point(Point::round<int32_t,double>(ldexp(x1, 8)), Point::round<int32_t,double>(ldexp(y1, 8))));
			if (debug)
				std::cerr << "detect_collision: move_to (" << x1 << ',' << y1 << ')' << std::endl;
			break;
		}

		case CAIRO_PATH_LINE_TO:
		{
			double x1(data[1].point.x), y1(data[1].point.y);
			if (applyctm)
				cr->user_to_device(x1, y1);
			ps.push_back(Point(Point::round<int32_t,double>(ldexp(x1, 8)), Point::round<int32_t,double>(ldexp(y1, 8))));
			if (debug)
				std::cerr << "detect_collision: line_to (" << x1 << ',' << y1 << ')' << std::endl;
			break;
		}

		case CAIRO_PATH_CURVE_TO:
		{
			double x1(data[1].point.x), y1(data[1].point.y);
			double x2(data[2].point.x), y2(data[2].point.y);
			double x3(data[3].point.x), y3(data[3].point.y);
			if (applyctm) {
				cr->user_to_device(x1, y1);
				cr->user_to_device(x2, y2);
				cr->user_to_device(x3, y3);
			}
			if (debug)
				std::cerr << "detect_collision: curve_to (" << x1 << ',' << y1
					  << " ) (" << x2 << ',' << y2
					  << " ) (" << x3 << ',' << y3 << ')' << std::endl;
			break;
		}

		case CAIRO_PATH_CLOSE_PATH:
			if (!ps.empty())
				mph.push_back(PolygonHole(ps));
			ps.clear();
			if (debug)
				std::cerr << "detect_collision: close_path" << std::endl;
			break;
		}
	}
	cairo_path_destroy(cpath);
	mph.geos_union(MultiPolygonHole());
	if (debug)
		std::cerr << "detect_collision: end" << std::endl;
	return mph;
}

bool CartoRender::detect_collision(const Cairo::RefPtr<Cairo::Context>& cr) const
{
	MultiPolygonHole mph(convert_path(cr, true));
	if (false)
		mph.print(std::cerr << "collision polygon" << std::endl) << std::endl;
	mph.geos_intersect(m_collisions);
	return !mph.empty();
}

void CartoRender::store_collision(const Cairo::RefPtr<Cairo::Context>& cr)
{
	MultiPolygonHole mph(convert_path(cr, true));
	if (false)
		mph.print(std::cerr << "collision polygon" << std::endl) << std::endl;
	m_collisions.geos_union(mph);
}

void CartoRender::populate_fonts(void)
{
	static constexpr bool log = false;
	static constexpr bool log_filename = false;
	m_fontlist.clear();
	PangoFontMap *pfm(pango_cairo_font_map_get_default());
	PangoFontFamily **ffam;
	int nfam;
	pango_font_map_list_families(pfm, &ffam, &nfam);
	for (int i = 0; i < nfam; ++i) {
		const char *famname(pango_font_family_get_name(ffam[i]));
		if (log)
			std::cerr << "Font Family: " << famname << std::endl;
		PangoFontFace **fface;
		int nface;
		pango_font_family_list_faces(ffam[i], &fface, &nface);
		for (int j = 0; j < nface; ++j) {
			const char *facename(pango_font_face_get_face_name(fface[j]));
			std::string fontname(famname);
			fontname.push_back(' ');
			fontname.append(facename);
			if (log) {
				std::cerr << "  Font: " << facename << " Full: \"" << fontname << '"';
				if (log_filename) {
					char *fname(0);
					{
						PangoFontDescription *fdesc(pango_font_face_describe(fface[j]));
						fname = pango_font_description_to_filename(fdesc);
						pango_font_description_free(fdesc);
					}
					std::cerr << " File: \"" << fname << '"';
					g_free(fname);
				}
				std::cerr << std::endl;
			}
			m_fontlist[fontname] = Pango::FontDescription(pango_font_face_describe(fface[j]), false);
		}
		g_free(fface);
	}
	g_free(ffam);
}
Glib::RefPtr<Pango::Layout> CartoRender::text_layout(const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::BasicTextParams& par,
						     double overridefontsize) const
{
	Glib::RefPtr<Pango::Layout> layout;
	for (const auto& fn : par.get_facename()) {
		fontlist_t::const_iterator i(m_fontlist.find(fn));
		if (i == m_fontlist.end())
			continue;
		layout = Pango::Layout::create(cr);
		Pango::FontDescription fd(i->second);
		if (std::isnan(overridefontsize))
			overridefontsize = par.get_size();
		fd.set_absolute_size(overridefontsize * Pango::SCALE);
		layout->set_font_description(fd);
		break;
	}
	if (!layout)
		return Glib::RefPtr<Pango::Layout>();
	// line spacing
#if defined(HAVE_PANGO_144)
	if (!std::isnan(par.get_linespacing())) {
		int linespacing(Pango::SCALE * par.get_linespacing());
		if (linespacing) {
			pango_layout_set_spacing(layout->gobj(), linespacing);
			pango_layout_set_line_spacing(layout->gobj(), 0);
		}
	}
#endif
	// line wrapping
	{
		int wrapwidth(0);
		if (!std::isnan(par.get_wrapwidth()) && par.get_wrapwidth() > 0)
			wrapwidth = par.get_wrapwidth() * Pango::SCALE;
		std::string curtext(par.get_name());
		for (;;) {
			layout->set_text(curtext);
			{
				// add attributes
				Pango::AttrList attrs;
				if (!std::isnan(par.get_characterspacing()) && par.get_characterspacing() > 0) {
					Pango::Attribute a(pango_attr_letter_spacing_new(par.get_characterspacing() * Pango::SCALE), false);
					a.set_start_index(0);
					a.set_end_index(curtext.size());
					attrs.change(a);
				}
			}
			if (wrapwidth <= 0)
				break;
			bool work(false);
			Pango::LayoutIter iter(layout->get_iter());
			for (;;) {
				if (iter.get_index() < static_cast<int>(curtext.size()) && iter.get_index() >= 0 &&
				    curtext[iter.get_index()] == par.get_wrapcharacter()) {
					Pango::Rectangle rect(iter.get_char_extents());
					if (rect.get_x() >= wrapwidth) {
						curtext[iter.get_index()] = '\n';
						work = true;
						if (!iter.next_line())
							break;
						continue;
					}
				}
				if (!iter.next_char())
					break;
			}
			if (!work)
				break;
		}
	}
	return layout;
}

bool CartoRender::shield_to_source(double& width, double& height, const Cairo::RefPtr<Cairo::Context>& cr, const CartoCSS::ShieldParams& par, double overridefontsize)
{
	width = height = std::numeric_limits<double>::quiet_NaN();
	if (!cr)
		return false;
	Glib::RefPtr<Pango::Layout> layout(text_layout(cr, par, overridefontsize));
	if (!layout)
		return false;
	switch (par.get_horizontalalignment()) {
	case CartoCSS::TextParams::horizontalalignment_t::left:
		layout->set_alignment(Pango::ALIGN_LEFT);
		break;

	case CartoCSS::TextParams::horizontalalignment_t::right:
		layout->set_alignment(Pango::ALIGN_RIGHT);
		break;

	default:
		layout->set_alignment(Pango::ALIGN_CENTER);
		break;
	}
	double pwidth, pheight;
	if (!set_pattern(pwidth, pheight, par.get_file(), cr, false, true))
		return false;
	width = pwidth;
	height = pheight;
	cr->push_group();
	// move shield image
	{
		Cairo::Matrix matrix;
		cr->get_source()->get_matrix(matrix);
		matrix.translate(-0.5 * pwidth, -0.5 * pheight);
		if (!par.get_unlockimage())
			matrix.translate(-par.get_textdx(), -par.get_textdy());
		cr->get_source()->set_matrix(matrix);
	}
	// paint shield image
	cr->paint();
	if (false) {
		set_color(cr, par.get_fill(), 1);
		cr->set_operator(Cairo::OPERATOR_IN);
		cr->paint();
		cr->set_operator(Cairo::OPERATOR_OVER);
	}
	// paint text over it
	double originx(par.get_textdx() + 0.5 * pwidth), originy(par.get_textdy() + 0.5 * pheight);
	layout->update_from_cairo_context(cr);
	Pango::Rectangle inkext(layout->get_ink_extents());
	originx -= (inkext.get_x() + 0.5 * inkext.get_width()) * (1.0 / Pango::SCALE);
	originy -= (inkext.get_y() + 0.5 * inkext.get_height()) * (1.0 / Pango::SCALE);
	cr->begin_new_path();
	cr->move_to(originx, originy);
	if (true) {
		pango_cairo_layout_path(cr->cobj(), layout->gobj());
		if (!std::isnan(par.get_haloradius()) && par.get_haloradius() > 0) {
			set_color(cr, par.get_halofill());
			cr->set_line_width(par.get_haloradius());
			cr->set_line_cap(Cairo::LINE_CAP_ROUND);
			cr->set_line_join(Cairo::LINE_JOIN_ROUND);
			cr->stroke_preserve();
		}
		set_color(cr, par.get_fill());
		cr->fill();
	} else {
		if (!std::isnan(par.get_haloradius()) && par.get_haloradius() > 0) {
			pango_cairo_layout_path(cr->cobj(), layout->gobj());
			set_color(cr, par.get_halofill());
			cr->set_line_width(par.get_haloradius());
			cr->set_line_cap(Cairo::LINE_CAP_ROUND);
			cr->set_line_join(Cairo::LINE_JOIN_ROUND);
			cr->stroke();
		}
		cr->begin_new_path();
		cr->move_to(originx, originy);
		set_color(cr, par.get_fill());
		layout->show_in_cairo_context(cr);
	}
	cr->pop_group_to_source();
	return true;
}
